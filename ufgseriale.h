#ifndef UFGSERIALE_H
#define UFGSERIALE_H

#include <QObject>
#include <QTimer>

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>



#define MAX_RX_BUFF 256

#define DATA_SIZE8      CS8
#define DATA_SIZE7      CS7
#define PARITY_NONE     0
#define PARITY_EVE      ( INPCK | PARENB)
#define PARITY_ODD      ( PARENB | PARODD | INPCK)
#define BITSTOP_1       0
#define BITSTOP_2       CSTOPB

class ufgSeriale : public QObject
{
    Q_OBJECT
public:
    explicit ufgSeriale(QObject *parent = 0);

    bool        binaryMode;
    bool        processLine;
    bool        isOpen;
    bool        flag_rx;
    int         carattereIniziale;
    int         carattereFinale;
    speed_t     baudRate;

    int  openPort();
    void closePort();
    void writeQString( QString* tx);
    void writeCString( char* tx );
    void writeChar( char tx);
    void writeBuffer( char* buf, int len );
    void setBinaryMode(bool set);
    void setEnable485(bool en) { enable485 = en; }
    void setPortname( QString port) { portname = port; }

    unsigned short CRC16( char *puchMsg, unsigned int dataLength);


private:
    void cfmakeraw (struct termios *termios_p);

    QTimer* timerRead;

    tcflag_t    parity;
    tcflag_t    data;
    tcflag_t    bitStop;
    int         flowControl;

    char bufferRx[MAX_RX_BUFF];
    char bufferRxCopy[MAX_RX_BUFF];

    int lenRx;
    int punRx;
    int faseRead;

    int  serialfd;        // file descriptor

    bool enable485;

    QString portname;

signals:
    void rxChar( char c);
    void rxLine( char* line, int len);
private slots:
    void readPort();            // letto in ascii
    void readPortInBinary();    // letto in binario
public slots:
};

#endif // UFGSERIALE_H
