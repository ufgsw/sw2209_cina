#ifndef PARAMETRI_H
#define PARAMETRI_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QList>

#define ITALIANO   "it"
#define INGLESE    "en"
#define CINESE     "cina"
#define TEDESCO    "de"
#define RELEASE_FW "1.3.6"

struct sTipoSonda
{
    unsigned char resistivo_dinamico         :1;
    unsigned char resistivo_old              :1;
    unsigned char resistivo_statico_piccolo  :1;
    unsigned char resistivo_statico_grande   :1;
    unsigned char capacitivo_dinamico        :1;
    unsigned char capacitivo_old             :1;
    unsigned char capacitivo_statico_piccolo :1;
    unsigned char capacitivo_statico_grande  :1;
};

struct sTipoTrattamento
{
    unsigned char resistivo  :4;
    unsigned char capacitivo :4;
};

union uTipoSonda
{
    unsigned char    val;
    sTipoSonda       tipo;
    sTipoTrattamento trattamento;
};

struct sProgramma
{
    int     id;
    QString nome;
    QString descrizione;
    QString note;

    QString posizionePaziente;
    QString posizioneNeutro;
    QString posizioneElettrodo;
    QString elettrodo;

    int     potenza;        // in percentuale
    int     potenzaMinima;
    int     potenzaMassima;
    int     tempo;          // in secondi
    int     tempoMinimo;
    int     tempoMassimo;
    int     frequenza;      // in KHz
    int     frequenzaPemf;  // in Hz

    uTipoSonda  sonda;

    QString tipopTrattamnento;
    QString tipoElettrodi;

    //bool    trattamentoCapacitico;
    //bool    elettrodoDinamico;

    bool    enable;
};

class parametri
{
public:
    parametri();

    void read();
    void save();

    bool    calibra;

    int     tipoMacchina;
    int     potenzaMassima;
    QString nomeMacchina;

    QString lingua;
    QString passwordSetup;
    QString passwordCalibra;
    QString release;

    QList<sProgramma> elencoProgrammi;
    sProgramma        mioProgramma;

    void setProgramma( sProgramma programma);

private:
    QJsonDocument jsonDoc;
    QJsonObject   jParametri;
    QJsonArray    jProgrammi;
    QJsonObject   jProgramma;

};

#endif // PARAMETRI_H
