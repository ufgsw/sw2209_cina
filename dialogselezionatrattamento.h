#ifndef DIALOGSELEZIONAABCD_H
#define DIALOGSELEZIONAABCD_H

#include <QDialog>
#include "diatermia.h"

namespace Ui {
class dialogSelezionaabcd;
}

class dialogSelezionaabcd : public QDialog
{
    Q_OBJECT

public:
    explicit dialogSelezionaabcd(QWidget *parent = 0);
    ~dialogSelezionaabcd();

    void setTitle( QString title );
    void setParametri( parametri* conf);

private:
    Ui::dialogSelezionaabcd *ui;

    void paintEvent(QPaintEvent *e);
    void showEvent(QShowEvent* e);

//    bool* variabileA;
//    bool* variabileB;
//    bool* variabileC;
//    bool* variabileD;

    parametri* mConfig;

public slots:
    void selezionatoA();
    void selezionatoB();
    void selezionatoC();
    void selezionatoD();
    void selezionatoE();
    void selezionatoF();
    void esci();

    void rotellaMove(bool up);
    void rotellaClick();

signals:
    void onClose();

};

#endif // DIALOGSELEZIONAABCD_H
