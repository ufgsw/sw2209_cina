//
//  serial.h
//  duetredue
//
//  Created by Diego Della Muzia on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "QObject"
#include "QString"

#define SERIAL_BUFF 8192

class ufgSerial : public QObject
{
    Q_OBJECT

public:
    explicit ufgSerial(QObject *parent = 0) : QObject(parent);

    bool isOpen;
    int  baudRate;
    int  parity;
    int  data;
    int  bitSTop;
    int  flowControl;
    int  bufferLen;

    QString* portName;

    void open();
    void close();
    void sendQString( QString* tx);

private:
    char buff[SERIAL_BUFF];
    int  carattereIniziale;
    int  carattereFinale;
    int  serial_fd;        // file descriptor
    bool rxOK;

signals:
    void rxChar( char c);

};



