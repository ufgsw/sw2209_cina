#include "ufgseriale.h"

#include <string.h>
#include <stdio.h>

#include <QThread>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <asm-generic/ioctls.h> /* TIOCGRS485 + TIOCSRS485 ioctl definitions */
#include <linux/ioctl.h>
#include <linux/serial.h>

static struct termios gOriginalTTYAttrs;

ufgSeriale::ufgSeriale(QObject *parent) : QObject(parent)
{
    memset(bufferRx, 0, MAX_RX_BUFF );

    isOpen            = false;
    baudRate          = B9600;
    data              = CS8;
    parity            = PARITY_NONE;
    bitStop           = BITSTOP_1;
    flowControl       = 0;
    lenRx             = 0;
    faseRead          = 0;
    portname          = "/dev/ttymxc1";
    binaryMode        = true;
    enable485         = false;
    flag_rx           = false;

    carattereIniziale = '$';   //0x02;
    carattereFinale   = 0x0d;  //0x03;
    processLine       = true;

    timerRead = new QTimer(this);
}

int ufgSeriale::openPort()
{
    struct serial_rs485 rs485conf;
    rs485conf.delay_rts_before_send = 2;
    rs485conf.delay_rts_after_send  = 2;

    struct termios  options;

    if( isOpen           ) return -1;
    if( portname == NULL ) return -1;

    serialfd = open( portname.toLatin1(), O_RDWR | O_NOCTTY | O_NONBLOCK ) ;

    if (serialfd == -1)
    {
        QString error = QString("Error opening serial port %1 %2 ").arg(portname).arg(strerror(errno));
        qDebug(error.toLatin1() );
        goto error;
    }
    /* The FNDELAY option causes the read function to return 0 if no
            characters are available on the port. To restore normal (blocking) behavior,
            call fcntl() without the FNDELAY option: */

    //	fcntl(fd, F_SETFL, 0); //<---- bloccante
    fcntl(serialfd, F_SETFL, FNDELAY); //<---- NON bloccante
    tcgetattr(serialfd , &options);

    //set port speed
    cfsetispeed(&options, baudRate);
    cfsetospeed(&options, baudRate);

    //options.c_oflag &= ~OPOST;

    //options.c_cflag = 0;
    options.c_cflag |= (CLOCAL | CREAD);

    //set 8n1
    options.c_cflag |= (data | parity | bitStop);
    //options.c_cflag &= ~CRTSCTS;
    cfmakeraw(&options);/* RAW MODE */

    //set raw input

    //options.c_lflag &= ~(ICANON | /* Enable canonical input (else raw) */
    //            ECHO | /*Enable echoing of input characters	*/
    //            ECHOE | /*Echo erase character as BS-SP-BS*/
    //            ISIG); /*Enable SIGINTR, SIGSUSP, SIGDSUSP, and SIGQUIT signals*/

    //options.c_lflag = 0;

    tcsetattr(serialfd, TCSANOW, &options); /*Make changes now without waiting for data to complete*/

    if( enable485 == true )
    {
        if (ioctl (serialfd, TIOCGRS485, &rs485conf) < 0)
        {
            qDebug("Error: TIOCGRS485 ioctl not supported.\n");
        }

        /* Enable RS-485 mode: */
        rs485conf.flags |= SER_RS485_ENABLED;
        if (ioctl (serialfd, TIOCSRS485, &rs485conf) < 0)
        {
            qDebug("Error: TIOCSRS485 ioctl not supported.\n");
        }
    }
    // Success:
    if( binaryMode == true )
        connect(timerRead, SIGNAL(timeout()), this, SLOT(readPortInBinary()) );
    else
        connect(timerRead, SIGNAL(timeout()), this, SLOT(readPort()));

    timerRead->start(1);

    qDebug( "Serial " + portname.toLatin1() + " is open " );
    isOpen = true;

    return serialfd;

    // Failure:
    error:
    if (serialfd != -1)
    {
      isOpen = false;
      close(serialfd);
    }

    return -1;
}

// Set the serial in raw mode
void ufgSeriale::cfmakeraw (struct termios *termios_p)
{
  termios_p->c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON );
  termios_p->c_oflag &= ~OPOST;
  termios_p->c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  //termios_p->c_cflag &= ~(CSIZE | PARENB);
  //termios_p->c_cflag |= CS8;
}



void ufgSeriale::setBinaryMode(bool set)
{
    binaryMode = set;
    //disconnect( timerRead , SIGNAL(timeout),this, 0  );
}

void ufgSeriale::closePort()
{
    if( isOpen)
    {
        timerRead->stop();
        qDebug( "Seriale is closed");
        close( serialfd);
        isOpen = false;
    }
}

void ufgSeriale::writeQString( QString* tx)
{
    const char* ctx = tx->toLatin1();
    if( isOpen )
    {
        write( serialfd, ctx, strlen(ctx));
    }
}

void ufgSeriale::writeBuffer(char *buf, int len)
{
    if( isOpen )
    {
        write( serialfd, buf, len );
    }
}

void ufgSeriale::writeCString( char* tx )
{
    if( isOpen )
    {
        write( serialfd, tx, strlen(tx));
    }
}

void ufgSeriale::writeChar( char tx)
{
    if( isOpen )
    {
        write( serialfd, &tx, 1);
    }
}

void ufgSeriale::readPort()
{
    char ch;
    if( isOpen && processLine )
    {
        while (read(serialfd,&ch,1) > 0 )
        {

            if( ch == carattereIniziale )
            {
                lenRx = 0;
            }
            else if( ch == carattereFinale)
            {
                bufferRx[lenRx] = 0;
                //memcpy(bufferRxCopy,bufferRx,lenRx+1);
                emit rxLine( bufferRx, lenRx);
            }
            else if( lenRx < MAX_RX_BUFF )
            {
                bufferRx[lenRx] = ch;
                ++lenRx;
            }
        }
    }
    else if( isOpen )
    {
        while (read(serialfd,&ch,1) > 0 )
        {
            emit rxChar(ch);
        }
    }
}

void ufgSeriale::readPortInBinary()
{
    static char end;
    char ch;
    if( isOpen  )
    {
        while( read( serialfd,&ch,1 ) > 0 )
        {
            end = 5;
            bufferRx[lenRx] = ch;
            ++lenRx;
            //qDebug("ricevuto " + QString::number(ch,16).toLatin1() );
        }
        if( end ) --end;
        else
        {
            if( lenRx != 0 )
            {
                emit rxLine( bufferRx, lenRx );
                flag_rx = true;
                lenRx   = 0;
            }
        }
    }

//    char ch;
//    if( isOpen )
//    {
//        while (read(serialfd,&ch,1) > 0 )
//        {
//            switch( faseRead )
//            {
//            case 0:
//                if( ch == 'u' )
//                {
//                    faseRead = 1;
//                    bufferRx[0] = ch;
//                }
//                break;
//            case 1:
//                if( ch == 'f' )
//                {
//                    faseRead = 2;
//                    bufferRx[1] = ch;
//                }
//                else faseRead = 0;
//                break;
//            case 2:
//                if( ch == 'g' )
//                {
//                    faseRead = 3;
//                    bufferRx[2] = ch;
//                }
//                else faseRead = 0;
//                break;
//            case 3:
//                punRx       = ch;
//                bufferRx[3] = ch;
//                lenRx       = 4;
//                faseRead    = 4;
//                break;
//            case 4:PARENB
//                bufferRx[lenRx] = ch;
//                ++lenRx;
//                if( punRx > 1 ) --punRx;
//                else
//                {
//                    memcpy( bufferRxCopy,bufferRx,lenRx );
//                    emit rxLine( bufferRxCopy, lenRx );
//                    faseRead = 0;
//                }
//                break;
//            }
//        }
//    }
}

unsigned short ufgSeriale::CRC16( char *puchMsg, unsigned int dataLength)
{
    unsigned short CheckSum;
    unsigned int j;
    unsigned short crc,i;
    CheckSum = 0xffff;
    for ( j = 0 ; j < dataLength ; j++ )
    {
        CheckSum = CheckSum^((unsigned short)puchMsg[j] & 0x00ff);
        for( i = 8 ; i > 0 ; i-- )
        {
            if((CheckSum)&0x0001)  CheckSum   = (CheckSum>>1)^0xa001;
            else CheckSum >>= 1;
        }
    }
    crc = CheckSum >> 8; // CheckSum
    CheckSum <<= 8;
    crc |= CheckSum & 0xff00;
    return crc;
}


