﻿#include "dialogorologio.h"
#include "ui_dialogorologio.h"

#include <QTime>

DialogOrologio::DialogOrologio(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogOrologio)
{
    ui->setupUi(this);

    ui->labelTitolo->setText( tr("Orologio"));
    ui->buttonExit->setImage( ":/img/ico/return.png", ":/img/ico/return.png", ":/img/ico/return.png" , 100 );

    connect( ui->buttonExit,  SIGNAL(buttonClicked()), this, SLOT(esci()));

    tastierino = new tastierinoNumerico(this);
    tastierino->setPasswordMode( false );

    tastierino->setGeometry( 200, 40, 400,400 );

    timer = new QTimer(this);

    connect( timer, SIGNAL(timeout()), this, SLOT(aggiorna()) );

    connect( ui->buttonOre, SIGNAL(buttonClicked()), this, SLOT(setOre()) );
    connect( ui->buttonMinuti, SIGNAL(buttonClicked()), this, SLOT(setMinuti()) );
    connect( ui->buttonSecondi, SIGNAL(buttonClicked()), this, SLOT(setSecondi()) );



//    QTime adesso = QTime::currentTime();

//    mOre = adesso.hour();
//    mMinuti = adesso.minute();
//    mSecondi = adesso.second();

}

DialogOrologio::~DialogOrologio()
{
    delete ui;
    delete tastierino;
}

void DialogOrologio::showEvent(QShowEvent *e)
{
    aggiorna();
    timer->start( 1000 );
}

void DialogOrologio::esci()
{
    ui->orologio->salva();
    timer->stop();
    close();
}

void DialogOrologio::aggiorna()
{
    mOre     = ui->orologio->getOre();
    mMinuti  = ui->orologio->getMinuti();
    mSecondi = ui->orologio->getSecondi();

    QString ore,min,sec;
    if( mOre < 10 ) ore = "0" + QString("%1").arg( mOre);
    else            ore = QString("%1").arg( mOre);

    if( mMinuti < 10 ) min = "0" + QString("%1").arg( mMinuti );
    else               min = QString("%1").arg( mMinuti );

    if( mSecondi < 10 ) sec = "0" + QString("%1").arg( mSecondi );
    else                sec = QString("%1").arg( mSecondi );

    ui->buttonOre->setText(     ore );
    ui->buttonMinuti->setText(  min );
    ui->buttonSecondi->setText( sec );
}

void DialogOrologio::setOre()
{
    tastierino->setTitle( tr("Ore"));
    tastierino->exec();

    if( tastierino->getText().length() > 0 )
    {
        int ore = tastierino->getText().toInt();
        if( ore < 24 )
        {
            mOre = ore;
            ui->orologio->setOrario( mOre, mMinuti, mSecondi );
        }
    }
}

void DialogOrologio::setMinuti()
{
    tastierino->setTitle( tr("Minuti"));
    tastierino->exec();

    if( tastierino->getText().length() > 0 )
    {
        int min = tastierino->getText().toInt();
        if( min < 60 )
        {
            mMinuti = min;
            ui->orologio->setOrario( mOre, mMinuti, mSecondi );
        }
    }
}

void DialogOrologio::setSecondi()
{
    tastierino->setTitle( tr("Secondi"));
    tastierino->exec();

    if( tastierino->getText().length() > 0 )
    {
        int sec = tastierino->getText().toInt();
        if( sec < 60 )
        {
            mSecondi = sec;
            ui->orologio->setOrario( mOre, mMinuti, mSecondi );
        }
    }
}
