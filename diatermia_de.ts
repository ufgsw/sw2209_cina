<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>DialogOrologio</name>
    <message>
        <location filename="dialogorologio.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="12"/>
        <source>Orologio</source>
        <translation>UHR</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="82"/>
        <source>Ore</source>
        <translation>Stunden</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="98"/>
        <source>Minuti</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="114"/>
        <source>Secondi</source>
        <translation>Sekunden</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="36"/>
        <source>Diatermia</source>
        <translation>DIATHERMIE</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="43"/>
        <location filename="mainwindow.cpp" line="1837"/>
        <source>MODALITA&apos;</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="44"/>
        <location filename="mainwindow.cpp" line="824"/>
        <location filename="mainwindow.cpp" line="1838"/>
        <source>FREQUENZA</source>
        <translation>FREQUENZ</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="50"/>
        <location filename="mainwindow.cpp" line="1844"/>
        <source>IMPOSTAZIONI</source>
        <translation>EINSTELLUNGEN</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="322"/>
        <location filename="mainwindow.cpp" line="343"/>
        <location filename="mainwindow.cpp" line="596"/>
        <location filename="mainwindow.cpp" line="617"/>
        <location filename="mainwindow.cpp" line="1864"/>
        <location filename="mainwindow.cpp" line="1879"/>
        <source>STATICO GRANDE</source>
        <translation>große statische</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="329"/>
        <location filename="mainwindow.cpp" line="350"/>
        <location filename="mainwindow.cpp" line="603"/>
        <location filename="mainwindow.cpp" line="624"/>
        <location filename="mainwindow.cpp" line="1859"/>
        <location filename="mainwindow.cpp" line="1884"/>
        <source>STATICO PICCOLO</source>
        <translation>kleine statische</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="849"/>
        <source>FREQUENZA PEMF</source>
        <translation>SUPER LOW FREQUENCY</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="46"/>
        <location filename="mainwindow.cpp" line="876"/>
        <location filename="mainwindow.cpp" line="1840"/>
        <source>POTENZA</source>
        <translation>POWER</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="45"/>
        <location filename="mainwindow.cpp" line="1839"/>
        <source>FREQUENZA LOW</source>
        <translation>SUPER LOW FREQUENCY</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="47"/>
        <location filename="mainwindow.cpp" line="741"/>
        <location filename="mainwindow.cpp" line="1841"/>
        <source>TEMPO</source>
        <translation>ZEIT</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <location filename="mainwindow.cpp" line="1842"/>
        <source>GUIDA</source>
        <translation>HILFE</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="49"/>
        <location filename="mainwindow.cpp" line="529"/>
        <location filename="mainwindow.cpp" line="1843"/>
        <source>INFO</source>
        <translation>INFORMATIONEN</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="1031"/>
        <location filename="mainwindow.cpp" line="1887"/>
        <source>Versione</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="314"/>
        <location filename="mainwindow.cpp" line="321"/>
        <location filename="mainwindow.cpp" line="328"/>
        <location filename="mainwindow.cpp" line="588"/>
        <location filename="mainwindow.cpp" line="595"/>
        <location filename="mainwindow.cpp" line="602"/>
        <location filename="mainwindow.cpp" line="1848"/>
        <location filename="mainwindow.cpp" line="1853"/>
        <location filename="mainwindow.cpp" line="1858"/>
        <location filename="mainwindow.cpp" line="1863"/>
        <source>CAPACITIVO</source>
        <translation>KAPAZITIVE</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="315"/>
        <location filename="mainwindow.cpp" line="336"/>
        <location filename="mainwindow.cpp" line="589"/>
        <location filename="mainwindow.cpp" line="610"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <location filename="mainwindow.cpp" line="1854"/>
        <location filename="mainwindow.cpp" line="1869"/>
        <location filename="mainwindow.cpp" line="1874"/>
        <source>DINAMICO</source>
        <translation>DYNAMISCH</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="335"/>
        <location filename="mainwindow.cpp" line="342"/>
        <location filename="mainwindow.cpp" line="349"/>
        <location filename="mainwindow.cpp" line="609"/>
        <location filename="mainwindow.cpp" line="616"/>
        <location filename="mainwindow.cpp" line="623"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <location filename="mainwindow.cpp" line="1873"/>
        <location filename="mainwindow.cpp" line="1878"/>
        <location filename="mainwindow.cpp" line="1883"/>
        <source>RESISTIVO</source>
        <translation>RESISTIV</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="530"/>
        <source>desrizione info</source>
        <translation>CARETHERAPY ist ein medizinisches Gerät, Missbrauch Schädigung des Patienten führen kann, bestätigen Sie, dass Sie das Handbuch gelesen haben und beobachtet haben, die oben angeführten Vorsichtsmaßnahmen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="548"/>
        <location filename="mainwindow.cpp" line="569"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="773"/>
        <source>MODALITA</source>
        <translation>MODE</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="906"/>
        <source>DINAMICI</source>
        <translation>DYNAMIC</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="907"/>
        <source>STATICI</source>
        <translation>STATIC</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="981"/>
        <source>CALIBRAZIONE TERMINATA</source>
        <translation>KALIBRIERUNG ABGESCHLOSSEN</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="999"/>
        <source>CALIBRAZIONE IN CORSO</source>
        <translation>KALIBRIERUNG LÄUFT</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1609"/>
        <source>descrizione allarme B001</source>
        <translation>Hardware Error B001( Interne Kommunikationsfehler an das System )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1622"/>
        <source>descrizione allarme A001</source>
        <translation>Hardware Error A001 ( Das System wird durch Kurzschluss erkannt )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="1637"/>
        <location filename="mainwindow.cpp" line="1642"/>
        <location filename="mainwindow.cpp" line="1657"/>
        <location filename="mainwindow.cpp" line="1662"/>
        <location filename="mainwindow.cpp" line="1694"/>
        <source>descrizione allarme B002</source>
        <translation>Hardware Error B002 ( Generisches Hardware-Fehler )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <source>descrizione allarme A003</source>
        <translation>Hardware Error A003 ( Das System hat einen anomalen Stromverbrauch )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1647"/>
        <source>descrizione allarme A004</source>
        <translation>Hardware Error A004 ( Das System hat eine abnorme Spannung )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1652"/>
        <source>descrizione allarme A002</source>
        <translation>Hardware Error A002 ( Das System hat eine Überhitzung erkannt )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1667"/>
        <source>descrizione allarme A005</source>
        <translation>Hardware Error A005 ( Das System hat einen anormalen aktuellen Patienten )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1684"/>
        <source>descrizione allarme A006</source>
        <translation>Hardware Error A006 ( Das System hat einen anormalen Betriebsfrequenz )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1689"/>
        <location filename="mainwindow.cpp" line="1754"/>
        <source>descrizione warning elettrodo massa</source>
        <translation>Aufmerksamkeit : Masseelektrode getrennt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1699"/>
        <source>descrizione warning mano scollegata</source>
        <translation>Aufmerksamkeit : Notknopf offline</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1780"/>
        <source>Verifica contatto sonda</source>
        <translation>Überprüfen Sie die Sonde Kontakt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1674"/>
        <location filename="mainwindow.cpp" line="1706"/>
        <location filename="mainwindow.cpp" line="1762"/>
        <source>descrizione warning capacitivo</source>
        <translation>Aufmerksamkeit : Kapazitive Elektrode getrennt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1678"/>
        <location filename="mainwindow.cpp" line="1711"/>
        <location filename="mainwindow.cpp" line="1767"/>
        <source>descrizione warning resistivo</source>
        <translation>Aufmerksamkeit : Resistive Elektrode getrennt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1774"/>
        <source>descrizione allarme emergenza</source>
        <translation>Aufmerksamkeit : Überprüfung der Notfall-Taste des Patienten</translation>
    </message>
</context>
<context>
    <name>dialogElencoProtocolli</name>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="97"/>
        <source>PROTOCOLLI GENERICI</source>
        <translation>GENERAL PROTOKOLLE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="99"/>
        <source>FASE ACUTA</source>
        <translation>AKUT PHASE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="100"/>
        <source>FASE CRONICA</source>
        <translation>Chronische Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="101"/>
        <source>RIDUZIONE DEL DOLORE</source>
        <translation>Schmerzreduktion</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="102"/>
        <source>CONTRATTURA</source>
        <translation>Kontraktur</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="103"/>
        <source>LINFODRENAGGIO</source>
        <translation>Lymphodränage</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="104"/>
        <source>RADICOLOPATIA FASE ACUTA</source>
        <translation>Radiculopathy AKUT PHASE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="105"/>
        <source>BLOCCO ARTICOLARE / RIGIDITA</source>
        <translation>Blockfuge / STEIFIGKEIT</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="126"/>
        <source>PROTOCOLLI PER PATOLOGIA</source>
        <translation>PROTOKOLLE FÜR KRANKHEIT</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="192"/>
        <location filename="dialogelencoprotocolli.cpp" line="218"/>
        <source>PROTOCOLLI NEUROLOGICI</source>
        <translation>PROTOKOLLE KREBS</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="200"/>
        <source>MIELO LESIONE PARZIALE</source>
        <translation>Mielo PARTIAL VERLETZUNGS</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="201"/>
        <source>TRATTAMENTO POST ICTUS</source>
        <translation>Behandlung nach STROKE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="202"/>
        <source>LESIONI NERVOSE PERIFERICHE ( ERNIE DISCALI )</source>
        <translation>Verletzung peripherer Nerven (Bandscheibenvorfall )</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="203"/>
        <source>LESIONI NERVOSE PERIFERICHE (PLESSO BRACHIALE )</source>
        <translation>Verletzung peripherer Nerven (Plexus brachialis)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="256"/>
        <location filename="dialogelencoprotocolli.cpp" line="270"/>
        <location filename="dialogelencoprotocolli.cpp" line="284"/>
        <location filename="dialogelencoprotocolli.cpp" line="298"/>
        <location filename="dialogelencoprotocolli.cpp" line="348"/>
        <location filename="dialogelencoprotocolli.cpp" line="483"/>
        <location filename="dialogelencoprotocolli.cpp" line="540"/>
        <location filename="dialogelencoprotocolli.cpp" line="554"/>
        <location filename="dialogelencoprotocolli.cpp" line="568"/>
        <location filename="dialogelencoprotocolli.cpp" line="582"/>
        <location filename="dialogelencoprotocolli.cpp" line="754"/>
        <location filename="dialogelencoprotocolli.cpp" line="813"/>
        <location filename="dialogelencoprotocolli.cpp" line="827"/>
        <location filename="dialogelencoprotocolli.cpp" line="841"/>
        <location filename="dialogelencoprotocolli.cpp" line="855"/>
        <location filename="dialogelencoprotocolli.cpp" line="906"/>
        <location filename="dialogelencoprotocolli.cpp" line="1013"/>
        <location filename="dialogelencoprotocolli.cpp" line="1070"/>
        <location filename="dialogelencoprotocolli.cpp" line="1084"/>
        <location filename="dialogelencoprotocolli.cpp" line="1098"/>
        <location filename="dialogelencoprotocolli.cpp" line="1271"/>
        <location filename="dialogelencoprotocolli.cpp" line="1329"/>
        <location filename="dialogelencoprotocolli.cpp" line="1343"/>
        <location filename="dialogelencoprotocolli.cpp" line="1503"/>
        <location filename="dialogelencoprotocolli.cpp" line="1561"/>
        <location filename="dialogelencoprotocolli.cpp" line="1709"/>
        <location filename="dialogelencoprotocolli.cpp" line="1723"/>
        <location filename="dialogelencoprotocolli.cpp" line="1737"/>
        <location filename="dialogelencoprotocolli.cpp" line="1751"/>
        <location filename="dialogelencoprotocolli.cpp" line="1812"/>
        <location filename="dialogelencoprotocolli.cpp" line="2296"/>
        <location filename="dialogelencoprotocolli.cpp" line="2324"/>
        <location filename="dialogelencoprotocolli.cpp" line="2375"/>
        <location filename="dialogelencoprotocolli.cpp" line="2389"/>
        <source>prono</source>
        <translation>anfällig</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="257"/>
        <location filename="dialogelencoprotocolli.cpp" line="271"/>
        <location filename="dialogelencoprotocolli.cpp" line="285"/>
        <location filename="dialogelencoprotocolli.cpp" line="299"/>
        <location filename="dialogelencoprotocolli.cpp" line="541"/>
        <location filename="dialogelencoprotocolli.cpp" line="555"/>
        <location filename="dialogelencoprotocolli.cpp" line="569"/>
        <location filename="dialogelencoprotocolli.cpp" line="583"/>
        <location filename="dialogelencoprotocolli.cpp" line="605"/>
        <location filename="dialogelencoprotocolli.cpp" line="619"/>
        <location filename="dialogelencoprotocolli.cpp" line="814"/>
        <location filename="dialogelencoprotocolli.cpp" line="828"/>
        <location filename="dialogelencoprotocolli.cpp" line="842"/>
        <location filename="dialogelencoprotocolli.cpp" line="856"/>
        <location filename="dialogelencoprotocolli.cpp" line="1071"/>
        <location filename="dialogelencoprotocolli.cpp" line="1085"/>
        <location filename="dialogelencoprotocolli.cpp" line="1099"/>
        <location filename="dialogelencoprotocolli.cpp" line="1330"/>
        <location filename="dialogelencoprotocolli.cpp" line="1344"/>
        <location filename="dialogelencoprotocolli.cpp" line="1562"/>
        <location filename="dialogelencoprotocolli.cpp" line="1710"/>
        <location filename="dialogelencoprotocolli.cpp" line="1724"/>
        <location filename="dialogelencoprotocolli.cpp" line="1738"/>
        <location filename="dialogelencoprotocolli.cpp" line="1752"/>
        <location filename="dialogelencoprotocolli.cpp" line="1775"/>
        <location filename="dialogelencoprotocolli.cpp" line="1789"/>
        <location filename="dialogelencoprotocolli.cpp" line="1863"/>
        <location filename="dialogelencoprotocolli.cpp" line="1877"/>
        <location filename="dialogelencoprotocolli.cpp" line="2246"/>
        <source>addome</source>
        <translation>Bauch</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="258"/>
        <location filename="dialogelencoprotocolli.cpp" line="272"/>
        <location filename="dialogelencoprotocolli.cpp" line="286"/>
        <location filename="dialogelencoprotocolli.cpp" line="300"/>
        <location filename="dialogelencoprotocolli.cpp" line="542"/>
        <location filename="dialogelencoprotocolli.cpp" line="556"/>
        <location filename="dialogelencoprotocolli.cpp" line="570"/>
        <location filename="dialogelencoprotocolli.cpp" line="584"/>
        <location filename="dialogelencoprotocolli.cpp" line="815"/>
        <location filename="dialogelencoprotocolli.cpp" line="829"/>
        <location filename="dialogelencoprotocolli.cpp" line="843"/>
        <location filename="dialogelencoprotocolli.cpp" line="857"/>
        <location filename="dialogelencoprotocolli.cpp" line="1072"/>
        <location filename="dialogelencoprotocolli.cpp" line="1086"/>
        <location filename="dialogelencoprotocolli.cpp" line="1100"/>
        <location filename="dialogelencoprotocolli.cpp" line="1331"/>
        <location filename="dialogelencoprotocolli.cpp" line="1345"/>
        <location filename="dialogelencoprotocolli.cpp" line="1563"/>
        <location filename="dialogelencoprotocolli.cpp" line="1711"/>
        <location filename="dialogelencoprotocolli.cpp" line="1725"/>
        <location filename="dialogelencoprotocolli.cpp" line="1739"/>
        <location filename="dialogelencoprotocolli.cpp" line="1753"/>
        <source>zona dolente</source>
        <translation>schmerzenden Bereich</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="259"/>
        <location filename="dialogelencoprotocolli.cpp" line="273"/>
        <location filename="dialogelencoprotocolli.cpp" line="337"/>
        <location filename="dialogelencoprotocolli.cpp" line="351"/>
        <location filename="dialogelencoprotocolli.cpp" line="365"/>
        <location filename="dialogelencoprotocolli.cpp" line="472"/>
        <location filename="dialogelencoprotocolli.cpp" line="486"/>
        <location filename="dialogelencoprotocolli.cpp" line="500"/>
        <location filename="dialogelencoprotocolli.cpp" line="543"/>
        <location filename="dialogelencoprotocolli.cpp" line="557"/>
        <location filename="dialogelencoprotocolli.cpp" line="607"/>
        <location filename="dialogelencoprotocolli.cpp" line="621"/>
        <location filename="dialogelencoprotocolli.cpp" line="709"/>
        <location filename="dialogelencoprotocolli.cpp" line="743"/>
        <location filename="dialogelencoprotocolli.cpp" line="757"/>
        <location filename="dialogelencoprotocolli.cpp" line="771"/>
        <location filename="dialogelencoprotocolli.cpp" line="816"/>
        <location filename="dialogelencoprotocolli.cpp" line="830"/>
        <location filename="dialogelencoprotocolli.cpp" line="881"/>
        <location filename="dialogelencoprotocolli.cpp" line="895"/>
        <location filename="dialogelencoprotocolli.cpp" line="909"/>
        <location filename="dialogelencoprotocolli.cpp" line="923"/>
        <location filename="dialogelencoprotocolli.cpp" line="1002"/>
        <location filename="dialogelencoprotocolli.cpp" line="1016"/>
        <location filename="dialogelencoprotocolli.cpp" line="1030"/>
        <location filename="dialogelencoprotocolli.cpp" line="1073"/>
        <location filename="dialogelencoprotocolli.cpp" line="1087"/>
        <location filename="dialogelencoprotocolli.cpp" line="1138"/>
        <location filename="dialogelencoprotocolli.cpp" line="1166"/>
        <location filename="dialogelencoprotocolli.cpp" line="1260"/>
        <location filename="dialogelencoprotocolli.cpp" line="1274"/>
        <location filename="dialogelencoprotocolli.cpp" line="1288"/>
        <location filename="dialogelencoprotocolli.cpp" line="1332"/>
        <location filename="dialogelencoprotocolli.cpp" line="1369"/>
        <location filename="dialogelencoprotocolli.cpp" line="1383"/>
        <location filename="dialogelencoprotocolli.cpp" line="1397"/>
        <location filename="dialogelencoprotocolli.cpp" line="1411"/>
        <location filename="dialogelencoprotocolli.cpp" line="1457"/>
        <location filename="dialogelencoprotocolli.cpp" line="1492"/>
        <location filename="dialogelencoprotocolli.cpp" line="1506"/>
        <location filename="dialogelencoprotocolli.cpp" line="1520"/>
        <location filename="dialogelencoprotocolli.cpp" line="1601"/>
        <location filename="dialogelencoprotocolli.cpp" line="1629"/>
        <location filename="dialogelencoprotocolli.cpp" line="1651"/>
        <location filename="dialogelencoprotocolli.cpp" line="1712"/>
        <location filename="dialogelencoprotocolli.cpp" line="1726"/>
        <location filename="dialogelencoprotocolli.cpp" line="1740"/>
        <location filename="dialogelencoprotocolli.cpp" line="1777"/>
        <location filename="dialogelencoprotocolli.cpp" line="1791"/>
        <location filename="dialogelencoprotocolli.cpp" line="1815"/>
        <location filename="dialogelencoprotocolli.cpp" line="1865"/>
        <location filename="dialogelencoprotocolli.cpp" line="1879"/>
        <location filename="dialogelencoprotocolli.cpp" line="1954"/>
        <location filename="dialogelencoprotocolli.cpp" line="1968"/>
        <location filename="dialogelencoprotocolli.cpp" line="2033"/>
        <location filename="dialogelencoprotocolli.cpp" line="2047"/>
        <location filename="dialogelencoprotocolli.cpp" line="2075"/>
        <location filename="dialogelencoprotocolli.cpp" line="2127"/>
        <location filename="dialogelencoprotocolli.cpp" line="2141"/>
        <location filename="dialogelencoprotocolli.cpp" line="2169"/>
        <location filename="dialogelencoprotocolli.cpp" line="2220"/>
        <location filename="dialogelencoprotocolli.cpp" line="2234"/>
        <location filename="dialogelencoprotocolli.cpp" line="2248"/>
        <location filename="dialogelencoprotocolli.cpp" line="2299"/>
        <location filename="dialogelencoprotocolli.cpp" line="2313"/>
        <location filename="dialogelencoprotocolli.cpp" line="2327"/>
        <location filename="dialogelencoprotocolli.cpp" line="2378"/>
        <source>DINAMICO 60</source>
        <translation>dynamisch 60</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="264"/>
        <location filename="dialogelencoprotocolli.cpp" line="292"/>
        <location filename="dialogelencoprotocolli.cpp" line="328"/>
        <location filename="dialogelencoprotocolli.cpp" line="356"/>
        <location filename="dialogelencoprotocolli.cpp" line="414"/>
        <location filename="dialogelencoprotocolli.cpp" line="463"/>
        <location filename="dialogelencoprotocolli.cpp" line="491"/>
        <location filename="dialogelencoprotocolli.cpp" line="548"/>
        <location filename="dialogelencoprotocolli.cpp" line="576"/>
        <location filename="dialogelencoprotocolli.cpp" line="612"/>
        <location filename="dialogelencoprotocolli.cpp" line="648"/>
        <location filename="dialogelencoprotocolli.cpp" line="672"/>
        <location filename="dialogelencoprotocolli.cpp" line="686"/>
        <location filename="dialogelencoprotocolli.cpp" line="700"/>
        <location filename="dialogelencoprotocolli.cpp" line="734"/>
        <location filename="dialogelencoprotocolli.cpp" line="762"/>
        <location filename="dialogelencoprotocolli.cpp" line="821"/>
        <location filename="dialogelencoprotocolli.cpp" line="849"/>
        <location filename="dialogelencoprotocolli.cpp" line="886"/>
        <location filename="dialogelencoprotocolli.cpp" line="914"/>
        <location filename="dialogelencoprotocolli.cpp" line="973"/>
        <location filename="dialogelencoprotocolli.cpp" line="993"/>
        <location filename="dialogelencoprotocolli.cpp" line="1021"/>
        <location filename="dialogelencoprotocolli.cpp" line="1092"/>
        <location filename="dialogelencoprotocolli.cpp" line="1129"/>
        <location filename="dialogelencoprotocolli.cpp" line="1157"/>
        <location filename="dialogelencoprotocolli.cpp" line="1194"/>
        <location filename="dialogelencoprotocolli.cpp" line="1231"/>
        <location filename="dialogelencoprotocolli.cpp" line="1251"/>
        <location filename="dialogelencoprotocolli.cpp" line="1279"/>
        <location filename="dialogelencoprotocolli.cpp" line="1374"/>
        <location filename="dialogelencoprotocolli.cpp" line="1402"/>
        <location filename="dialogelencoprotocolli.cpp" line="1483"/>
        <location filename="dialogelencoprotocolli.cpp" line="1511"/>
        <location filename="dialogelencoprotocolli.cpp" line="1569"/>
        <location filename="dialogelencoprotocolli.cpp" line="1592"/>
        <location filename="dialogelencoprotocolli.cpp" line="1620"/>
        <location filename="dialogelencoprotocolli.cpp" line="1656"/>
        <location filename="dialogelencoprotocolli.cpp" line="1717"/>
        <location filename="dialogelencoprotocolli.cpp" line="1731"/>
        <location filename="dialogelencoprotocolli.cpp" line="1745"/>
        <location filename="dialogelencoprotocolli.cpp" line="1759"/>
        <location filename="dialogelencoprotocolli.cpp" line="1796"/>
        <location filename="dialogelencoprotocolli.cpp" line="1884"/>
        <location filename="dialogelencoprotocolli.cpp" line="1908"/>
        <location filename="dialogelencoprotocolli.cpp" line="1959"/>
        <location filename="dialogelencoprotocolli.cpp" line="1973"/>
        <location filename="dialogelencoprotocolli.cpp" line="1987"/>
        <location filename="dialogelencoprotocolli.cpp" line="2038"/>
        <location filename="dialogelencoprotocolli.cpp" line="2066"/>
        <location filename="dialogelencoprotocolli.cpp" line="2132"/>
        <location filename="dialogelencoprotocolli.cpp" line="2160"/>
        <location filename="dialogelencoprotocolli.cpp" line="2225"/>
        <location filename="dialogelencoprotocolli.cpp" line="2253"/>
        <location filename="dialogelencoprotocolli.cpp" line="2332"/>
        <location filename="dialogelencoprotocolli.cpp" line="2397"/>
        <source>RESISTIVO</source>
        <translation>RESISTIV</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="278"/>
        <location filename="dialogelencoprotocolli.cpp" line="306"/>
        <location filename="dialogelencoprotocolli.cpp" line="342"/>
        <location filename="dialogelencoprotocolli.cpp" line="370"/>
        <location filename="dialogelencoprotocolli.cpp" line="392"/>
        <location filename="dialogelencoprotocolli.cpp" line="428"/>
        <location filename="dialogelencoprotocolli.cpp" line="442"/>
        <location filename="dialogelencoprotocolli.cpp" line="477"/>
        <location filename="dialogelencoprotocolli.cpp" line="505"/>
        <location filename="dialogelencoprotocolli.cpp" line="562"/>
        <location filename="dialogelencoprotocolli.cpp" line="590"/>
        <location filename="dialogelencoprotocolli.cpp" line="626"/>
        <location filename="dialogelencoprotocolli.cpp" line="714"/>
        <location filename="dialogelencoprotocolli.cpp" line="748"/>
        <location filename="dialogelencoprotocolli.cpp" line="776"/>
        <location filename="dialogelencoprotocolli.cpp" line="835"/>
        <location filename="dialogelencoprotocolli.cpp" line="863"/>
        <location filename="dialogelencoprotocolli.cpp" line="900"/>
        <location filename="dialogelencoprotocolli.cpp" line="928"/>
        <location filename="dialogelencoprotocolli.cpp" line="950"/>
        <location filename="dialogelencoprotocolli.cpp" line="1007"/>
        <location filename="dialogelencoprotocolli.cpp" line="1035"/>
        <location filename="dialogelencoprotocolli.cpp" line="1078"/>
        <location filename="dialogelencoprotocolli.cpp" line="1106"/>
        <location filename="dialogelencoprotocolli.cpp" line="1143"/>
        <location filename="dialogelencoprotocolli.cpp" line="1171"/>
        <location filename="dialogelencoprotocolli.cpp" line="1208"/>
        <location filename="dialogelencoprotocolli.cpp" line="1265"/>
        <location filename="dialogelencoprotocolli.cpp" line="1293"/>
        <location filename="dialogelencoprotocolli.cpp" line="1337"/>
        <location filename="dialogelencoprotocolli.cpp" line="1351"/>
        <location filename="dialogelencoprotocolli.cpp" line="1388"/>
        <location filename="dialogelencoprotocolli.cpp" line="1416"/>
        <location filename="dialogelencoprotocolli.cpp" line="1438"/>
        <location filename="dialogelencoprotocolli.cpp" line="1462"/>
        <location filename="dialogelencoprotocolli.cpp" line="1497"/>
        <location filename="dialogelencoprotocolli.cpp" line="1525"/>
        <location filename="dialogelencoprotocolli.cpp" line="1606"/>
        <location filename="dialogelencoprotocolli.cpp" line="1634"/>
        <location filename="dialogelencoprotocolli.cpp" line="1670"/>
        <location filename="dialogelencoprotocolli.cpp" line="1782"/>
        <location filename="dialogelencoprotocolli.cpp" line="1820"/>
        <location filename="dialogelencoprotocolli.cpp" line="1870"/>
        <location filename="dialogelencoprotocolli.cpp" line="2052"/>
        <location filename="dialogelencoprotocolli.cpp" line="2080"/>
        <location filename="dialogelencoprotocolli.cpp" line="2146"/>
        <location filename="dialogelencoprotocolli.cpp" line="2174"/>
        <location filename="dialogelencoprotocolli.cpp" line="2239"/>
        <location filename="dialogelencoprotocolli.cpp" line="2304"/>
        <location filename="dialogelencoprotocolli.cpp" line="2318"/>
        <location filename="dialogelencoprotocolli.cpp" line="2383"/>
        <source>CAPACITIVO</source>
        <translation>KAPAZITIVE</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="301"/>
        <source>STATICO</source>
        <translation>STATISCH</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="320"/>
        <location filename="dialogelencoprotocolli.cpp" line="334"/>
        <location filename="dialogelencoprotocolli.cpp" line="362"/>
        <location filename="dialogelencoprotocolli.cpp" line="384"/>
        <location filename="dialogelencoprotocolli.cpp" line="406"/>
        <location filename="dialogelencoprotocolli.cpp" line="420"/>
        <location filename="dialogelencoprotocolli.cpp" line="434"/>
        <location filename="dialogelencoprotocolli.cpp" line="455"/>
        <location filename="dialogelencoprotocolli.cpp" line="469"/>
        <location filename="dialogelencoprotocolli.cpp" line="497"/>
        <location filename="dialogelencoprotocolli.cpp" line="640"/>
        <location filename="dialogelencoprotocolli.cpp" line="664"/>
        <location filename="dialogelencoprotocolli.cpp" line="678"/>
        <location filename="dialogelencoprotocolli.cpp" line="692"/>
        <location filename="dialogelencoprotocolli.cpp" line="706"/>
        <location filename="dialogelencoprotocolli.cpp" line="726"/>
        <location filename="dialogelencoprotocolli.cpp" line="740"/>
        <location filename="dialogelencoprotocolli.cpp" line="768"/>
        <location filename="dialogelencoprotocolli.cpp" line="878"/>
        <location filename="dialogelencoprotocolli.cpp" line="892"/>
        <location filename="dialogelencoprotocolli.cpp" line="942"/>
        <location filename="dialogelencoprotocolli.cpp" line="985"/>
        <location filename="dialogelencoprotocolli.cpp" line="999"/>
        <location filename="dialogelencoprotocolli.cpp" line="1027"/>
        <location filename="dialogelencoprotocolli.cpp" line="1121"/>
        <location filename="dialogelencoprotocolli.cpp" line="1186"/>
        <location filename="dialogelencoprotocolli.cpp" line="1200"/>
        <location filename="dialogelencoprotocolli.cpp" line="1243"/>
        <location filename="dialogelencoprotocolli.cpp" line="1257"/>
        <location filename="dialogelencoprotocolli.cpp" line="1285"/>
        <location filename="dialogelencoprotocolli.cpp" line="1366"/>
        <location filename="dialogelencoprotocolli.cpp" line="1380"/>
        <location filename="dialogelencoprotocolli.cpp" line="1430"/>
        <location filename="dialogelencoprotocolli.cpp" line="1454"/>
        <location filename="dialogelencoprotocolli.cpp" line="1475"/>
        <location filename="dialogelencoprotocolli.cpp" line="1489"/>
        <location filename="dialogelencoprotocolli.cpp" line="1517"/>
        <location filename="dialogelencoprotocolli.cpp" line="1584"/>
        <location filename="dialogelencoprotocolli.cpp" line="1951"/>
        <location filename="dialogelencoprotocolli.cpp" line="1965"/>
        <location filename="dialogelencoprotocolli.cpp" line="1979"/>
        <location filename="dialogelencoprotocolli.cpp" line="2030"/>
        <location filename="dialogelencoprotocolli.cpp" line="2044"/>
        <location filename="dialogelencoprotocolli.cpp" line="2058"/>
        <location filename="dialogelencoprotocolli.cpp" line="2072"/>
        <location filename="dialogelencoprotocolli.cpp" line="2124"/>
        <location filename="dialogelencoprotocolli.cpp" line="2138"/>
        <location filename="dialogelencoprotocolli.cpp" line="2152"/>
        <location filename="dialogelencoprotocolli.cpp" line="2166"/>
        <location filename="dialogelencoprotocolli.cpp" line="2217"/>
        <location filename="dialogelencoprotocolli.cpp" line="2310"/>
        <source>supino</source>
        <translation>träge</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="321"/>
        <location filename="dialogelencoprotocolli.cpp" line="349"/>
        <location filename="dialogelencoprotocolli.cpp" line="407"/>
        <location filename="dialogelencoprotocolli.cpp" line="456"/>
        <location filename="dialogelencoprotocolli.cpp" line="484"/>
        <location filename="dialogelencoprotocolli.cpp" line="727"/>
        <location filename="dialogelencoprotocolli.cpp" line="755"/>
        <location filename="dialogelencoprotocolli.cpp" line="966"/>
        <location filename="dialogelencoprotocolli.cpp" line="986"/>
        <location filename="dialogelencoprotocolli.cpp" line="1014"/>
        <location filename="dialogelencoprotocolli.cpp" line="1244"/>
        <location filename="dialogelencoprotocolli.cpp" line="1272"/>
        <location filename="dialogelencoprotocolli.cpp" line="1476"/>
        <location filename="dialogelencoprotocolli.cpp" line="1504"/>
        <source>diaframma</source>
        <translation>Membran</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="408"/>
        <location filename="dialogelencoprotocolli.cpp" line="422"/>
        <location filename="dialogelencoprotocolli.cpp" line="436"/>
        <source>sulla lesione</source>
        <translation>auf Verletzung</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="287"/>
        <location filename="dialogelencoprotocolli.cpp" line="323"/>
        <location filename="dialogelencoprotocolli.cpp" line="409"/>
        <location filename="dialogelencoprotocolli.cpp" line="423"/>
        <location filename="dialogelencoprotocolli.cpp" line="437"/>
        <location filename="dialogelencoprotocolli.cpp" line="458"/>
        <location filename="dialogelencoprotocolli.cpp" line="571"/>
        <location filename="dialogelencoprotocolli.cpp" line="585"/>
        <location filename="dialogelencoprotocolli.cpp" line="667"/>
        <location filename="dialogelencoprotocolli.cpp" line="729"/>
        <location filename="dialogelencoprotocolli.cpp" line="844"/>
        <location filename="dialogelencoprotocolli.cpp" line="858"/>
        <location filename="dialogelencoprotocolli.cpp" line="988"/>
        <location filename="dialogelencoprotocolli.cpp" line="1246"/>
        <location filename="dialogelencoprotocolli.cpp" line="1346"/>
        <location filename="dialogelencoprotocolli.cpp" line="1478"/>
        <location filename="dialogelencoprotocolli.cpp" line="1564"/>
        <source>STATICO grande</source>
        <translation>große STATIC</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="128"/>
        <source>LOMBALGIA</source>
        <translation>Rückenschmerzen</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="129"/>
        <source>LOMBOSCIALTAGIA SUB ACUTA</source>
        <translation>Lenden-Sciatalgie - Subakut</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="130"/>
        <source>PERIARTRITE SPALLA FASE ACUTA (DINAMICI)</source>
        <translation>Periarthritis der Splitter - akute Phase (Dynamische Elektroden)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="131"/>
        <source>PERIARTRITE SPALLA FASE ACUTA (STATICI)</source>
        <translation>Periarthritis der Splitter - akute Phase - (Statische Elektroden)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="132"/>
        <source>PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (DINAMICI)</source>
        <translation>Periarthritis der Splitter -subakut (Dynamische Elektroden)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="133"/>
        <source>PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (STATICI)</source>
        <translation>Periarthritis der Splitter -subakut - (Statische Elektroden)</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="134"/>
        <source>SPALLA FASE ACUTA</source>
        <translation>
Schulter akute Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="136"/>
        <source>SPALLA FASE SUB CUTA</source>
        <translation>subakute Schulter</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="137"/>
        <source>EPICONDILITE DA SOVRACCARICO</source>
        <translation>
Epikondilit</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="138"/>
        <source>POLSO FASE ACUTA</source>
        <translation>
Puls akute Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="139"/>
        <source>POLSO FASE SUB  ACUTA O CRONICA</source>
        <translation>
Pulsphase subakut und chronisch</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="140"/>
        <source>COXARTROSI SUB ACUTA E CRONICA</source>
        <translation>Sub akute und chronische Coxarthrose</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="141"/>
        <source>DISTORSIONE CAVIGLIA FASE ACUTA</source>
        <translation>akute Phase der Knöchelverstauchung</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="142"/>
        <source>DISTORSIONE CAVIGLIA FASE SUB ACUTA</source>
        <translation>Knöchelstauchungs phase subakut</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="162"/>
        <source>GINOCCHIO POST CHIRURGICO</source>
        <translation>Postoperatives Knie </translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="164"/>
        <source>PRIMA FASE - DRENAGGIO</source>
        <translation>Postoperatives Knie  - Erste Entwässerungsphase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="165"/>
        <source>PRIMA FASE - ANTINFIAMMATORIO</source>
        <translation>Postoperatives Knie - Erste Entzündungshemmende Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="166"/>
        <source>SECONDA FASE - MOBILIZZAZZIONE PASSIVA</source>
        <translation>Postoperatives Knie - Passive Mobilisierung der zweiten Phase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="167"/>
        <source>TERZA FASE - MOBILIZZAZZIONE ATTIVA</source>
        <translation>Postoperatives Knie.- Dritte aktive Mobilisierungsphase</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="168"/>
        <source>QUARTA FASE - MOBILIZZAZZIONE ATTIVA CON RESISTENZA</source>
        <translation>Postoperatives Knie - Vierte Phase Aktive Mobilisierung mit Widerstand</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="169"/>
        <source>QUINTA FASE - RECUPERO DEFICIT DI FLESSIONE</source>
        <translation>Postoperatives Knie -Fünfte Phase Erholung Biegedefizit</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="170"/>
        <source>SESTA FASE - RECUPERO DEFICIT IN FLESSO/ESTENSIONE</source>
        <translation>Postoperatives Knie - sechste Phase Wiederherstellung des Flexions- / Dehnungsdefizits</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="172"/>
        <source>SESTA FASE - RECUPERO MOBILITA ARTICOLARE</source>
        <translation>Postoperatives Knie - sechste Phase Wiederherstellung der Gelenkmobilität</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="204"/>
        <source>TRATTAMENTO DELLE LESIONI SPINALI PERIFERICHE</source>
        <translation>
Behandlung von peripheren Wirbelsäulenverletzungen</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="226"/>
        <source>PERIARTRITE DELLA SPALLA</source>
        <translation>Shoulder periarthritis</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="227"/>
        <source>SPONDILOSI CERVICALE</source>
        <translation>cervical spondylosis</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="228"/>
        <source>ERNIA DEL DISCO LOMBARE</source>
        <translation>lumbar disc herniation</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="229"/>
        <source>SFORZO MUSCOLARE</source>
        <translation>muscle strain</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="230"/>
        <source>CUSTOM</source>
        <translation>CUSTOM</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="336"/>
        <location filename="dialogelencoprotocolli.cpp" line="471"/>
        <location filename="dialogelencoprotocolli.cpp" line="742"/>
        <location filename="dialogelencoprotocolli.cpp" line="1001"/>
        <location filename="dialogelencoprotocolli.cpp" line="1259"/>
        <location filename="dialogelencoprotocolli.cpp" line="1491"/>
        <source>muscolo psoas</source>
        <translation>Psoas muscle</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="350"/>
        <location filename="dialogelencoprotocolli.cpp" line="485"/>
        <location filename="dialogelencoprotocolli.cpp" line="756"/>
        <location filename="dialogelencoprotocolli.cpp" line="1015"/>
        <location filename="dialogelencoprotocolli.cpp" line="1273"/>
        <location filename="dialogelencoprotocolli.cpp" line="1505"/>
        <source>L1-L4 e piliforme</source>
        <translation>L1 - L5 und piriformis</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="364"/>
        <location filename="dialogelencoprotocolli.cpp" line="499"/>
        <location filename="dialogelencoprotocolli.cpp" line="770"/>
        <location filename="dialogelencoprotocolli.cpp" line="1029"/>
        <location filename="dialogelencoprotocolli.cpp" line="1287"/>
        <location filename="dialogelencoprotocolli.cpp" line="1519"/>
        <source>zona lombare e glutei</source>
        <translation>Lumbaler Bereich und Gesäß</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="421"/>
        <location filename="dialogelencoprotocolli.cpp" line="694"/>
        <location filename="dialogelencoprotocolli.cpp" line="2032"/>
        <location filename="dialogelencoprotocolli.cpp" line="2060"/>
        <location filename="dialogelencoprotocolli.cpp" line="2126"/>
        <location filename="dialogelencoprotocolli.cpp" line="2154"/>
        <source>polso</source>
        <translation>Handgelenk</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="435"/>
        <location filename="dialogelencoprotocolli.cpp" line="2297"/>
        <location filename="dialogelencoprotocolli.cpp" line="2376"/>
        <location filename="dialogelencoprotocolli.cpp" line="2391"/>
        <source>pianta del piede</source>
        <translation>Sohle</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="606"/>
        <source>inserzione nervo sciatico fino irradiazione dolore</source>
        <translation>Insertion  sciatic nerve until  where arrive pain</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="620"/>
        <source>muscoli paravertebrali, glutei, bicipiti femorali, polpacci e tibiali</source>
        <translation>paravertebral muscles, gluteus, femural biceps, calf and  tibial muscle</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1587"/>
        <source>RESISTIVO piccolo</source>
        <translation>Resistive klein</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1649"/>
        <location filename="dialogelencoprotocolli.cpp" line="1663"/>
        <location filename="dialogelencoprotocolli.cpp" line="1901"/>
        <source>bicipite femorale</source>
        <translation>Oberschenkel Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1650"/>
        <source>inserzione vasto mediale,vasto laterale,retto femorale</source>
        <translation>Große mediale Insertion, weites laterales, femorales Rektum</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1776"/>
        <location filename="dialogelencoprotocolli.cpp" line="1790"/>
        <location filename="dialogelencoprotocolli.cpp" line="1864"/>
        <location filename="dialogelencoprotocolli.cpp" line="1878"/>
        <source>tricipite brachiale,sovraspinato,infraspinato,trapezio,pettorali,bicipite brachiale</source>
        <translation>Brachialis Trizeps, Supraspinatus, Infraspinatus, Trapezius, Brustmuskel, Brachii Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1814"/>
        <source>bicipite femorale,polpaccio,tibiale</source>
        <translation>Biceps femoris, Kalb, Tibia</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1953"/>
        <source>braccio e avambraccio</source>
        <translation>Arm und Unterarm</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1967"/>
        <source>inserzione tendini</source>
        <translation>Sehnen</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2218"/>
        <source>lombo sacrale</source>
        <translation>Sakralrücken</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2219"/>
        <source>zona trocanterica e ischiatica</source>
        <translation>Trochanter- und Ischiaszone</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2231"/>
        <source>supina</source>
        <translation>träge</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2247"/>
        <source>gluteo e bicipite femorale</source>
        <translation>Gluteus und Bizeps femoris</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2298"/>
        <location filename="dialogelencoprotocolli.cpp" line="2377"/>
        <source>gastrocnemius mediale e laterale e soleo</source>
        <translation>Medialer und lateraler Gastrocnemius, Soleus</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2311"/>
        <location filename="dialogelencoprotocolli.cpp" line="2390"/>
        <source>gastrocnemius</source>
        <translation>Gastrocnemius</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2312"/>
        <source>dorso del piede</source>
        <translation>Rückseite des Fußes</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2325"/>
        <source>gastrocnemius pos</source>
        <translation>Gastrocnemius</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2326"/>
        <source>pianta del piede, soleo e gastrocnemius laterale</source>
        <translation>Fußpflanze, Soleus und lateraler Gastrocnemius</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="665"/>
        <location filename="dialogelencoprotocolli.cpp" line="679"/>
        <location filename="dialogelencoprotocolli.cpp" line="693"/>
        <location filename="dialogelencoprotocolli.cpp" line="707"/>
        <source>zona cervico/dorsale</source>
        <translation>Bereich Gebärmutterhalskrebs / Rücken</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="666"/>
        <source>muscolo retto femorale</source>
        <translation>rectus femoris</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="680"/>
        <source>articolazione gleno omerale</source>
        <translation>humeral Artikulation gleno</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="387"/>
        <location filename="dialogelencoprotocolli.cpp" line="643"/>
        <location filename="dialogelencoprotocolli.cpp" line="681"/>
        <location filename="dialogelencoprotocolli.cpp" line="695"/>
        <location filename="dialogelencoprotocolli.cpp" line="945"/>
        <location filename="dialogelencoprotocolli.cpp" line="968"/>
        <location filename="dialogelencoprotocolli.cpp" line="1101"/>
        <location filename="dialogelencoprotocolli.cpp" line="1124"/>
        <location filename="dialogelencoprotocolli.cpp" line="1152"/>
        <location filename="dialogelencoprotocolli.cpp" line="1189"/>
        <location filename="dialogelencoprotocolli.cpp" line="1203"/>
        <location filename="dialogelencoprotocolli.cpp" line="1226"/>
        <location filename="dialogelencoprotocolli.cpp" line="1433"/>
        <location filename="dialogelencoprotocolli.cpp" line="1615"/>
        <location filename="dialogelencoprotocolli.cpp" line="1665"/>
        <location filename="dialogelencoprotocolli.cpp" line="1754"/>
        <location filename="dialogelencoprotocolli.cpp" line="1903"/>
        <location filename="dialogelencoprotocolli.cpp" line="1982"/>
        <location filename="dialogelencoprotocolli.cpp" line="2061"/>
        <location filename="dialogelencoprotocolli.cpp" line="2155"/>
        <location filename="dialogelencoprotocolli.cpp" line="2392"/>
        <source>STATICO piccolo</source>
        <translation>kleine STATIC</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="604"/>
        <location filename="dialogelencoprotocolli.cpp" line="618"/>
        <source>decubito laterale prono</source>
        <translation>Seitlicher seitlicher Dekubitus</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="641"/>
        <location filename="dialogelencoprotocolli.cpp" line="1187"/>
        <source>gadtrocnemius</source>
        <translation>Gastrocnemius</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="642"/>
        <location filename="dialogelencoprotocolli.cpp" line="944"/>
        <location filename="dialogelencoprotocolli.cpp" line="1188"/>
        <location filename="dialogelencoprotocolli.cpp" line="1202"/>
        <location filename="dialogelencoprotocolli.cpp" line="1432"/>
        <location filename="dialogelencoprotocolli.cpp" line="1664"/>
        <location filename="dialogelencoprotocolli.cpp" line="1813"/>
        <location filename="dialogelencoprotocolli.cpp" line="1902"/>
        <source>retto femorale</source>
        <translation>Femoral-Rektum</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="708"/>
        <source>arto superiore plegico</source>
        <translation>gelähmte Arm</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1149"/>
        <location filename="dialogelencoprotocolli.cpp" line="1612"/>
        <location filename="dialogelencoprotocolli.cpp" line="1648"/>
        <location filename="dialogelencoprotocolli.cpp" line="1662"/>
        <location filename="dialogelencoprotocolli.cpp" line="1774"/>
        <location filename="dialogelencoprotocolli.cpp" line="1788"/>
        <location filename="dialogelencoprotocolli.cpp" line="1862"/>
        <location filename="dialogelencoprotocolli.cpp" line="1876"/>
        <location filename="dialogelencoprotocolli.cpp" line="1900"/>
        <source>seduto</source>
        <translation>Sitzung</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="965"/>
        <location filename="dialogelencoprotocolli.cpp" line="1223"/>
        <source>confortevole</source>
        <translation>komfortabel</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="967"/>
        <source>compressione intervertebrale</source>
        <translation>Banddruck</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="335"/>
        <location filename="dialogelencoprotocolli.cpp" line="363"/>
        <location filename="dialogelencoprotocolli.cpp" line="385"/>
        <location filename="dialogelencoprotocolli.cpp" line="470"/>
        <location filename="dialogelencoprotocolli.cpp" line="498"/>
        <location filename="dialogelencoprotocolli.cpp" line="741"/>
        <location filename="dialogelencoprotocolli.cpp" line="769"/>
        <location filename="dialogelencoprotocolli.cpp" line="893"/>
        <location filename="dialogelencoprotocolli.cpp" line="943"/>
        <location filename="dialogelencoprotocolli.cpp" line="1000"/>
        <location filename="dialogelencoprotocolli.cpp" line="1028"/>
        <location filename="dialogelencoprotocolli.cpp" line="1201"/>
        <location filename="dialogelencoprotocolli.cpp" line="1258"/>
        <location filename="dialogelencoprotocolli.cpp" line="1286"/>
        <location filename="dialogelencoprotocolli.cpp" line="1381"/>
        <location filename="dialogelencoprotocolli.cpp" line="1431"/>
        <location filename="dialogelencoprotocolli.cpp" line="1490"/>
        <location filename="dialogelencoprotocolli.cpp" line="1518"/>
        <source>zona lombare</source>
        <translation>den unteren Rücken</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="879"/>
        <location filename="dialogelencoprotocolli.cpp" line="1367"/>
        <location filename="dialogelencoprotocolli.cpp" line="1952"/>
        <location filename="dialogelencoprotocolli.cpp" line="1966"/>
        <location filename="dialogelencoprotocolli.cpp" line="1980"/>
        <source>scapola</source>
        <translation>Schulterblatt</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="880"/>
        <location filename="dialogelencoprotocolli.cpp" line="1368"/>
        <source>deltoide e bicipiti brachiali</source>
        <translation>Deltoideus und Oberarm Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="894"/>
        <location filename="dialogelencoprotocolli.cpp" line="1382"/>
        <source>pettorale e bicipiti brachiali</source>
        <translation>Brust- und Oberarm Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="907"/>
        <location filename="dialogelencoprotocolli.cpp" line="1122"/>
        <location filename="dialogelencoprotocolli.cpp" line="1395"/>
        <location filename="dialogelencoprotocolli.cpp" line="1585"/>
        <location filename="dialogelencoprotocolli.cpp" line="2031"/>
        <location filename="dialogelencoprotocolli.cpp" line="2045"/>
        <location filename="dialogelencoprotocolli.cpp" line="2059"/>
        <location filename="dialogelencoprotocolli.cpp" line="2073"/>
        <location filename="dialogelencoprotocolli.cpp" line="2125"/>
        <location filename="dialogelencoprotocolli.cpp" line="2139"/>
        <location filename="dialogelencoprotocolli.cpp" line="2153"/>
        <location filename="dialogelencoprotocolli.cpp" line="2167"/>
        <location filename="dialogelencoprotocolli.cpp" line="2232"/>
        <source>braccio</source>
        <translation>Arm</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="908"/>
        <location filename="dialogelencoprotocolli.cpp" line="1396"/>
        <source>sotto scapolare e scapolare</source>
        <translation>Unter dem Skapulier und Skapulier</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="920"/>
        <location filename="dialogelencoprotocolli.cpp" line="1135"/>
        <location filename="dialogelencoprotocolli.cpp" line="1163"/>
        <location filename="dialogelencoprotocolli.cpp" line="1408"/>
        <location filename="dialogelencoprotocolli.cpp" line="1598"/>
        <location filename="dialogelencoprotocolli.cpp" line="1626"/>
        <source>su di un lato</source>
        <translation>Auf der einen Seite</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="921"/>
        <location filename="dialogelencoprotocolli.cpp" line="1136"/>
        <location filename="dialogelencoprotocolli.cpp" line="1164"/>
        <location filename="dialogelencoprotocolli.cpp" line="1409"/>
        <location filename="dialogelencoprotocolli.cpp" line="1599"/>
        <location filename="dialogelencoprotocolli.cpp" line="1627"/>
        <source>gomito</source>
        <translation>Ellbogen</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="922"/>
        <location filename="dialogelencoprotocolli.cpp" line="1137"/>
        <location filename="dialogelencoprotocolli.cpp" line="1165"/>
        <location filename="dialogelencoprotocolli.cpp" line="1410"/>
        <location filename="dialogelencoprotocolli.cpp" line="1600"/>
        <location filename="dialogelencoprotocolli.cpp" line="1628"/>
        <source>deltoide e zona scapolare</source>
        <translation>Deltoideus und Schulterblattbereich</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1123"/>
        <location filename="dialogelencoprotocolli.cpp" line="1586"/>
        <source>zona scapolare</source>
        <translation>Skapulierbereich</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1151"/>
        <location filename="dialogelencoprotocolli.cpp" line="1614"/>
        <source>bicipite brachiale</source>
        <translation>Bizeps Bizeps</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1394"/>
        <location filename="dialogelencoprotocolli.cpp" line="2245"/>
        <source>prona</source>
        <translation>anfällig</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1455"/>
        <source>lombare</source>
        <translation>Lenden-</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1456"/>
        <source>muscoli denervati</source>
        <translation>denervierten Muskeln</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1224"/>
        <source>plesso brachiale posteriore</source>
        <translation>Hinten Plexus brachialis</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1225"/>
        <source>plesso brachiale anteriore</source>
        <translation>Plexus brachialis vorne</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1150"/>
        <location filename="dialogelencoprotocolli.cpp" line="1613"/>
        <location filename="dialogelencoprotocolli.cpp" line="1981"/>
        <location filename="dialogelencoprotocolli.cpp" line="2046"/>
        <location filename="dialogelencoprotocolli.cpp" line="2074"/>
        <location filename="dialogelencoprotocolli.cpp" line="2140"/>
        <location filename="dialogelencoprotocolli.cpp" line="2168"/>
        <source>avambraccio</source>
        <translation>Unterarm</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="322"/>
        <location filename="dialogelencoprotocolli.cpp" line="457"/>
        <location filename="dialogelencoprotocolli.cpp" line="728"/>
        <location filename="dialogelencoprotocolli.cpp" line="987"/>
        <location filename="dialogelencoprotocolli.cpp" line="1245"/>
        <location filename="dialogelencoprotocolli.cpp" line="1477"/>
        <source>lombosacrale</source>
        <translation>lumbosacral</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2233"/>
        <source>coscia</source>
        <translation>Schenkel</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="386"/>
        <source>cavo popliteo</source>
        <translation>Kabel poplitea</translation>
    </message>
</context>
<context>
    <name>dialogMenuGuida</name>
    <message>
        <location filename="dialogmenuguida.cpp" line="37"/>
        <source>PROTOCOLLI</source>
        <translation>PROTOKOLLE</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="38"/>
        <source>GENERICI</source>
        <translation>GENERIC</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="39"/>
        <location filename="dialogmenuguida.cpp" line="41"/>
        <source>PROTOCOLLI PATOLOGIE</source>
        <translation>PROTOKOLLE KRANKHEITEN</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="40"/>
        <source>NEUROLOGICI</source>
        <translation>NEUROLOGISCHE</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="42"/>
        <source>ORTOPEDICI</source>
        <translation>ORTHOPEDIC</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="44"/>
        <source>Descrizione protocolli generici</source>
        <translation>Terapeuci Protokolle für die analgetische Behandlungen mit statischen Elektroden mit Physiotherapie</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="45"/>
        <source>Descrizione protocolli neurologici</source>
        <translation>terapeuci Protokolle von orthopädischen Erkrankungen unterteilt nur statische Elektroden</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="46"/>
        <source>Descrizione protocolli ortopedici</source>
        <translation>Protokolle für die Behandlung von Patienten mit neurologischen Erkrankungen</translation>
    </message>
</context>
<context>
    <name>dialogSelezionaProgramma</name>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="58"/>
        <location filename="dialogselezionaprogramma.cpp" line="59"/>
        <location filename="dialogselezionaprogramma.cpp" line="60"/>
        <location filename="dialogselezionaprogramma.cpp" line="61"/>
        <source>Posizione Neutro :</source>
        <translation>Neutrale Position:</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="53"/>
        <location filename="dialogselezionaprogramma.cpp" line="54"/>
        <location filename="dialogselezionaprogramma.cpp" line="55"/>
        <location filename="dialogselezionaprogramma.cpp" line="56"/>
        <source>Posizione Paziente :</source>
        <translation>Position des Patienten:</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="63"/>
        <location filename="dialogselezionaprogramma.cpp" line="64"/>
        <location filename="dialogselezionaprogramma.cpp" line="65"/>
        <location filename="dialogselezionaprogramma.cpp" line="66"/>
        <source>Posizione Elettrodo :</source>
        <translation>Elektroden Lage :</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="68"/>
        <location filename="dialogselezionaprogramma.cpp" line="69"/>
        <location filename="dialogselezionaprogramma.cpp" line="70"/>
        <location filename="dialogselezionaprogramma.cpp" line="71"/>
        <source>Elettrodo</source>
        <translation>ELEKTRODE</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="84"/>
        <location filename="dialogselezionaprogramma.cpp" line="164"/>
        <location filename="dialogselezionaprogramma.cpp" line="189"/>
        <location filename="dialogselezionaprogramma.cpp" line="214"/>
        <location filename="dialogselezionaprogramma.cpp" line="239"/>
        <source>POTENZA</source>
        <translation>POWER</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="85"/>
        <location filename="dialogselezionaprogramma.cpp" line="165"/>
        <location filename="dialogselezionaprogramma.cpp" line="190"/>
        <location filename="dialogselezionaprogramma.cpp" line="215"/>
        <location filename="dialogselezionaprogramma.cpp" line="240"/>
        <source>MODALITA</source>
        <translation>MODUS</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="86"/>
        <location filename="dialogselezionaprogramma.cpp" line="166"/>
        <location filename="dialogselezionaprogramma.cpp" line="167"/>
        <location filename="dialogselezionaprogramma.cpp" line="191"/>
        <location filename="dialogselezionaprogramma.cpp" line="192"/>
        <location filename="dialogselezionaprogramma.cpp" line="216"/>
        <location filename="dialogselezionaprogramma.cpp" line="217"/>
        <location filename="dialogselezionaprogramma.cpp" line="241"/>
        <location filename="dialogselezionaprogramma.cpp" line="242"/>
        <source>FREQUENZA</source>
        <translation>FREQUENZ</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="87"/>
        <location filename="dialogselezionaprogramma.cpp" line="168"/>
        <location filename="dialogselezionaprogramma.cpp" line="169"/>
        <location filename="dialogselezionaprogramma.cpp" line="170"/>
        <location filename="dialogselezionaprogramma.cpp" line="193"/>
        <location filename="dialogselezionaprogramma.cpp" line="194"/>
        <location filename="dialogselezionaprogramma.cpp" line="195"/>
        <location filename="dialogselezionaprogramma.cpp" line="218"/>
        <location filename="dialogselezionaprogramma.cpp" line="219"/>
        <location filename="dialogselezionaprogramma.cpp" line="220"/>
        <location filename="dialogselezionaprogramma.cpp" line="243"/>
        <location filename="dialogselezionaprogramma.cpp" line="244"/>
        <location filename="dialogselezionaprogramma.cpp" line="245"/>
        <source>SLF</source>
        <translation>SLF</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="163"/>
        <location filename="dialogselezionaprogramma.cpp" line="188"/>
        <location filename="dialogselezionaprogramma.cpp" line="213"/>
        <location filename="dialogselezionaprogramma.cpp" line="238"/>
        <source>TEMPO</source>
        <translation>ZEIT</translation>
    </message>
</context>
<context>
    <name>dialogSelezionaabcd</name>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="42"/>
        <source>CAPACITIVO DINAMICO</source>
        <translation>kapazitive dynamische</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="43"/>
        <source>CAPACITIVO STATICO GRANDE</source>
        <translation>große statische Kapazität</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="44"/>
        <source>CAPACITIVO STATICO PICCOLO</source>
        <translation>kleine statische Kapazität</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="45"/>
        <source>RESISTIVO DINAMICO</source>
        <translation>dynamische Widerstands</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="46"/>
        <source>RESISTIVO STATICO GRANDE</source>
        <translation>große statische Widerstands</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="47"/>
        <source>RESISTIVO STATICO PICCOLO</source>
        <translation>kleine statische Widerstands</translation>
    </message>
</context>
<context>
    <name>dialogSetup</name>
    <message>
        <location filename="dialogsetup.ui" line="47"/>
        <source>Setup</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="47"/>
        <source>Tipo 1</source>
        <translation>CT200</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="48"/>
        <source>Tipo 2</source>
        <translation>CT250</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="49"/>
        <source>Tipo 3</source>
        <translation>CT300</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="51"/>
        <source>Tipo di macchina</source>
        <translation>Maschinentyp</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="98"/>
        <source>Vecchia password</source>
        <translation>Altes Passwort</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="110"/>
        <source>Nuova password</source>
        <translation>Neues Kennwort</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="113"/>
        <source>Riscrivi password</source>
        <translation>Passwort wiederholen</translation>
    </message>
</context>
<context>
    <name>dialogStartCalibrazione</name>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="31"/>
        <source>PROCEDURA DI CALIBRAZIONE</source>
        <translation>KALIBRIERVERFAHREN</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="32"/>
        <source>Info calibrazione</source>
        <translation>Schließen Sie das Handstück kapazitive und resistive nicht eingesetzt Elektroden. Legen Sie eine Hand die Masseelektrode. Starten Sie das Kalibrierungsverfahren</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="79"/>
        <source>Vecchia password</source>
        <translation>Altes Passwort</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="92"/>
        <source>Nuova password</source>
        <translation>Neues Kennwort</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="95"/>
        <source>Riscrivi password</source>
        <translation>Passwort wiederholen</translation>
    </message>
</context>
<context>
    <name>dialogUser</name>
    <message>
        <location filename="dieloguser.cpp" line="79"/>
        <source>CALIBRA</source>
        <translation>SIZING</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="80"/>
        <source>OROLOGIO</source>
        <translation>UHR</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="142"/>
        <source>Vecchia password</source>
        <translation>Altes Passwort</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="154"/>
        <source>Nuova password</source>
        <translation>Neues Kennwort</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="157"/>
        <source>Riscrivi password</source>
        <translation>Passwort wiederholen</translation>
    </message>
</context>
<context>
    <name>dielogUser</name>
    <message>
        <source>CALIBRA</source>
        <translation type="obsolete">SIZING</translation>
    </message>
    <message>
        <source>OROLOGIO</source>
        <translation type="obsolete">UHR</translation>
    </message>
    <message>
        <source>Vecchia password</source>
        <translation type="obsolete">Altes Passwort</translation>
    </message>
    <message>
        <source>Nuova password</source>
        <translation type="obsolete">Neues Kennwort</translation>
    </message>
    <message>
        <source>Riscrivi password</source>
        <translation type="obsolete">Passwort wiederholen</translation>
    </message>
</context>
<context>
    <name>ufgOrologio</name>
    <message>
        <location filename="ufgorologio.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
</TS>
