#ifndef UFGBUTTON_H
#define UFGBUTTON_H

#include <QWidget>
#include <QLabel>
#include <QPixmap>

class ufgButton : public QWidget
{
    Q_OBJECT
public:
    explicit ufgButton(QWidget *parent = 0);

    void setImage(QString normal, QString press, QString focus, int scale );
    void setBordoEnable(bool en);
    void setBordoDimensione( int dim)
    {
        dimensioneBordo = dim;
    }
    void setBordoColors( QColor normal, QColor press, QColor focus)
    {
        colorNormal = normal;
        colorFocus  = focus;
        colorPress  = press;
    }

private:

    QLabel* immagine;

    QPixmap imageNormal;
    QPixmap imagePress;
    QPixmap imageFocus;

    QColor colorNormal;
    QColor colorPress;
    QColor colorFocus;

    void paintEvent(QPaintEvent *e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void focusInEvent(QFocusEvent* e);
    void focusOutEvent(QFocusEvent* e);

    bool bordo;
    bool focus;
    bool premuto;

    int scala;
    int dimensioneBordo;
public slots:

signals:
    void buttonClicked();
};

#endif // UFGBUTTON_H
