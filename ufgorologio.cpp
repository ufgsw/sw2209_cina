#include "ufgorologio.h"
#include "ui_ufgorologio.h"

#include <QtGui>
#include <QProcess>

ufgOrologio::ufgOrologio(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ufgOrologio)
{
    ui->setupUi(this);

    mAutoUpdate = true;

    time = QTime::currentTime();

    dateTime = QDateTime::currentDateTime();

    mOre     = time.hour();
    mMinuti  = time.minute();
    mSecondi = time.second();

    timer = new QTimer(this);
    connect( timer, SIGNAL(timeout()), this, SLOT(ridisegna()) );

}

ufgOrologio::~ufgOrologio()
{
    delete ui;
    timer->stop();

    delete timer;
}

void ufgOrologio::showEvent(QShowEvent *event)
{
    timer->start(1000);
}


void ufgOrologio::setOrario(int ore, int minuti, int secondi)
{
    time = QTime( ore, minuti, secondi );

    this->update();
}

void ufgOrologio::salva()
{
    QString     cmdData = QString( "date" ); // +%T -s" "10:13:13" )
    QString     cmdSave = "hwclock";
    QStringList argumentData;
    QStringList argumentSave;

    QString sdata = QString("%1:%2:%3").arg(mOre).arg(mMinuti).arg(mSecondi);
    argumentData << "+%T" << "-s" << sdata;

    argumentSave << "--systohc";

    QProcess *myProcess = new QProcess(this);


    myProcess->start( cmdData, argumentData);

    myProcess->waitForFinished(1000);
    qDebug( myProcess->readAll() );

    myProcess->start( cmdSave, argumentSave );

    myProcess->waitForFinished(1000);
    qDebug( myProcess->readAll() );

}

void ufgOrologio::paintEvent(QPaintEvent *e)
{
    static const QPoint hourHand[3] =
   {
           QPoint( 4, 6),
           QPoint(-4, 6),
           QPoint(0, -40)
   };
   static const QPoint minuteHand[3] =
   {
       QPoint( 4, 6),
       QPoint(-4, 6),
       QPoint(0, -70)
   };
   static const QPoint secondsHand[3] =
   {
       QPoint( 2, 4),
       QPoint(-2, 4),
       QPoint(0, -80)
   };

   QColor hourColor(127, 0, 127);
   QColor minuteColor(0, 127, 127, 191);
   QColor secondsColor( 255, 0,0 );

   int side = qMin(width(), height());
   //QTime time = QTime::currentTime();

   QPainter painter(this);
   painter.setRenderHint(QPainter::Antialiasing);
   painter.translate(width() / 2, height() / 2); // porto 0,0  al centro
   painter.scale( side / 200.0, side / 200.0);

   // Disegno la lancetta delle ore
   painter.setPen(Qt::NoPen);
   painter.setBrush(hourColor);

   painter.save();
   painter.rotate(30.0 * ( mOre + mMinuti / 60.0));
   painter.drawConvexPolygon(hourHand, 3);
   painter.restore();

   painter.setPen(hourColor);

   // Disegno le tacchette delle ore
   for (int i = 0; i < 12; ++i)
   {
       painter.drawLine(88, 0, 96, 0);
       painter.rotate(30.0);   // step di 30 gradi ogni ora
   }

   // Disegno la lancetta dei minuti
   painter.setPen(Qt::NoPen);
   painter.setBrush(minuteColor);

   painter.save();
   painter.rotate(6.0 * (mMinuti + mSecondi / 60.0));
   painter.drawConvexPolygon(minuteHand, 3);
   painter.restore();

   painter.setPen(minuteColor);

   // Disegno le tacchette dei minuti
   for (int j = 0; j < 60; ++j)
   {
       if ((j % 5) != 0) painter.drawLine(92, 0, 96, 0);

       painter.rotate(6.0);    // step di 6 gradi ogni minuto
   }

   // Disegno la lancetta dei secondi
   painter.setPen(Qt::NoPen);
   painter.setBrush(secondsColor);

   painter.save();
   painter.rotate(6.0 * ( mSecondi ));
   painter.drawConvexPolygon( secondsHand, 3 );
   painter.restore();

   //Qtrasform t;
}


void ufgOrologio::ridisegna()
{
    time = time.addSecs( 1 );
    if( mAutoUpdate == true )
    {
        mOre     = time.hour();
        mMinuti  = time.minute();
        mSecondi = time.second();




        this->update();
    }
}
