#ifndef TASTIERINONUMERICO_H
#define TASTIERINONUMERICO_H

#include <QDialog>
#include "ddmbutton.h"

namespace Ui {
class tastierinoNumerico;
}

class tastierinoNumerico : public QDialog
{
    Q_OBJECT

public:
    explicit tastierinoNumerico(QWidget *parent = 0);
    ~tastierinoNumerico();

    void setTitle( QString title);
    void setPasswordMode( bool mode ) { passwordMode = mode; }
    QString getText();

private:
    Ui::tastierinoNumerico *ui;
    void paintEvent(QPaintEvent *);

    bool primoTasto;
    bool passwordMode;

    QString testo;

private slots:
    void tastoPremuto(ddmButton* tasto);

signals:
    void endEdit(QString testo);
};

#endif // TASTIERINONUMERICO_H
