#include "diatermia.h"

#include <QFileSystemModel>
#include <QProcess>


parametri::parametri()
{
    calibra = false;
}

void parametri::read()
{
    QFile File("/home/root/myfile.json");

    if( File.exists() == true )
    {
        File.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = File.readAll();
        jsonDoc    = QJsonDocument::fromJson(saveData);
        jParametri = jsonDoc.object();
        File.close();
//        jProgrammi = jParametri["programmi"].toArray();
    }
    else
    {
        // DEFAULT
        jParametri["release"]               = RELEASE_FW;
        jParametri["tipoMacchina"]          = 1;
        jParametri["lingua"]                = "cin";
        jParametri["password setup"]        = "0000";
        jParametri["password calibrazione"] = "0000";

        jsonDoc.setObject(jParametri);

        File.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
        File.write( jsonDoc.toJson() );
        File.close();

    }
    mioProgramma.id                     = 1;
    mioProgramma.descrizione            = "err";
    mioProgramma.nome                   = "err";
    mioProgramma.note                   = "err";
    mioProgramma.tipopTrattamnento      = "capacitivo";
    mioProgramma.tipoElettrodi          = "dinamici";
    mioProgramma.frequenza              = 460;
    mioProgramma.frequenzaPemf          = 300;
    mioProgramma.potenza                = 5;
    mioProgramma.tempo                  = 10;
    mioProgramma.sonda.val              = 0;
    mioProgramma.sonda.tipo.capacitivo_dinamico = 1;

    lingua            = jParametri["lingua"].toString();
    tipoMacchina      = jParametri["tipoMacchina"].toInt();
    passwordSetup     = jParametri["password setup"].toString();
    passwordCalibra   = jParametri["password calibrazione"].toString();
    release           = jParametri["release"].toString();

    if( release != RELEASE_FW )
    {
        save();
    }


}

void parametri::save()
{
    QFile File("/home/root/myfile.json");
    if( File.exists() == true )
    {
        File.remove();
        jParametri["release"]               = RELEASE_FW;
        jParametri["tipoMacchina"]          = tipoMacchina;
        jParametri["lingua"]                = lingua;
        jParametri["password setup"]        = passwordSetup ;
        jParametri["password calibrazione"] = passwordCalibra;

        jsonDoc.setObject(jParametri);

        File.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
        File.write( jsonDoc.toJson() );
        File.close();

        qDebug( "parametri salvati " );
    }

}
