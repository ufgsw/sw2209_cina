#include "dieloguser.h"
#include "ui_dieloguser.h"
#include <QProcess>

dialogUser::dialogUser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dielogUser)
{
    ui->setupUi(this);

    //ui->buttonItalia->setBordoDimensione(2);
    //ui->buttonInglese->setBordoDimensione(2);
    //ui->buttonTedesco->setBordoDimensione(2);
    //ui->buttonCinese->setBordoDimensione(2);
    ui->buttonPassword->setBordoDimensione(2);
    ui->buttonExit->setBordoDimensione(2);

    ui->buttonItalia->setVisible(false);
    ui->buttonCinese->setVisible(false);
    ui->buttonInglese->setVisible(false);
    ui->buttonTedesco->setVisible(false);
    ui->boxLingua->setVisible(false);

//    ui->buttonItalia->setImage(     ":/img/flag/italia-128.png",":/img/flag/italia-128.png",":/img/flag/italia-128.png", 100);
//    ui->buttonInglese->setImage(    ":/img/flag/UK-128.png",":/img/flag/UK-128.png",":/img/flag/UK-128.png", 100);
//    ui->buttonTedesco->setImage(    ":/img/flag/germania-128.png",":/img/flag/germania-128.png",":/img/flag/germania-128.png", 100);
//    ui->buttonCinese->setImage(     ":/img/flag/cina-128.png",":/img/flag/cina-128.png",":/img/flag/cina-128.png", 100);
    ui->buttonExit->setImage(       ":/img/ico/return.png", ":/img/ico/return.png", ":/img/ico/return.png" , 100 );
    //ui->buttonCalibra->setImage(    ":/img/ico/mixer.png" , ":/img/ico/mixer.png" , ":/img/ico/mixer.png" , 100 );

    config = NULL;

    connect( ui->buttonItalia,  SIGNAL(buttonClicked()), this, SLOT(buttonItalia_clicked()));
    connect( ui->buttonInglese, SIGNAL(buttonClicked()), this, SLOT(buttonInglese_clicked()));
    connect( ui->buttonTedesco, SIGNAL(buttonClicked()), this, SLOT(buttonTedesco_clicked()));
    connect( ui->buttonCinese,  SIGNAL(buttonClicked()), this, SLOT(buttonCinese_clicked()));

    connect( ui->buttonOrologio, SIGNAL(buttonClicked()), this, SLOT(showOrologio()) );
    connect( ui->buttonCalibra,  SIGNAL(buttonClicked()), this, SLOT(showCalibra()));
    connect( ui->buttonPassword, SIGNAL(buttonClicked()), this, SLOT(selezionatoPassword()));
    connect( ui->buttonExit,     SIGNAL(buttonClicked()), this, SLOT(exit()));

    formCalibra = new dialogStartCalibrazione(this);
    formOrologio = new DialogOrologio(this);

}

dialogUser::~dialogUser()
{
    delete ui;
    delete formCalibra;
    delete formOrologio;
}

void dialogUser::exit()
{
    config->save();

    QProcess *mount = new QProcess(this);
    mount->start("sync");
    mount->waitForFinished(-1);

    emit onClose();
    close();
}

void dialogUser::setParametri(parametri *conf)
{
    config = conf;
    if( config != NULL )
    {
        if(      config->lingua == ITALIANO  ) this->buttonItalia_clicked();
        else if( config->lingua == INGLESE   ) this->buttonInglese_clicked();
        else if( config->lingua == CINESE    ) this->buttonCinese_clicked();
        else if( config->lingua == TEDESCO   ) this->buttonTedesco_clicked();

        if( config->passwordCalibra != "0000" )
            ui->buttonPassword->setImage( ":/img/ico/lock.png" , ":/img/ico/lock.png" , ":/img/ico/lock.png" , 100 );
        else
            ui->buttonPassword->setImage( ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , 100 );

    }
    else qDebug("Err config = NULL");

    ui->buttonCalibra->setText(tr("CALIBRA"));
    ui->buttonOrologio->setText( tr("OROLOGIO"));

}

void dialogUser::setRotella(Rotella *_rotella)
{
    mRotella = _rotella;
    //disconnect( mRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    //connect(    mRotella,SIGNAL(valueChange(bool)), ui->progressBar, SLOT(rotellaMove(bool)));
    mRotella->setEnableBeep( false );
}

void dialogUser::buttonItalia_clicked()
{
    ui->buttonItalia->setBordoEnable(true);
    ui->buttonInglese->setBordoEnable(false);
    ui->buttonCinese->setBordoEnable(false);
    ui->buttonTedesco->setBordoEnable(false);

    config->lingua = ITALIANO;
}

void dialogUser::buttonTedesco_clicked()
{
    ui->buttonItalia->setBordoEnable(false);
    ui->buttonInglese->setBordoEnable(false);
    ui->buttonCinese->setBordoEnable(false);
    ui->buttonTedesco->setBordoEnable(true);

    config->lingua = TEDESCO;
}

void dialogUser::buttonInglese_clicked()
{
    ui->buttonItalia->setBordoEnable(false);
    ui->buttonInglese->setBordoEnable(true);
    ui->buttonCinese->setBordoEnable(false);
    ui->buttonTedesco->setBordoEnable(false);

    config->lingua = INGLESE;
}

void dialogUser::buttonCinese_clicked()
{
    ui->buttonItalia->setBordoEnable(false);
    ui->buttonInglese->setBordoEnable(false);
    ui->buttonCinese->setBordoEnable(true);
    ui->buttonTedesco->setBordoEnable(false);

    config->lingua = CINESE;
}

void dialogUser::selezionatoPassword()
{
    QString psw1;
    QString psw2;
    tastierinoNumerico* tast;

    if( config->passwordCalibra != "0000" )
    {
        tast = new tastierinoNumerico(this);
        tast->setGeometry( 200, 40, 400,400 );
        tast->setTitle( tr("Vecchia password"));
        tast->exec();
        if( tast->getText() == config->passwordCalibra || tast->getText() == "1247" )
        {
            config->passwordCalibra = "0000";
            ui->buttonPassword->setImage( ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , 100 );
        }
    }
    else
    {
        tast = new tastierinoNumerico(this);
        tast->setGeometry( 200, 40, 400,400 );
        tast->setTitle( tr("Nuova password"));
        tast->exec();
        psw1 = tast->getText();
        tast->setTitle( tr("Riscrivi password"));
        tast->exec();
        psw2 = tast->getText();

        if( psw1.length() > 0 && psw2.length() > 0 )
        {
            if( psw1 == psw2 ) {
                config->passwordCalibra = psw1;
                ui->buttonPassword->setImage( ":/img/ico/lock.png" , ":/img/ico/lock.png" , ":/img/ico/lock.png" , 100 );
            }
        }
    }
    if( tast != NULL ) delete tast;
}

void dialogUser::showCalibra()
{
    formCalibra->showNormal();
    connect( formCalibra, SIGNAL(onStartCalibra()), this, SLOT(exeCalibra()) );
}

void dialogUser::exeCalibra()
{
    config->calibra = true;
    this->exit();
}

void dialogUser::showOrologio()
{
    formOrologio->exec();
}
