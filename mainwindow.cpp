#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tastierinonumerico.h"
//#include "dialogsetlingua.h"

//#include <QSqlDatabase>
//#include <QSqlTableModel>
//#include <QSqlQuery>
#include <QCoreApplication>
#include <QTranslator>

#include <QFileSystemModel>
#include <QTimer>
#include <QTreeView>
#include <QPixmap>
#include <QInputDialog>
#include <QProcess>

#include <QTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    config         = new parametri();
    config->read();

    setLingua( "cina", false );

    ui->setupUi(this);
    font.setPixelSize(40);

    //font.setFamily("wqy-zenhei");
    //QApplication::setFont(font);

    //QString cin = "模式";
    //QString cin = "稍";
    //QString cin = QChar( 1000 );

    //ui->label_3->setFont(font);
    //ui->label_3->setText( cin );

    ui->buttonTrattamento->setTitle(tr("MODALITA'"));
    ui->buttonFrequenza->setTitle(tr("FREQUENZA"));
    ui->buttonPemf->setTitle(tr("FREQUENZA LOW"));
    ui->buttonPotenza->setTitle(tr("POTENZA"));
    ui->buttonTempo->setTitle(tr("TEMPO"));
    ui->buttonGuida->setText(tr("GUIDA"));
    ui->buttonInfo->setText(tr("INFO"));
    ui->buttonCalibra->setText(tr("IMPOSTAZIONI"));

    ui->buttonStart->setImage( ":/img/ico/start.png",":/img/ico/start.png",":/img/ico/start.png", 100);
    //ui->buttonSetup->setImage( ":/img/tecnobody.png",":/img/tecnobody.png",":/img/tecnobody.png", 100);
    ui->buttonSetup->setBordoEnable(false);
    //ui->buttonTecnobody->setBordoEnable(false);
    //ui->buttonTecnobody->setImage( ":/img/tecnobody.png",":/img/tecnobody.png",":/img/tecnobody.png", 100);

    rectInterno.setRect( 230, 70, 340, 380 );

    formInfo = new dialogInfo(this);
    //formInfo->setGeometry( 150,20,500,440);
    formInfo->setGeometry(  rectInterno );

    formSetVar = new dialogSetVar(this);
    formSetVar->setGeometry( rectInterno );
    //formSetVar->setGeometry( 150,20,500,440);

    formUser = new dialogUser(this);
    formUser->setGeometry( 0,0,800,480 );


    formCalibra       = NULL;
    formSelezionaAB   = NULL;
    formSelezionaABC  = NULL;
    formSelezionaABCD = NULL;

//    formGuida = new dialogMenuGuida(this);
//    formGuida->setGeometry(0,0,800,480);

    formElencoProtocolli = new dialogElencoProtocolli(this);
    formElencoProtocolli->setGeometry( 0,0,800,480 );

    formSetup = new dialogSetup(this,config);
    formSetup->setGeometry(0,0,800,480);

    tastierino = new tastierinoNumerico(this);
    tastierino->setGeometry( 200, 40, 400,400 );


    miaRotella = new Rotella();
    miaRotella->delegate = this;

    //connect( ui->buttonTecnobody,   SIGNAL(buttonClicked()), this, SLOT(richiediPassword()));
    connect( ui->buttonTrattamento, SIGNAL(buttonClicked()), this, SLOT(getTrattamento()) );
    connect( ui->buttonPotenza,     SIGNAL(buttonClicked()), this, SLOT(getPotenza()) );
    connect( ui->buttonFrequenza,   SIGNAL(buttonClicked()), this, SLOT(getFrequenza()));
    connect( ui->buttonPemf,        SIGNAL(buttonClicked()), this, SLOT(getFrequenzaPemf()));
    connect( ui->buttonTempo,       SIGNAL(buttonClicked()), this, SLOT(getTempo()));
    connect( ui->buttonStart,       SIGNAL(buttonClicked()), this, SLOT(startStop()));
    connect( ui->buttonGuida,       SIGNAL(buttonClicked()), this, SLOT(showMenuGuida()));
    connect( ui->buttonInfo,        SIGNAL(buttonClicked()), this, SLOT(showInfo()));
    connect( ui->buttonCalibra,     SIGNAL(buttonClicked()), this, SLOT(showCalibra()));
    connect( ui->buttonSetup,       SIGNAL(buttonClicked()), this, SLOT(showSetup()));

    connect( formInfo, SIGNAL(onClose()), this, SLOT(riprendoRotella()));

    connect( formElencoProtocolli, SIGNAL(onclose()), this, SLOT(riprendoRotella()));
    connect( formElencoProtocolli, SIGNAL(caricaProgramma(sProgramma)), this, SLOT(caricaProgramma(sProgramma)));

    connect( formSetup, SIGNAL(onClose()), this, SLOT(newConfig()));
    connect( formUser,  SIGNAL(onClose()), this, SLOT(newConfig()));
    //connect( formSetup, SIGNAL(onCalibra()), this, SLOT(calibraMacchina()));

    //connect( formElencoProtocolli, SIGNAL(caricaProgramma(sProgramma)), this,SLOT(caricaProgramma(sProgramma)));
    connect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    connect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));
    connect( miaRotella, SIGNAL(beep(int)),         this, SLOT(setCicala(int)));

    connect( this, SIGNAL(allarmeInCorso()), this, SLOT(showAllarme()));
    connect( this, SIGNAL(warningInCorso()), this, SLOT(showWarning()));

    seriale = new ufgSeriale(this);
    seriale->setBinaryMode( false );
    seriale->carattereIniziale = '$';
    seriale->openPort();
    if( seriale->isOpen )
    {
        connect( seriale, SIGNAL(rxLine(char*,int)), this, SLOT(procSerialRx(char*,int)));
    }


    rs485 = new ufgSeriale(this);
    rs485->baudRate = B9600;
    rs485->setPortname( "/dev/ttymxc7" );
    rs485->setBinaryMode( false );
    rs485->setEnable485(  true );
    rs485->carattereIniziale = '#';
    rs485->openPort();

    if( rs485->isOpen )
    {
        connect( rs485, SIGNAL(rxLine(char*,int)), this, SLOT(proc485Rx(char*,int)) );
    }

    QTimer *timer1 = new QTimer(this);
    connect(timer1, SIGNAL(timeout()), this, SLOT(comTask()));
    timer1->start(100);

    QTimer *timer2 = new QTimer(this);
    connect(timer2, SIGNAL(timeout()), this, SLOT(refreshTask()));
    timer2->start(250);

//    MONTO UNA BELLA CHIAVETTA :)
//    QProcess *mount = new QProcess(this);
//    mount->start("mount", QStringList() << "/dev/sda1" << "/mnt");
//    mount->waitForFinished(-1);

    QString rel = tr("Versione");
    QString rl = QString( "%1 %2").arg(rel).arg( RELEASE_FW );
    ui->labelRelease->setText( rl );

    if( config->tipoMacchina == CT200 ) {
        ui->labelTipoMacchina->setText( "CT200");
        config->mioProgramma.frequenzaPemf = 300;
    }
    if( config->tipoMacchina == CT250 ) ui->labelTipoMacchina->setText( "CT250");
    if( config->tipoMacchina == CT300 ) ui->labelTipoMacchina->setText( "CT300");

    comando               = 0;
    cicala                = 0;
    stato                 = IN_STOP;
    tempoRun              = 0;
    ingressi              = 0;
    allarmi.val           = 0;
    allarmiOld.val        = 0;
    warning.val           = 0;
    warningOld.val        = 0;
    fermoPerWarning       = false;
    allarmeComunicazione  = 20;
    sondaCollegataOld.val = 0;
    counterPC             = 0;
    vco3                  = 0;

//    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
//    if( db.isValid() ) qDebug("DB OK");
//    else               qDebug("DB NOT OK");

    setParametri( &config->mioProgramma );

    formWarning = new dialogWarning(this);
    formWarning->setGeometry( rectInterno );

     // setLingua("cina", true);
     // this->ui->retranslateUi(this);

    QProcess *proc = new QProcess(this);
    proc->start("sync");
    proc->waitForFinished( 1000 );

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setLingua(QString lingua, bool reload)
{
    bool ok = false;

    if( reload == true ) qApp->removeTranslator( &traduttore);

    if( lingua == "it" )
    {
        qDebug( "Italiano");
        ok = traduttore.load("diatermia_it.qm","/home/root/");
    }
    else if(lingua == "en")
    {
        qDebug( "Inglese");
        ok = traduttore.load("diatermia_en.qm","/home/root/");
    }
    else if(lingua == "de")
    {
        qDebug( "Tedesco");
        ok = traduttore.load("diatermia_de.qm","/home/root/");
    }
    else if(lingua == "cina")
    {
        qDebug( "Cinese");
        ok = traduttore.load("diatermia_cina.qm","/home/root/");
    }
    if(  ok == false )
    {
        qDebug("Errore set lingue");
    }

    qApp->installTranslator(&traduttore);

    if( reload == true ) setScritte();
}

void MainWindow::setParametri( sProgramma* par )
{
    config->mioProgramma.descrizione   = par->descrizione;
    config->mioProgramma.elettrodo     = par->elettrodo;
    config->mioProgramma.enable        = par->enable;
    config->mioProgramma.frequenza     = par->frequenza;
    config->mioProgramma.frequenzaPemf = par->frequenzaPemf;
    config->mioProgramma.potenza       = par->potenza;
    config->mioProgramma.sonda.val     = par->sonda.val;
    config->mioProgramma.tempo         = par->tempo;

    if( config->tipoMacchina == CT200 ) par->frequenzaPemf = 300;

    setTrattamento();

    if( config->mioProgramma.frequenza == 460 )
    {
        ui->buttonFrequenza->setText("0.46 MHz");
        frequenza450khz = true;
        frequenza680khz = false;
    }
    else
    {
        ui->buttonFrequenza->setText("0.69 MHz");
        frequenza450khz = false;
        frequenza680khz = true;
    }

    if( config->mioProgramma.frequenzaPemf == 100 )
    {
        pemf100Hz = true;
        pemf200Hz = false;
        pemf300Hz = false;
        ui->buttonPemf->setText("100 Hz");
    }
    else if( config->mioProgramma.frequenzaPemf == 200)
    {
        pemf100Hz = false;
        pemf200Hz = true;
        pemf300Hz = false;
        ui->buttonPemf->setText("200 Hz");
    }
    else if( config->mioProgramma.frequenzaPemf == 300)
    {
        pemf100Hz = false;
        pemf200Hz = false;
        pemf300Hz = true;
        ui->buttonPemf->setText("300 Hz");
    }
    QString spemf = QString( "%1 Hz").arg(config->mioProgramma.frequenzaPemf);
    ui->buttonPemf->setText(spemf);

    if(      config->mioProgramma.potenza < 1   ) config->mioProgramma.potenza = 1;
    else if( config->mioProgramma.potenza > 100 ) config->mioProgramma.potenza = 100;

    QString spotenza = QString("%1 %").arg(config->mioProgramma.potenza);
    ui->buttonPotenza->setText( spotenza);
    int mm,ss;
    mm = config->mioProgramma.tempo / 60;
    ss = config->mioProgramma.tempo - ( mm * 60);
    QString stempo = QString("%1:%2 min").arg(mm,2,10,QLatin1Char('0')).arg(ss,2,10,QLatin1Char('0'));
    ui->buttonTempo->setText(stempo);
}


void MainWindow::refreshParametri()
{
    if( config->mioProgramma.sonda.tipo.capacitivo_dinamico )
    {
        QPixmap pix = QPixmap(":/img/ico/dinamico_capacitivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("CAPACITIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("DINAMICO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.capacitivo_statico_grande )
    {
        QPixmap pix = QPixmap(":/img/ico/statico_capacitivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("CAPACITIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO GRANDE").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.capacitivo_statico_piccolo )
    {
        QPixmap pix = QPixmap(":/img/ico/statico_capacitivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("CAPACITIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO PICCOLO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_dinamico )
    {
        QPixmap pix = QPixmap(":/img/ico/dinamico_resistivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("DINAMICO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_statico_grande )
    {
        QPixmap pix = QPixmap(":/img/ico/statico_resistivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO GRANDE").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_statico_piccolo )
    {
        QPixmap pix = QPixmap(":/img/ico/statico_resistivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO PICCOLO").toUpper());
    }
    sondaCollegataOld.val = 0;

    if( config->mioProgramma.frequenza == 460 )
    {
        ui->buttonFrequenza->setText("0.46 MHz");
        frequenza450khz = true;
        frequenza680khz = false;
    }
    else
    {
        ui->buttonFrequenza->setText("0.69 MHz");
        frequenza450khz = false;
        frequenza680khz = true;
    }

    if( config->mioProgramma.frequenzaPemf == 100 )
    {
        pemf100Hz = true;
        pemf200Hz = false;
        pemf300Hz = false;
        ui->buttonPemf->setText("100 Hz");
    }
    else if( config->mioProgramma.frequenzaPemf == 200)
    {
        pemf100Hz = false;
        pemf200Hz = true;
        pemf300Hz = false;
        ui->buttonPemf->setText("200 Hz");
    }
    else if( config->mioProgramma.frequenzaPemf == 300)
    {
        pemf100Hz = false;
        pemf200Hz = false;
        pemf300Hz = true;
        ui->buttonPemf->setText("300 Hz");
    }
    QString spemf = QString( "%1 Hz").arg(config->mioProgramma.frequenzaPemf);
    ui->buttonPemf->setText(spemf);

    if(      config->mioProgramma.potenza < 1   ) config->mioProgramma.potenza = 1;
    else if( config->mioProgramma.potenza > 100 ) config->mioProgramma.potenza = 100;

    QString spotenza = QString("%1 %").arg(config->mioProgramma.potenza);
    ui->buttonPotenza->setText( spotenza);
    int mm,ss;
    mm = config->mioProgramma.tempo / 60;
    ss = config->mioProgramma.tempo - ( mm * 60);
    QString stempo = QString("%1:%2 min").arg(mm,2,10,QLatin1Char('0')).arg(ss,2,10,QLatin1Char('0'));
    ui->buttonTempo->setText(stempo);
}

void MainWindow::startStop()
{
    if( stato == IN_STOP || stato == IN_PAUSA ) {
        startEsecuzione();
    }
    else {
        if( tempoRun == 0 ) stopEsecuzione();
        else                pausaEsecuzione();
    }
}

void MainWindow::startEsecuzione()
{
//    QString err = "sonda = " + QString::number( sondaCollegata.val ) +  QString::number( config->mioProgramma.sonda.val );
//    qDebug( err.toLatin1() );

    bool errsonda = false;
    if( config->mioProgramma.sonda.tipo.capacitivo_dinamico        && sondaCollegata.tipo.capacitivo_dinamico == 0) errsonda = true;
    if( config->mioProgramma.sonda.tipo.capacitivo_old             && sondaCollegata.tipo.capacitivo_old == 0     ) errsonda = true;
    if( config->mioProgramma.sonda.tipo.capacitivo_statico_grande  && sondaCollegata.tipo.capacitivo_statico_grande == 0 ) errsonda = true;
    if( config->mioProgramma.sonda.tipo.capacitivo_statico_piccolo && sondaCollegata.tipo.capacitivo_statico_piccolo == 0 ) errsonda = true;
    if( config->mioProgramma.sonda.tipo.resistivo_dinamico         && sondaCollegata.tipo.resistivo_dinamico == 0 ) errsonda = true;
    if( config->mioProgramma.sonda.tipo.resistivo_old              && sondaCollegata.tipo.resistivo_old == 0 ) errsonda = true;
    if( config->mioProgramma.sonda.tipo.resistivo_statico_grande   && sondaCollegata.tipo.resistivo_statico_grande == 0) errsonda = true;
    if( config->mioProgramma.sonda.tipo.resistivo_statico_piccolo  && sondaCollegata.tipo.resistivo_statico_piccolo == 0 ) errsonda = true;


    if( errsonda )
    {
        mioWarning.val = W_ELETTRODO_SCOLLEGATO;
        emit warningInCorso();
        return;
    }
    else mioWarning.val = 0;

    if( warning.val != 0 )
    {
        warningOld.val = warning.val;
        emit warningInCorso();
        return;
    }

    ui->buttonStart->setImage(  ":/img/ico/pause.png",":/img/ico/pause.png",":/img/ico/pause.png", 100);

    if( stato == IN_STOP ) tempoRun  = config->mioProgramma.tempo * 10;
    stato = IN_RUN;

    ui->buttonTrattamento->setEnabled(false);
    ui->buttonCalibra->setEnabled(false);
    ui->buttonFrequenza->setEnabled(false);
    ui->buttonPemf->setEnabled(false);
    ui->buttonPotenza->setEnabled(true);
    ui->buttonTempo->setEnabled(false);
    ui->buttonGuida->setEnabled(false);
    ui->buttonInfo->setEnabled(false);
    ui->buttonSetup->setEnabled(false);
}

void MainWindow::stopEsecuzione()
{
    ui->buttonStart->setImage(  ":/img/ico/start.png",":/img/ico/start.png",":/img/ico/start.png", 100);

    stato = IN_STOP;
    setTempo( config->mioProgramma.tempo );
    tempoRun = 0;

    ui->buttonTrattamento->setEnabled(true);
    ui->buttonCalibra->setEnabled(true);
    ui->buttonFrequenza->setEnabled(true);
    ui->buttonPemf->setEnabled(true);
    ui->buttonPotenza->setEnabled(true);
    ui->buttonTempo->setEnabled(true);
    ui->buttonGuida->setEnabled(true);
    ui->buttonInfo->setEnabled(true);
    ui->buttonSetup->setEnabled(true);

    if( formSetVar->isVisible() ) formSetVar->close();
}

void MainWindow::pausaEsecuzione()
{
    ui->buttonStart->setImage(  ":/img/ico/start.png",":/img/ico/start.png",":/img/ico/start.png", 100);

    stato = IN_PAUSA;

    ui->buttonTrattamento->setEnabled(true);
    ui->buttonFrequenza->setEnabled(true);
    ui->buttonPemf->setEnabled(true);
    ui->buttonPotenza->setEnabled(true);
    ui->buttonTempo->setEnabled(true);
    ui->buttonGuida->setEnabled(true);
    ui->buttonInfo->setEnabled(true);
    ui->buttonCalibra->setEnabled(true);
    ui->buttonSetup->setEnabled(true);
}

void MainWindow::showMenuGuida()
{

    if( config->tipoMacchina == CT200 ) formElencoProtocolli->setLicenzaStart( false );
    else                                formElencoProtocolli->setLicenzaStart( true );

    formElencoProtocolli->setProtocllo( CINA );
    formElencoProtocolli->setRotella( miaRotella );
    formElencoProtocolli->showNormal();


    // Passo l'uso della rotella al secondo form
//    disconnect( miaRotella, SIGNAL(valueChange(bool)),  this, SLOT(rotellaMove(bool)));
//    disconnect( miaRotella, SIGNAL(click()),            this, SLOT(rotellaClick()));
//    connect( miaRotella, SIGNAL(valueChange(bool)), formGuida, SLOT(rotellaMove(bool)));
//    connect( miaRotella, SIGNAL(click()),           formGuida, SLOT(rotellaClick()));

//    miaRotella->delegate = formGuida;
}

void MainWindow::caricaProgramma(sProgramma programma)
{
    //qDebug( "Carico nuovo programma ");
    config->mioProgramma = programma;
    setParametri( &config->mioProgramma );
    riprendoRotella();
}

void MainWindow::showInfo()
{
    formInfo->setTitle(tr("INFO"));
    formInfo->setText( tr("desrizione info" ));
    formInfo->showNormal();

    // Passo l'uso della rotella al secondo form
    disconnect( miaRotella, SIGNAL(valueChange(bool)),  this, SLOT(rotellaMove(bool)));
    disconnect( miaRotella, SIGNAL(click()),            this, SLOT(rotellaClick()));
    connect(    miaRotella, SIGNAL(valueChange(bool)), formInfo, SLOT(rotellaMove(bool)));
    connect(    miaRotella, SIGNAL(click()),           formInfo, SLOT(rotellaClick()));

    miaRotella->delegate = formInfo;
}

void MainWindow::showSetup()
{
    if( config->passwordSetup != "0000" )
    {
        //tastierinoNumerico* tast = new tastierinoNumerico(this);
        //tast->setGeometry( 200, 40, 400,400 );
        tastierino->setTitle( tr("Password"));
        tastierino->exec();
        if( tastierino->getText() == config->passwordSetup || tastierino->getText() == "1247" )
        {
            formSetup->showNormal();
        }
    }
    else formSetup->showNormal();
}

void MainWindow::showCalibra()
{
    allarmeComunicazione = 1000;

    formUser->setRotella( miaRotella );
    formUser->setParametri( config );

    if( config->passwordCalibra != "0000" )
    {
        //tastierinoNumerico* tast = new tastierinoNumerico(this);
        //tast->setGeometry( 200, 40, 400,400 );
        tastierino->setTitle( tr("Password"));
        tastierino->exec();

        if( tastierino->getText() == config->passwordCalibra || tastierino->getText() == "1247" )
        {
            formUser->showNormal();
        }
    }
    else formUser->showNormal();

}

void MainWindow::setTrattamento()
{

    if( config->mioProgramma.sonda.tipo.capacitivo_dinamico )
    {
        QPixmap pix = QPixmap(":/img/ico/dinamico_capacitivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("CAPACITIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("DINAMICO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.capacitivo_statico_grande )
    {
        QPixmap pix = QPixmap(":/img/ico/statico_capacitivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("CAPACITIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO GRANDE").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.capacitivo_statico_piccolo )
    {
        QPixmap pix = QPixmap(":/img/ico/statico_capacitivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("CAPACITIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO PICCOLO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_dinamico )
    {
        QPixmap pix = QPixmap(":/img/ico/dinamico_resistivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("DINAMICO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_statico_grande )
    {
        QPixmap pix = QPixmap(":/img/ico/statico_resistivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO GRANDE").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_statico_piccolo )
    {
        QPixmap pix = QPixmap(":/img/ico/statico_resistivo.png");
        ui->imageModalita->setPixmap(pix);
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO PICCOLO").toUpper());
    }
    sondaCollegataOld.val = 0;
    // Riprendo possesso della rotella su questo form
    this->riprendoRotella();
}

void MainWindow::setFrequenza()
{
    if( frequenza450khz ) {
        config->mioProgramma.frequenza = 460;
        ui->buttonFrequenza->setText("0.46 MHz");
    }
    if( frequenza680khz ) {
        config->mioProgramma.frequenza = 690;
        ui->buttonFrequenza->setText("0.69 MHz");
    }

    this->riprendoRotella();
}

void MainWindow::setFrequenzaPemf()
{
    if( pemf100Hz ) {
        config->mioProgramma.frequenzaPemf = 100;
        ui->buttonPemf->setText("100 Hz");
    }
    if( pemf200Hz ) {
        config->mioProgramma.frequenzaPemf = 200;
        ui->buttonPemf->setText("200 Hz");
    }
    if( pemf300Hz ) {
        config->mioProgramma.frequenzaPemf = 300;
        ui->buttonPemf->setText("300 Hz");
    }

    // Riprendo possesso della rotella su questo form
    this->riprendoRotella();
}



void MainWindow::setPotenza(int val)
{
    if(      val < 1   ) val = 1;
    else if( val > 100 ) val = 100;

    config->mioProgramma.potenza = val;
    QString spotenza = QString("%1 %").arg(config->mioProgramma.potenza);
    ui->buttonPotenza->setText( spotenza);

}

void MainWindow::incrementaPotenza()
{
    if( config->mioProgramma.potenza < 100 )
    {
        config->mioProgramma.potenza = config->mioProgramma.potenza +1;
        QString spotenza = QString("%1 %").arg(config->mioProgramma.potenza);
        ui->buttonPotenza->setText( spotenza);
    }
}

void MainWindow::decrementaPotenza()
{
    if( config->mioProgramma.potenza > 1 )
    {
        config->mioProgramma.potenza = config->mioProgramma.potenza - 1;
        QString spotenza = QString("%1 %").arg(config->mioProgramma.potenza);
        ui->buttonPotenza->setText( spotenza);
    }
}

/*
void MainWindow::setElettrodi()
{
    if( elettrodiDinamici ) {
        ui->buttonElettrodi->setText(tr("DINAMICI"));
        QPixmap pix = QPixmap(":/img/ico/dinamico.png");
        ui->imageElettrodi->setPixmap(pix);
    }
    if( elettrodiStatici  ) {
        QPixmap pix = QPixmap(":/img/ico/statico.png");
        ui->imageElettrodi->setPixmap(pix);
        ui->buttonElettrodi->setText(tr("STATICI"));
    }
    this->riprendoRotella();
}
*/


void MainWindow::setTempo(int secondi)
{
    int mm,ss;
    config->mioProgramma.tempo = secondi;
    mm = config->mioProgramma.tempo / 60;
    ss = config->mioProgramma.tempo - ( mm * 60);
    QString stempo = QString("%1:%2 min").arg(mm,2,10,QLatin1Char('0')).arg(ss,2,10,QLatin1Char('0'));
    ui->buttonTempo->setText(stempo);

    if( stato == IN_PAUSA )
    {
        tempoRun  = config->mioProgramma.tempo * 10;
    }


    // Riprendo possesso della rotella su questo form
    this->riprendoRotella();
}

void MainWindow::getTempo()
{
    delete formSetVar;

    formSetVar = new dialogSetVar(this);
//    formSetVar->setGeometry( 150,20,500,440);
    formSetVar->setGeometry( rectInterno );

    formSetVar->setTitle( tr("TEMPO"));
    formSetVar->setTipoValore( T_TIME );
    formSetVar->setValoriInt(0, 6000 , 60 , 60 , config->mioProgramma.tempo );
    formSetVar->setRotella(miaRotella);
    connect( formSetVar, SIGNAL(saveValueInt(int)), this, SLOT(setTempo(int)));
    formSetVar->showNormal();

    // Passo l'uso della rotella al secondo form
    disconnect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    disconnect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));
    connect(    miaRotella, SIGNAL(valueChange(bool)), formSetVar, SLOT(rotellaMove(bool)));
    connect(    miaRotella, SIGNAL(click()),           formSetVar, SLOT(rotellaClick()));

    miaRotella->delegate = formSetVar;

}

void MainWindow::getTrattamento()
{

    if( formSelezionaABCD != NULL ) delete formSelezionaABCD;

    formSelezionaABCD = new dialogSelezionaabcd(this);
    formSelezionaABCD->setGeometry( 50, 70, 700, 380 );

    formSelezionaABCD->setParametri( config );

    connect( formSelezionaABCD, SIGNAL(onClose()), this, SLOT(setTrattamento()));
    formSelezionaABCD->setTitle( tr("MODALITA"));
    formSelezionaABCD->show();

    if( stato == IN_PAUSA )
    {
        setPotenza(5);
    }
    // Passo l'uso della rotella al secondo form
    disconnect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    disconnect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));
    connect(    miaRotella, SIGNAL(valueChange(bool)), formSelezionaABCD, SLOT(rotellaMove(bool)));
    connect(    miaRotella, SIGNAL(click()),           formSelezionaABCD, SLOT(rotellaClick()));

    miaRotella->delegate = formSelezionaAB;

    return;
}

void MainWindow::getFrequenza()
{
    if( formSelezionaAB != NULL ) delete formSelezionaAB;

    formSelezionaAB = new dialodSelezionaAB(this);
    formSelezionaAB->setGeometry( rectInterno );

    formSelezionaAB->setVariabileA(&frequenza450khz, "0.46 MHz");
    formSelezionaAB->setVariabileB(&frequenza680khz, "0.69 MHz");
    formSelezionaAB->setTitle(tr("FREQUENZA"));
    connect( formSelezionaAB, SIGNAL(onClose()), this, SLOT(setFrequenza()));
    formSelezionaAB->show();

    // Passo l'uso della rotella al secondo form
    disconnect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    disconnect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));
    connect(    miaRotella, SIGNAL(valueChange(bool)), formSelezionaAB, SLOT(rotellaMove(bool)));
    connect(    miaRotella, SIGNAL(click()),           formSelezionaAB, SLOT(rotellaClick()));

    miaRotella->delegate = formSelezionaAB;
}

void MainWindow::getFrequenzaPemf()
{
    if( config->tipoMacchina != CT200 )
    {
        if( formSelezionaABC != NULL ) delete formSelezionaABC;

        formSelezionaABC = new dialogSelezionaabc(this);
        formSelezionaABC->setGeometry( rectInterno );

        formSelezionaABC->setVariabileA(&pemf100Hz, "100Hz");
        formSelezionaABC->setVariabileB(&pemf200Hz, "200Hz");
        formSelezionaABC->setVariabileC(&pemf300Hz, "300Hz");
        formSelezionaABC->setTitle( tr("FREQUENZA PEMF"));

        connect( formSelezionaABC, SIGNAL(onClose()), this, SLOT( setFrequenzaPemf()));
        formSelezionaABC->show();

        // Passo l'uso della rotella al secondo form
        disconnect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
        disconnect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));
        connect(    miaRotella, SIGNAL(valueChange(bool)), formSelezionaABC, SLOT(rotellaMove(bool)));
        connect(    miaRotella, SIGNAL(click()),           formSelezionaABC, SLOT(rotellaClick()));

        miaRotella->delegate = formSelezionaABC;
    }
}

void MainWindow::getPotenza()
{
    int limite = 100;

    if(      sondaCollegata.tipo.resistivo_statico_piccolo  && config->mioProgramma.sonda.tipo.resistivo_statico_piccolo ) limite = 60;
    else if( sondaCollegata.tipo.resistivo_statico_grande   && config->mioProgramma.sonda.tipo.resistivo_statico_grande  ) limite = 70;
    else if( sondaCollegata.tipo.capacitivo_statico_piccolo && config->mioProgramma.sonda.tipo.capacitivo_statico_piccolo) limite = 60;
    else if( sondaCollegata.tipo.capacitivo_statico_grande  && config->mioProgramma.sonda.tipo.capacitivo_statico_grande ) limite = 70;

    delete formSetVar;
    formSetVar = new dialogSetVar(this);
    formSetVar->setGeometry( rectInterno );
    formSetVar->setTitle( tr("POTENZA"));
    formSetVar->setTipoValore( T_PERCENT );
    formSetVar->setValoriInt(1, limite , 1, 1,  config->mioProgramma.potenza );

    connect( formSetVar, SIGNAL(chiuso()),          this , SLOT(riprendoRotella()));
    connect( formSetVar, SIGNAL(saveValueInt(int)), this,  SLOT(setPotenza(int)));

    if( stato == IN_RUN ) formSetVar->setEnableProgressbar(false);

    formSetVar->setRotella(miaRotella);
    formSetVar->showNormal();

    // Passo l'uso della rotella al secondo form
    disconnect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    disconnect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));
    connect(    miaRotella, SIGNAL(valueChange(bool)), formSetVar, SLOT(rotellaMove(bool)));
    connect(    miaRotella, SIGNAL(click()),           formSetVar, SLOT(rotellaClick()));

    miaRotella->delegate = formSetVar;


}

void MainWindow::getElettrodi()
{
    if( formSelezionaAB != NULL ) delete formSelezionaAB;

    formSelezionaAB = new dialodSelezionaAB(this);
    formSelezionaAB->setGeometry( rectInterno );
    formSelezionaAB->disconnect();
    formSelezionaAB->setVariabileA(&elettrodiDinamici, tr("DINAMICI"));
    formSelezionaAB->setVariabileB(&elettrodiStatici,  tr("STATICI" ));
    connect( formSelezionaAB, SIGNAL(onClose()), this, SLOT(setElettrodi()));
    formSelezionaAB->setTitle("ELETTRODI");
    formSelezionaAB->showNormal();

    // Passo l'uso della rotella al secondo form
    disconnect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    disconnect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));
    connect(    miaRotella, SIGNAL(valueChange(bool)), formSelezionaAB, SLOT(rotellaMove(bool)));
    connect(    miaRotella, SIGNAL(click()),           formSelezionaAB, SLOT(rotellaClick()));
    miaRotella->delegate = formSelezionaAB;
}

void MainWindow::refreshTask()
{
    static int timerAttesa    = 0;
    static int numeroImmagine = 0;
    QPixmap pix;

    if( stato == IN_PAUSA && numeroImmagine < 10 ) numeroImmagine = 10;
    if( stato == IN_STOP  && numeroImmagine < 10 ) numeroImmagine = 11;

    if( timeoutWarning ) --timeoutWarning;
    else
    {
        if( formWarning->isActiveWindow() ) formWarning->close();
    }

    // Refresh immagine
    switch(numeroImmagine)
    {
    case 0:
        pix = QPixmap(":/img/ico/run0.png");
        ui->imageRun->setPixmap(pix);
        numeroImmagine = 1;
        break;
    case 1:
        pix = QPixmap(":/img/ico/run1.png");
        ui->imageRun->setPixmap(pix);
        numeroImmagine = 2;
        break;
    case 2:
        pix = QPixmap(":/img/ico/run2.png");
        ui->imageRun->setPixmap(pix);
        numeroImmagine = 3;
        break;
    case 3:
        pix = QPixmap(":/img/ico/run3.png");
        ui->imageRun->setPixmap(pix);
        numeroImmagine = 4;
        break;
    case 4:
        pix = QPixmap(":/img/ico/run2.png");
        ui->imageRun->setPixmap(pix);
        numeroImmagine = 5;
        break;
    case 5:
        pix = QPixmap(":/img/ico/run1.png");
        ui->imageRun->setPixmap(pix);
        numeroImmagine = 0;
        break;
    case 10:
        pix = QPixmap(":/img/ico/run0.png");
        ui->imageRun->setPixmap(pix);
        numeroImmagine = 20;
        break;
    case 11:
        ui->imageRun->clear();
        numeroImmagine = 20;
        break;
    case 12:
        // Attendo la fine della calibrazione
        if( stato != IN_CALIBRAZIONE )
        {
            formWarning->setText(       tr("CALIBRAZIONE TERMINATA") );
            timerAttesa    = 12;
            numeroImmagine = 13;
        }
        timeoutWarning = 12;
        break;
    case 13:
        if( timerAttesa ) --timerAttesa;
        else
        {
            //formWarning->close();
            numeroImmagine = 20;
        }
        break;
    case 20:
        if( stato == IN_RUN          ) numeroImmagine = 0;
        if( stato == IN_CALIBRAZIONE )
        {
            formWarning->setText(       tr("CALIBRAZIONE IN CORSO") );
            formWarning->showNormal();
            timeoutWarning = 12;
            numeroImmagine = 12;
        }
        break;
    }

    // Refresh orologio
    QTime adesso = QTime::currentTime();
    QString ore,min,sec;
    if( adesso.hour() < 10 ) ore = "0" + QString("%1").arg(adesso.hour());
    else                     ore = QString("%1").arg(adesso.hour());

    if( adesso.minute() < 10 ) min = "0" + QString("%1").arg( adesso.minute() );
    else                       min = QString("%1").arg( adesso.minute() );

    if( adesso.second() < 10 ) sec = "0" + QString("%1").arg( adesso.second() );
    else                       sec = QString("%1").arg( adesso.second() );

    ui->labelOrologio->setText( ore + ":" + min + ":" + sec );


    // Refresh RELEASE + freq
    double freqout = (double)vco3;
    freqout = (freqout * 0.041) + 61;

    /*
    QString rel = tr("Versione");
    QString rl  = QString( "%1 %2 / %3 / %4").arg(rel).arg( RELEASE_FW ).arg(releaseHardware).arg( (int)freqout);
    */

    QString rel = tr("Versione");
    QString rl  = QString( "%1 %2 / %3").arg(rel).arg( RELEASE_FW ).arg(releaseHardware);


    ui->labelRelease->setText( rl );

    // Refresh Controllo sonda collegata
    if( sondaCollegata.val != sondaCollegataOld.val )
    {
        sondaCollegataOld.val = sondaCollegata.val;

        if( config->mioProgramma.sonda.tipo.capacitivo_dinamico )
        {
            if( sondaCollegata.trattamento.capacitivo ) ui->buttonTrattamento->setTextColor( Qt::white );
            else                                        ui->buttonTrattamento->setTextColor( Qt::blue );

            if( sondaCollegata.tipo.capacitivo_dinamico ) ui->buttonTrattamento->setTitleColor( Qt::white );
            else if( sondaCollegata.tipo.capacitivo_old ) ui->buttonTrattamento->setTitleColor( Qt::white );
            else                                          ui->buttonTrattamento->setTitleColor( Qt::blue );
        }
        else if( config->mioProgramma.sonda.tipo.capacitivo_old )
        {
            if( sondaCollegata.trattamento.capacitivo ) ui->buttonTrattamento->setTextColor( Qt::white );
            else                                        ui->buttonTrattamento->setTextColor( Qt::blue );

            if( sondaCollegata.tipo.capacitivo_dinamico ) ui->buttonTrattamento->setTitleColor( Qt::white );
            else                                          ui->buttonTrattamento->setTitleColor( Qt::blue );
        }
        else if( config->mioProgramma.sonda.tipo.capacitivo_statico_grande  )
        {
            if( sondaCollegata.trattamento.capacitivo ) ui->buttonTrattamento->setTextColor( Qt::white );
            else                                        ui->buttonTrattamento->setTextColor( Qt::blue );

            if( sondaCollegata.tipo.capacitivo_statico_grande  ) ui->buttonTrattamento->setTitleColor( Qt::white );
            else                                                 ui->buttonTrattamento->setTitleColor( Qt::blue );
        }
        else if( config->mioProgramma.sonda.tipo.capacitivo_statico_piccolo  )
        {
            if( sondaCollegata.trattamento.capacitivo ) ui->buttonTrattamento->setTextColor( Qt::white );
            else                                        ui->buttonTrattamento->setTextColor( Qt::blue );

            if(      sondaCollegata.tipo.capacitivo_statico_piccolo ) ui->buttonTrattamento->setTitleColor( Qt::white );
            else                                                      ui->buttonTrattamento->setTitleColor( Qt::blue );
        }
        else if( config->mioProgramma.sonda.tipo.resistivo_dinamico )
        {
            if( sondaCollegata.trattamento.resistivo  ) ui->buttonTrattamento->setTextColor( Qt::white );
            else                                        ui->buttonTrattamento->setTextColor( Qt::blue );
            if( sondaCollegata.tipo.resistivo_dinamico ) ui->buttonTrattamento->setTitleColor( Qt::white );
            else if( sondaCollegata.tipo.resistivo_old ) ui->buttonTrattamento->setTitleColor( Qt::white );
            else                                         ui->buttonTrattamento->setTitleColor( Qt::blue );
        }
        else if( config->mioProgramma.sonda.tipo.resistivo_statico_grande )
        {
            if( sondaCollegata.trattamento.resistivo ) ui->buttonTrattamento->setTextColor( Qt::white );
            else                                       ui->buttonTrattamento->setTextColor( Qt::blue );

            if( sondaCollegata.tipo.resistivo_statico_grande  ) ui->buttonTrattamento->setTitleColor( Qt::white );
            else                                                ui->buttonTrattamento->setTitleColor( Qt::blue );
        }
        else if( config->mioProgramma.sonda.tipo.resistivo_statico_piccolo )
        {
            if( sondaCollegata.trattamento.resistivo ) ui->buttonTrattamento->setTextColor( Qt::white );
            else                                       ui->buttonTrattamento->setTextColor( Qt::blue );

            if( sondaCollegata.tipo.resistivo_statico_piccolo ) ui->buttonTrattamento->setTitleColor( Qt::white );
            else                                                ui->buttonTrattamento->setTitleColor( Qt::blue );
        }
    }

}

void MainWindow::comTask()
{
    static int     ds = 0;
    int            fr;
    int            cr;
    int            el;
    int            mod;
    QString        tx;
    QByteArray     ba;
    unsigned short crcc;

    fr = config->mioProgramma.frequenza == 460?1:2;

    if( ++ds == 10 )
    {
        int tempo;
        if(      stato == IN_RUN   ) tempo = tempoRun;
        else if( stato == IN_PAUSA ) tempo = tempoRun;
        else                         tempo = config->mioProgramma.tempo;

        if(      config->mioProgramma.sonda.tipo.capacitivo_dinamico        == 1 ) mod = 1;
        else if( config->mioProgramma.sonda.tipo.resistivo_dinamico         == 1 ) mod = 2;
        else if( config->mioProgramma.sonda.tipo.capacitivo_statico_piccolo == 1 ) mod = 3;
        else if( config->mioProgramma.sonda.tipo.capacitivo_statico_grande  == 1 ) mod = 4;
        else if( config->mioProgramma.sonda.tipo.resistivo_statico_piccolo  == 1 ) mod = 5;
        else if( config->mioProgramma.sonda.tipo.resistivo_statico_grande   == 1 ) mod = 6;

        // mando le informazioni al PC
        tx = QString("#;%1;%2;%3;%4;%5;%6;%7;%8;%9;")
                .arg(counterPC)
                .arg(tempo)
                .arg(config->mioProgramma.potenza)
                .arg(fr)
                .arg(config->mioProgramma.frequenzaPemf / 100 )
                .arg(mod)
                .arg(stato)
                .arg(allarmi.val)
                .arg(warning.val);

//        tx = QString("#;%1;%2;%3;%4;%5;%6;")
//                .arg(1)
//                .arg(tempo)
//                .arg(config->mioProgramma.potenza)
//                .arg( fr )
//                .arg(config->mioProgramma.frequenzaPemf / 100 )
//                .arg(stato);

        ba = tx.toLatin1();
        crcc = CRC16( ba.data() ,ba.length());

        tx = QString( "%1%2\r" ).arg( tx ).arg( crcc );

        rs485->writeQString( &tx);
        ds = 0;
    }

    if( allarmeComunicazione > 0 ) --allarmeComunicazione;
    else
    {
        stopEsecuzione();
        emit allarmeInCorso();
    }

    // CT300
    int pot = config->mioProgramma.potenza;
    if( pot > 1 )
    {
        // CT250 ?
        if( config->tipoMacchina == CT250 ) pot = (pot * 90) / 100;
        // CT200 ?
        if( config->tipoMacchina == CT200 ) pot = (pot * 80) / 100;
    }


    // ESECUZIONE
    if( stato != IN_RUN ) pot = 0;
    else
    {       
        if( tempoRun > 0 && warning.val == 0 ) {
            --tempoRun;
            if( tempoRun % 10 == 0 )
            {
                int mm,ss;
                mm = tempoRun / 600;
                ss = (tempoRun / 10 )- ( mm * 60);
                QString stempo = QString("%1:%2 min").arg(mm,2,10,QLatin1Char('0')).arg(ss,2,10,QLatin1Char('0'));
                ui->buttonTempo->setText(stempo);
            }
        }
        else if( tempoRun == 0 )
        {
            cicala = 2;
            stopEsecuzione();

        }
    }

    if( config->calibra == true )
    {
        config->calibra = false;
        comando = 1;
    }

    // COMUNICAZIONE
    if(      config->mioProgramma.sonda.trattamento.capacitivo )   cr = 1;
    else                                                           cr = 2;
    if(      config->mioProgramma.sonda.tipo.capacitivo_dinamico ) el = 2;
    else if( config->mioProgramma.sonda.tipo.capacitivo_old      ) el = 2;
    else if( config->mioProgramma.sonda.tipo.resistivo_dinamico  ) el = 2;
    else if( config->mioProgramma.sonda.tipo.resistivo_old       ) el = 2;
    else                                                           el = 1;

    tx = QString(":%1;%2;%3;%4;%5;%6;%7;").arg(cicala).arg(pot,3,10,QChar('0')).arg(cr).arg(fr).arg( config->mioProgramma.frequenzaPemf / 100 ).arg(el).arg( comando );

    ba = tx.toLatin1();
    crcc = CRC16( ba.data() ,ba.length());

    tx = QString( "%1%2\r" ).arg(tx).arg(crcc,5,10,QChar('0'));
    seriale->writeQString(&tx);

    cicala = 0;
}

void MainWindow::proc485Rx( char *line, int len )
{
    // es #;1;10;5;1;3;1;41283
    QStringList dati = QString(line).split(';');

    if( dati.count() == 8 )
    {
        // verifico crc
        QString    rx = QString("#%1;%2;%3;%4;%5;%6;%7;").arg(dati[0]).arg(dati[1]).arg(dati[2]).arg(dati[3]).arg(dati[4]).arg(dati[5]).arg(dati[6]);
        QByteArray ba = rx.toLatin1();

        unsigned short crc    = CRC16( ba.data(),ba.length() );
        unsigned short miocrc = dati[7].toInt();

        if( crc == miocrc )
        {
            if( stato != IN_RUN )
            {
                sProgramma newProg;
                bool ok = true;

                newProg.tempo   = dati.at(2).toInt();
                newProg.potenza = dati.at(3).toInt();

                if( newProg.potenza > 100 ) ok = false;

                if(      dati.at(4) == "1" ) newProg.frequenza = 460;
                else if( dati.at(4) == "2" ) newProg.frequenza = 690;
                else                         ok = false;
                if(      dati.at(5) == "1" ) newProg.frequenzaPemf = 100;
                else if( dati.at(5) == "2" ) newProg.frequenzaPemf = 200;
                else if( dati.at(5) == "3" ) newProg.frequenzaPemf = 300;
                else                         ok = false;
                newProg.sonda.val = 0;
                if(      dati.at(6) == "1" ) newProg.sonda.tipo.capacitivo_dinamico        = 1;
                else if( dati.at(6) == "2" ) newProg.sonda.tipo.resistivo_dinamico         = 1;
                else if( dati.at(6) == "3" ) newProg.sonda.tipo.capacitivo_statico_piccolo = 1;
                else if( dati.at(6) == "4" ) newProg.sonda.tipo.capacitivo_statico_grande  = 1;
                else if( dati.at(6) == "5" ) newProg.sonda.tipo.resistivo_statico_piccolo  = 1;
                else if( dati.at(6) == "6" ) newProg.sonda.tipo.resistivo_statico_grande   = 1;
                else                         ok = false;

                if( ok == true )
                {
                    counterPC = dati.at(1).toInt();
                    caricaProgramma( newProg );
                }
            }
        }
    }
}

void MainWindow::procSerialRx( char *line, int len )
{

    static int stst = 0;
    QStringList dati = QString(line).split(';');

    allarmeComunicazione = 10;


    if( dati.count() == 8 )
    {
        // verifico crc
        QString    rx = QString("$%1;%2;%3;%4;%5;%6;%7;").arg(dati[0]).arg(dati[1]).arg(dati[2]).arg(dati[3]).arg(dati[4]).arg(dati[5]).arg(dati[6]);
        QByteArray ba = rx.toLatin1();

        unsigned short crc    = CRC16( ba.data(),ba.length() );
        unsigned short miocrc = dati[7].toInt();

        if( crc == miocrc )
        {
            //qDebug("ciao");
            int rotella            = dati[0].toInt();
            ingressi               = dati[1].toInt();
            allarmi.val            = dati[2].toInt();
            warning.val            = dati[3].toInt();              // 4096 = mano scollegata
            sondaCollegata.val     = dati[4].toInt();
            sondaCollegata.tipo.capacitivo_old = 0; // eliminate le sonde con resistenze da 1k
            sondaCollegata.tipo.resistivo_old  = 0; // idem
            statoMacchina          = dati[5].toInt();
            releaseHardware        = dati[6];

            if( allarmi.val != allarmiOld.val )
            {
                emit allarmeInCorso();
                allarmiOld.val = allarmi.val;
            }
            if( warning.val == 0 && warningOld.val != 0 )
            {
                warningOld.val = 0;
                emit warningInCorso();
            }
            if( warning.val != warningOld.val && stato == IN_RUN )
            {
                emit warningInCorso();
                warningOld.val = warning.val;
            }
            // Premuto il pulsante di Start/ Stop ?
            if( ingressi & 0x02 )
            {
                if( stst == 0 ) {
                    cicala = 1;
                    if(      stato == IN_RUN   ) stopEsecuzione();
                    else if( stato == IN_PAUSA ) stopEsecuzione();
                    else                         startEsecuzione();
                }
                stst = 1;
            }
            else stst = 0;

            if( statoMacchina & 0x01 )
            {
                ui->buttonTrattamento->setEnabled(false);
                ui->buttonFrequenza->setEnabled(false);
                ui->buttonPemf->setEnabled(false);
                ui->buttonPotenza->setEnabled(true);
                ui->buttonTempo->setEnabled(false);
                ui->buttonGuida->setEnabled(false);
                ui->buttonInfo->setEnabled(false);
                ui->buttonCalibra->setEnabled(false);
                stato = IN_CALIBRAZIONE;
            }
            else if( statoMacchina == 0 && stato == IN_CALIBRAZIONE )
            {
                stopEsecuzione();
                comando = 0;
            }
            miaRotella->setValue(rotella, (ingressi & 0x01) );
        }
    }
    else if( dati.count() == 9 )
    {
        // verifico crc
        QString    rx = QString("$%1;%2;%3;%4;%5;%6;%7;%8;").arg(dati[0]).arg(dati[1]).arg(dati[2]).arg(dati[3]).arg(dati[4]).arg(dati[5]).arg(dati[6]).arg(dati[7]);
        QByteArray ba = rx.toLatin1();

        unsigned short crc    = CRC16( ba.data(),ba.length() );
        unsigned short miocrc = dati[8].toInt();

        if( crc == miocrc )
        {
            int rotella            = dati[0].toInt();
            ingressi               = dati[1].toInt();
            allarmi.val            = dati[2].toInt();
            warning.val            = dati[3].toInt();              // 4096 = mano scollegata
            sondaCollegata.val     = dati[4].toInt();
            sondaCollegata.tipo.capacitivo_old = 0; // eliminate le sonde con resistenze da 1k
            sondaCollegata.tipo.resistivo_old  = 0; // idem
            statoMacchina          = dati[5].toInt();
            releaseHardware        = dati[6];
            vco3                   = dati[7].toInt();

            if( allarmi.val != allarmiOld.val )
            {
                emit allarmeInCorso();
                allarmiOld.val = allarmi.val;
            }
            if( warning.val == 0 && warningOld.val != 0 )
            {
                warningOld.val = 0;
                emit warningInCorso();
            }
            if( warning.val != warningOld.val && stato == IN_RUN )
            {
                emit warningInCorso();
                warningOld.val = warning.val;
            }
            // Premuto il pulsante di Start/ Stop ?
            if( ingressi & 0x02 )
            {
                if( stst == 0 ) {
                    cicala = 1;
                    if(      stato == IN_RUN   ) stopEsecuzione();
                    else if( stato == IN_PAUSA ) stopEsecuzione();
                    else                         startEsecuzione();
                }
                stst = 1;
            }
            else stst = 0;

            if( statoMacchina & 0x01 )
            {
                ui->buttonTrattamento->setEnabled(false);
                ui->buttonFrequenza->setEnabled(false);
                ui->buttonPemf->setEnabled(false);
                ui->buttonPotenza->setEnabled(true);
                ui->buttonTempo->setEnabled(false);
                ui->buttonGuida->setEnabled(false);
                ui->buttonInfo->setEnabled(false);
                ui->buttonCalibra->setEnabled(false);
                stato = IN_CALIBRAZIONE;
            }
            else if( statoMacchina == 0 && stato == IN_CALIBRAZIONE )
            {
                stopEsecuzione();
                comando = 0;
            }
            miaRotella->setValue(rotella, (ingressi & 0x01) );
        }
    }
}

void MainWindow::updateTabFocus()
{
    //miatastiera = new ufgTastiera(this);
    //miatastiera->setGeometry(0,0,790,100);
    //miatastiera->show();
    //miatastiera->showanimation(QRect(0,0,800,0),QRect(0,0,800,100));

//    // -----------------------------------
//    // TEST QTIME
//    // -----------------------------------
//    static bool start = false;

//    if( start == false )
//    {
//        start = true;
//        timeStart.setHMS(0,0,0);
//        timeStart.start();

//        ptime = new QTime(0,0,0);
//        ptime->setHMS(2,0,0,0);
//        ptime->start();
//        int difh = ptime->hour();
//        qDebug("%d",difh);
//    }
//    else
//    {
//        QTime adesso;

//        int ms = ptime->elapsed();
//        ptime->restart();

//        QString mss = QString::number(ms);

//        qDebug(mss.toLatin1());

//        //QTime diff = adesso->msecsTo(timeStart);

//        //QString difstring = diff.toString("hh:mm:ss.zzz");
//        QString difstring = ptime->toString("hh:mm:ss.zzz");
//        qDebug(difstring.toLatin1());
//    }

//    // ---------------------------------------------
//    // TEST LUMINOSITA DISPLAY
//    // ---------------------------------------------
//    static int luminosita = 6;
// //    QStringList listaValori;
// //    listaValori << "1" << "2" << "3" << "4" << "5" << "6" << "7";

// //    QInputDialog dialog;
// //    dialog.setGeometry(50,20,200,100);
// //    const QString luminosita = dialog.getItem( this ,"Luminosita", "Seleziona il livello di luminosità",listaValori);

//    QFile miofile("/sys/class/backlight/backlight.18/brightness");
//    miofile.open(QIODevice::WriteOnly | QIODevice::Text);

//    QString lum = QString::number(luminosita);
//    //miofile.write( luminosita.toLatin1() );
//    miofile.write( lum.toLatin1() );

//    if(luminosita >1) --luminosita;
//    else luminosita = 7;

//    miofile.close();

}

void MainWindow::setCicala(int tempo)
{
    //cicala = tempo;
}

void MainWindow::rotellaMove(bool up)
{
    if( stato == IN_RUN )
    {
        getPotenza();
//        if( up )
//        {
//            incrementaPotenza();
//        }
//        else if( config->potenza > 1 )
//        {
//            decrementaPotenza();
//        }
    }

    else
    {
        if( miaRotella->delegate == this )
        {
            if( up ) {
                this->focusNextChild();
            }
            else
            {
                this->focusPreviousChild();
            }
        }
    }
}

void MainWindow::rotellaClick()
{
    if( miaRotella->delegate == this )
    {
        if(     this->focusWidget() == ui->buttonTrattamento ) this->getTrattamento();
        else if(this->focusWidget() == ui->buttonFrequenza   ) this->getFrequenza();
        else if(this->focusWidget() == ui->buttonPemf        ) this->getFrequenzaPemf();
        else if(this->focusWidget() == ui->buttonPotenza     ) this->getPotenza();
        else if(this->focusWidget() == ui->buttonTempo       ) this->getTempo();
        else if(this->focusWidget() == ui->buttonStart       ) this->startStop();
        else if(this->focusWidget() == ui->buttonInfo        ) this->showInfo();
        else if(this->focusWidget() == ui->buttonGuida       ) this->showMenuGuida();
        else if(this->focusWidget() == ui->buttonCalibra     ) emit ui->buttonCalibra->buttonClicked();
    }
}

bool MainWindow::listFileAndDirectory( QDir dir)
{
    QStringList listafile;
    bool ok = dir.exists();  //check if directory exist
    if ( ok )
    {
        //set fileinfo filter
        QFileInfoList entries = dir.entryInfoList( QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files );
        //loop over entries filter selected
        foreach ( QFileInfo entryInfo, entries )
        {
           QString path = entryInfo.absoluteFilePath();
           if ( entryInfo.isDir() )	//check if entryInfo is dir
            {
                if ( !listFileAndDirectory( QDir( path ) ) )
                {
                    ok = false;
                    break;
                }
            }
            else
            {
                listafile.append(path);
            }
        }
    }
    if (ok && !dir.exists(dir.absolutePath()))  ok = false;

    foreach (QString file, listafile ) {
        qDebug( file.toLatin1());
    }

    return ok;
}


void MainWindow::showEvent(QShowEvent *)
{

}

void MainWindow::changeEvent(QEvent *event)
{
    if( event != NULL )
    {
        if( event->type() == QEvent::LanguageChange )
        {
            ui->retranslateUi(this);
        }
    }
}

void MainWindow::showAllarme()
{
    bool show = false;

    QString descrizione;
    QString immagine;    
    QString al;

    if( allarmeComunicazione == 0 )
    {
        descrizione = QString( tr("descrizione allarme B001"));
        immagine    = ":/img/attenzione.png";
        allarmeComunicazione = 10;
        show = true;
    }

    if( allarmi.val != 0 )
    {
        show = true;
        pausaEsecuzione();
        al = " (0x" + QString::number( allarmi.val, 16 ) + ")";
        if( allarmi.val & AL_CORTO )
        {
            descrizione = QString( tr("descrizione allarme A001"));
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_CONDUZIONE_DIODI )
        {
            descrizione = QString( tr("descrizione allarme B002")) + al;
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_ALCURR )
        {
            descrizione = QString( tr("descrizione allarme A003"));
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_VREF )
        {
            descrizione = QString( tr("descrizione allarme B002")) + al;
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_TATT )
        {
            descrizione = QString( tr("descrizione allarme B002")) + al;
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_VPOWER )
        {
            descrizione = QString( tr("descrizione allarme A004"));
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_TEMP )
        {
            descrizione = QString( tr("descrizione allarme A002"));
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_TRANS )
        {
            descrizione = QString( tr("descrizione allarme B002")) + al;
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_VPAZ )
        {
            descrizione = QString( tr("descrizione allarme B002")) + al;
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_IPAZ )
        {
            descrizione = QString( tr("descrizione allarme A005"));
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_IVUOTO )
        {
            if( config->mioProgramma.sonda.trattamento.capacitivo )
            {
                descrizione = QString( tr("descrizione warning capacitivo"));
            }
            else
            {
                descrizione = QString( tr("descrizione warning resistivo"));
            }
            immagine    = ":/img/ico/allarmeElettrodo.png";
        }
        else if( allarmi.val & AL_VCO3 )
        {
            descrizione = QString( tr("descrizione allarme A006"));
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_APROVAMASSA )
        {
            descrizione = QString( tr("descrizione warning elettrodo massa"));
            immagine    = ":/img/ico/allarmeMassa.png";
        }
        else if( allarmi.val & AL_VMIN )
        {
            descrizione = QString( tr("descrizione allarme B002")) + al;
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_DEADMAN )
        {
            descrizione = QString( tr("descrizione warning mano scollegata"));
            immagine    = ":/img/attenzione.png";
        }
        else if( allarmi.val & AL_SONDA_NC )
        {
            if( config->mioProgramma.sonda.trattamento.capacitivo )
            {
                descrizione = QString( tr("descrizione warning capacitivo"));
                immagine    = ":/img/ico/allarmeElettrodo.png";
            }
            else
            {
                descrizione = QString( tr("descrizione warning resistivo"));
                immagine    = ":/img/ico/allarmeElettrodo.png";
            }
        }
    }
    if( show == true )
    {
        formInfo->setText(       descrizione );
        formInfo->setImageTitle( immagine    );

        disconnect( formInfo, SIGNAL(onClose()), 0, 0 );
        connect(    formInfo, SIGNAL(onClose()), this, SLOT(resetAllarmi()));

        disconnect( miaRotella, SIGNAL(valueChange(bool)),  this, SLOT(rotellaMove(bool)));
        disconnect( miaRotella, SIGNAL(click()),            this, SLOT(rotellaClick()));
        connect(    miaRotella, SIGNAL(valueChange(bool)),  formInfo, SLOT(rotellaMove(bool)));
        connect(    miaRotella, SIGNAL(click()),            formInfo, SLOT(rotellaClick()));

        formInfo->showNormal();
    }
}

void MainWindow::showWarning()
{
    QString descrizione;
    QString immagine;

    bool show = false;

    if( warning.val == 0 && mioWarning.val == 0 )
    {
        formWarning->close();
    }

    if( formInfo->isActiveWindow() ) return;

    warning.val += mioWarning.val;
    mioWarning.val = 0;

    if( warning.val != 0 )
    {
        if( warning.val & W_ELETTRODOMASSA_SCOLLEGATO )
        {
            descrizione = QString( tr("descrizione warning elettrodo massa"));
            immagine    = ":/img/ico/allarmeMassa.png";
            show        = true;
        }
        else if( warning.val & W_ELETTRODO_SCOLLEGATO )
        {
            if( config->mioProgramma.sonda.trattamento.capacitivo )
            {
                descrizione = QString( tr("descrizione warning capacitivo"));
                immagine    = ":/img/ico/allarmeElettrodo.png";
            }
            else
            {
                descrizione = QString( tr("descrizione warning resistivo"));
                immagine    = ":/img/ico/allarmeElettrodo.png";
            }
            show = true;
        }
        else if( warning.val & W_POMPETTA )
        {
            descrizione = QString( tr("descrizione allarme emergenza"));
            immagine    = ":/img/ico/allarmeEmergenza.png";
            show        = true;
        }
        else if( warning.val & AL_IVUOTO )
        {
            descrizione = QString( tr("Verifica contatto sonda"));
            immagine    = ":/img/ico/allarmeElettrodo.png";
            show        = true;
        }

        if( show == true )
        {
            formWarning->setText( descrizione );
            formWarning->showNormal();

            if( stato == IN_RUN )   timeoutWarning = 50;
            else                    timeoutWarning = 10;

        }
    }
}

void MainWindow::riprendoRotella()
{
    disconnect( miaRotella, SIGNAL(valueChange(bool)), 0, 0 );
    disconnect( miaRotella, SIGNAL(click()),           0, 0 );
    connect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    connect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));

    miaRotella->setEnableBeep( true );
    miaRotella->setStep(1);
    miaRotella->delegate = this;
}

void MainWindow::resetAllarmi()
{
    riprendoRotella();
    disconnect( formInfo, SIGNAL(onClose()));
}

void MainWindow::richiediPassword()
{
    //tastierinoNumerico* tast = new tastierinoNumerico(this);
    //tast->setGeometry( 200, 40, 400,400 );
    tastierino->setTitle( "enter password");
    tastierino->showNormal();
}

void MainWindow::newConfig()
{
//    if( config->tipoMacchina == CT200 ) config->mioProgramma.frequenzaPemf = 300;
    //setLingua( config->lingua, true );
    //setScritte();

    setParametri(&config->mioProgramma);

    this->riprendoRotella();

//    connect( formUser, SIGNAL(onClose()), this, SLOT(riprendoRotella()));
}

void MainWindow::setScritte()
{
    ui->buttonTrattamento->setTitle(tr("MODALITA'"));
    ui->buttonFrequenza->setTitle(tr("FREQUENZA"));
    ui->buttonPemf->setTitle(tr("FREQUENZA LOW"));
    ui->buttonPotenza->setTitle(tr("POTENZA"));
    ui->buttonTempo->setTitle(tr("TEMPO"));
    ui->buttonGuida->setText(tr("GUIDA"));
    ui->buttonInfo->setText(tr("INFO"));
    ui->buttonCalibra->setText(tr("IMPOSTAZIONI"));

    if( config->mioProgramma.sonda.tipo.capacitivo_dinamico )
    {
        ui->buttonTrattamento->setTitle(tr("CAPACITIVO").toUpper() );
        ui->buttonTrattamento->setText(tr("DINAMICO").toUpper() );
    }
    else if( config->mioProgramma.sonda.tipo.capacitivo_old )
    {
        ui->buttonTrattamento->setTitle(tr("CAPACITIVO").toUpper());
        ui->buttonTrattamento->setText(tr("DINAMICO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.capacitivo_statico_piccolo)
    {
        ui->buttonTrattamento->setText(tr("CAPACITIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO PICCOLO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.capacitivo_statico_grande )
    {
        ui->buttonTrattamento->setText(tr("CAPACITIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO GRANDE").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_dinamico )
    {
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("DINAMICO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_old )
    {
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("DINAMICO").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_statico_grande )
    {
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO GRANDE").toUpper());
    }
    else if( config->mioProgramma.sonda.tipo.resistivo_statico_piccolo )
    {
        ui->buttonTrattamento->setText(tr("RESISTIVO").toUpper());
        ui->buttonTrattamento->setTitle(tr("STATICO PICCOLO").toUpper());
    }

    QString rel = tr("Versione");
    QString rl = QString( "%1 %2").arg(rel).arg( RELEASE_FW );
    ui->labelRelease->setText( rl );

    if(      config->tipoMacchina == CT200 ) ui->labelTipoMacchina->setText( "CT200");
    else if( config->tipoMacchina == CT250 ) ui->labelTipoMacchina->setText( "CT250");
    else if( config->tipoMacchina == CT300 ) ui->labelTipoMacchina->setText( "CT300");
}

//void MainWindow::showInfoCalibra()
//{
//    QString titolo      = tr("PROCEDURA DI CALIBRAZIONE");
//    QString descrizione = tr("Info calibrazione");

//    formInfo->setText( descrizione );
//    formInfo->setTitle( titolo );

//    disconnect( formInfo, SIGNAL(onClose()), 0 ,0 );
//    connect( formInfo, SIGNAL(onClose()), this, SLOT(exeCalibrazione()));

//    disconnect( miaRotella, SIGNAL(valueChange(bool)),  this, SLOT(rotellaMove(bool)));
//    disconnect( miaRotella, SIGNAL(click()),            this, SLOT(rotellaClick()));
//    connect( miaRotella, SIGNAL(valueChange(bool)), formInfo, SLOT(rotellaMove(bool)));
//    connect( miaRotella, SIGNAL(click()),           formInfo, SLOT(rotellaClick()));

//    formInfo->showNormal();

//}

void MainWindow::exeCalibrazione()
{
    comando = 1;
    riprendoRotella();
}

unsigned short MainWindow::CRC16( char *puchMsg, unsigned int dataLength)
{
    unsigned short CheckSum;
    unsigned int j;
    unsigned short crc,i;
    CheckSum = 0xffff;
    for ( j = 0 ; j < dataLength ; j++ )
    {
        CheckSum = CheckSum^((unsigned short)puchMsg[j] & 0x00ff);
        for( i = 8 ; i > 0 ; i-- )
        {
            if((CheckSum)&0x0001)  CheckSum   = (CheckSum>>1)^0xa001;
            else CheckSum >>= 1;
        }
    }
    crc = CheckSum >> 8; // CheckSum
    CheckSum <<= 8;
    crc |= CheckSum & 0xff00;
    return crc;
}
