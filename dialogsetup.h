#ifndef DIALOGSETUP_H
#define DIALOGSETUP_H

#include <QDialog>
#include "diatermia.h"
#include "tastierinonumerico.h"

namespace Ui {
class dialogSetup;
}

class dialogSetup : public QDialog
{
    Q_OBJECT

private:
    Ui::dialogSetup *ui;
    parametri* config;

    void showEvent(QShowEvent*);

public:
    explicit dialogSetup(QWidget *parent = 0, parametri* conf = NULL);
    ~dialogSetup();    

    void setParametri( parametri* conf)
    {
        config = conf;
    }

private slots:

    void selezionatoMacchina1();
    void selezionatoMacchina2();
    void selezionatoMacchina3();
    void selezionatoPassword();

    //void exeCalibrazione();

    void exit();
signals:
    void onClose();
};

#endif // DIALOGSETUP_H
