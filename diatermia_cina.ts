<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh" sourcelanguage="it">
<context>
    <name>DialogOrologio</name>
    <message>
        <location filename="dialogorologio.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="12"/>
        <source>Orologio</source>
        <translation>時鐘</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="82"/>
        <source>Ore</source>
        <translation>小時</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="98"/>
        <source>Minuti</source>
        <translation>小時</translation>
    </message>
    <message>
        <location filename="dialogorologio.cpp" line="114"/>
        <source>Secondi</source>
        <translation>秒</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="36"/>
        <source>Diatermia</source>
        <translation>透熱</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="43"/>
        <location filename="mainwindow.cpp" line="1837"/>
        <source>MODALITA&apos;</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="44"/>
        <location filename="mainwindow.cpp" line="824"/>
        <location filename="mainwindow.cpp" line="1838"/>
        <source>FREQUENZA</source>
        <translation>频率</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="50"/>
        <location filename="mainwindow.cpp" line="1844"/>
        <source>IMPOSTAZIONI</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="849"/>
        <source>FREQUENZA PEMF</source>
        <translation>SUPER LOW FREQUENCY</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="46"/>
        <location filename="mainwindow.cpp" line="876"/>
        <location filename="mainwindow.cpp" line="1840"/>
        <source>POTENZA</source>
        <translation>功率</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="47"/>
        <location filename="mainwindow.cpp" line="741"/>
        <location filename="mainwindow.cpp" line="1841"/>
        <source>TEMPO</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <location filename="mainwindow.cpp" line="1842"/>
        <source>GUIDA</source>
        <translation>治疗方案</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="49"/>
        <location filename="mainwindow.cpp" line="529"/>
        <location filename="mainwindow.cpp" line="1843"/>
        <source>INFO</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="1031"/>
        <location filename="mainwindow.cpp" line="1887"/>
        <source>Versione</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="314"/>
        <location filename="mainwindow.cpp" line="321"/>
        <location filename="mainwindow.cpp" line="328"/>
        <location filename="mainwindow.cpp" line="588"/>
        <location filename="mainwindow.cpp" line="595"/>
        <location filename="mainwindow.cpp" line="602"/>
        <location filename="mainwindow.cpp" line="1848"/>
        <location filename="mainwindow.cpp" line="1853"/>
        <location filename="mainwindow.cpp" line="1858"/>
        <location filename="mainwindow.cpp" line="1863"/>
        <source>CAPACITIVO</source>
        <translation>电容</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="315"/>
        <location filename="mainwindow.cpp" line="336"/>
        <location filename="mainwindow.cpp" line="589"/>
        <location filename="mainwindow.cpp" line="610"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <location filename="mainwindow.cpp" line="1854"/>
        <location filename="mainwindow.cpp" line="1869"/>
        <location filename="mainwindow.cpp" line="1874"/>
        <source>DINAMICO</source>
        <translation>动态</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="335"/>
        <location filename="mainwindow.cpp" line="342"/>
        <location filename="mainwindow.cpp" line="349"/>
        <location filename="mainwindow.cpp" line="609"/>
        <location filename="mainwindow.cpp" line="616"/>
        <location filename="mainwindow.cpp" line="623"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <location filename="mainwindow.cpp" line="1873"/>
        <location filename="mainwindow.cpp" line="1878"/>
        <location filename="mainwindow.cpp" line="1883"/>
        <source>RESISTIVO</source>
        <translation>电阻</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="548"/>
        <location filename="mainwindow.cpp" line="569"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="773"/>
        <source>MODALITA</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="906"/>
        <source>DINAMICI</source>
        <translation>动态</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="907"/>
        <source>STATICI</source>
        <translation>静止</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="981"/>
        <source>CALIBRAZIONE TERMINATA</source>
        <translation>校准已完成</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="999"/>
        <source>CALIBRAZIONE IN CORSO</source>
        <translation>正在校准</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1780"/>
        <source>Verifica contatto sonda</source>
        <translation>檢查探頭接觸</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="530"/>
        <source>desrizione info</source>
        <translation>该医疗设备若使用不当会造成患者损伤，请仔细阅读产品说明书及操作方法</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1609"/>
        <source>descrizione allarme B001</source>
        <translation>报警 內部通訊錯誤給系統</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1622"/>
        <source>descrizione allarme A001</source>
        <translation>报警 通過短路檢測出的系統</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1652"/>
        <source>descrizione allarme A002</source>
        <translation>报警 該系統已檢測到過熱</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <source>descrizione allarme A003</source>
        <translation>报警 該系統檢測到異常電流消耗</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1647"/>
        <source>descrizione allarme A004</source>
        <translation>报警 該系統檢測到異常電壓</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="1637"/>
        <location filename="mainwindow.cpp" line="1642"/>
        <location filename="mainwindow.cpp" line="1657"/>
        <location filename="mainwindow.cpp" line="1662"/>
        <location filename="mainwindow.cpp" line="1694"/>
        <source>descrizione allarme B002</source>
        <oldsource>descrizione allarme trans</oldsource>
        <translation>报警 通用硬件錯誤</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="45"/>
        <location filename="mainwindow.cpp" line="1839"/>
        <source>FREQUENZA LOW</source>
        <translation>调制频率</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="322"/>
        <location filename="mainwindow.cpp" line="343"/>
        <location filename="mainwindow.cpp" line="596"/>
        <location filename="mainwindow.cpp" line="617"/>
        <location filename="mainwindow.cpp" line="1864"/>
        <location filename="mainwindow.cpp" line="1879"/>
        <source>STATICO GRANDE</source>
        <translation>大静态</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="329"/>
        <location filename="mainwindow.cpp" line="350"/>
        <location filename="mainwindow.cpp" line="603"/>
        <location filename="mainwindow.cpp" line="624"/>
        <location filename="mainwindow.cpp" line="1859"/>
        <location filename="mainwindow.cpp" line="1884"/>
        <source>STATICO PICCOLO</source>
        <translation>小静态</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1667"/>
        <source>descrizione allarme A005</source>
        <translation>报警 該系統檢測到異常電流的患者</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1684"/>
        <source>descrizione allarme A006</source>
        <translation>报警 該系統檢測到異常操作頻率</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1689"/>
        <location filename="mainwindow.cpp" line="1754"/>
        <source>descrizione warning elettrodo massa</source>
        <translation>注意：接地电极断开</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1774"/>
        <source>descrizione allarme emergenza</source>
        <translation>檢查患者的緊急按鈕</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1674"/>
        <location filename="mainwindow.cpp" line="1706"/>
        <location filename="mainwindow.cpp" line="1762"/>
        <source>descrizione warning capacitivo</source>
        <translation>注意 電容電極斷開</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1678"/>
        <location filename="mainwindow.cpp" line="1711"/>
        <location filename="mainwindow.cpp" line="1767"/>
        <source>descrizione warning resistivo</source>
        <translation>注意 電阻電極斷開</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1699"/>
        <source>descrizione warning mano scollegata</source>
        <translation>注意 緊急按鈕當前離線</translation>
    </message>
</context>
<context>
    <name>dialogElencoProtocolli</name>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="97"/>
        <source>PROTOCOLLI GENERICI</source>
        <translation>常规处方</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="99"/>
        <source>FASE ACUTA</source>
        <translation>急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="100"/>
        <source>FASE CRONICA</source>
        <translation>恢复期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="101"/>
        <source>RIDUZIONE DEL DOLORE</source>
        <translation>镇痛</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="102"/>
        <source>CONTRATTURA</source>
        <translation>痉挛控制</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="103"/>
        <source>LINFODRENAGGIO</source>
        <translation>淋巴引流</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="104"/>
        <source>RADICOLOPATIA FASE ACUTA</source>
        <translation>急性神经根压迫</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="105"/>
        <source>BLOCCO ARTICOLARE / RIGIDITA</source>
        <translation>关节僵硬</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="126"/>
        <source>PROTOCOLLI PER PATOLOGIA</source>
        <translation>常见病症处方</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="192"/>
        <location filename="dialogelencoprotocolli.cpp" line="218"/>
        <source>PROTOCOLLI NEUROLOGICI</source>
        <translation>神经损伤处方</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="200"/>
        <source>MIELO LESIONE PARZIALE</source>
        <translation>不完全神经损伤</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="201"/>
        <source>TRATTAMENTO POST ICTUS</source>
        <translation>脑卒中</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="202"/>
        <source>LESIONI NERVOSE PERIFERICHE ( ERNIE DISCALI )</source>
        <translation>周围神经损伤（椎间盘突出）</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="203"/>
        <source>LESIONI NERVOSE PERIFERICHE (PLESSO BRACHIALE )</source>
        <translation>周围神经损伤（臂丛）</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="256"/>
        <location filename="dialogelencoprotocolli.cpp" line="270"/>
        <location filename="dialogelencoprotocolli.cpp" line="284"/>
        <location filename="dialogelencoprotocolli.cpp" line="298"/>
        <location filename="dialogelencoprotocolli.cpp" line="348"/>
        <location filename="dialogelencoprotocolli.cpp" line="483"/>
        <location filename="dialogelencoprotocolli.cpp" line="540"/>
        <location filename="dialogelencoprotocolli.cpp" line="554"/>
        <location filename="dialogelencoprotocolli.cpp" line="568"/>
        <location filename="dialogelencoprotocolli.cpp" line="582"/>
        <location filename="dialogelencoprotocolli.cpp" line="754"/>
        <location filename="dialogelencoprotocolli.cpp" line="813"/>
        <location filename="dialogelencoprotocolli.cpp" line="827"/>
        <location filename="dialogelencoprotocolli.cpp" line="841"/>
        <location filename="dialogelencoprotocolli.cpp" line="855"/>
        <location filename="dialogelencoprotocolli.cpp" line="906"/>
        <location filename="dialogelencoprotocolli.cpp" line="1013"/>
        <location filename="dialogelencoprotocolli.cpp" line="1070"/>
        <location filename="dialogelencoprotocolli.cpp" line="1084"/>
        <location filename="dialogelencoprotocolli.cpp" line="1098"/>
        <location filename="dialogelencoprotocolli.cpp" line="1271"/>
        <location filename="dialogelencoprotocolli.cpp" line="1329"/>
        <location filename="dialogelencoprotocolli.cpp" line="1343"/>
        <location filename="dialogelencoprotocolli.cpp" line="1503"/>
        <location filename="dialogelencoprotocolli.cpp" line="1561"/>
        <location filename="dialogelencoprotocolli.cpp" line="1709"/>
        <location filename="dialogelencoprotocolli.cpp" line="1723"/>
        <location filename="dialogelencoprotocolli.cpp" line="1737"/>
        <location filename="dialogelencoprotocolli.cpp" line="1751"/>
        <location filename="dialogelencoprotocolli.cpp" line="1812"/>
        <location filename="dialogelencoprotocolli.cpp" line="2296"/>
        <location filename="dialogelencoprotocolli.cpp" line="2324"/>
        <location filename="dialogelencoprotocolli.cpp" line="2375"/>
        <location filename="dialogelencoprotocolli.cpp" line="2389"/>
        <source>prono</source>
        <translation>俯卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="257"/>
        <location filename="dialogelencoprotocolli.cpp" line="271"/>
        <location filename="dialogelencoprotocolli.cpp" line="285"/>
        <location filename="dialogelencoprotocolli.cpp" line="299"/>
        <location filename="dialogelencoprotocolli.cpp" line="541"/>
        <location filename="dialogelencoprotocolli.cpp" line="555"/>
        <location filename="dialogelencoprotocolli.cpp" line="569"/>
        <location filename="dialogelencoprotocolli.cpp" line="583"/>
        <location filename="dialogelencoprotocolli.cpp" line="605"/>
        <location filename="dialogelencoprotocolli.cpp" line="619"/>
        <location filename="dialogelencoprotocolli.cpp" line="814"/>
        <location filename="dialogelencoprotocolli.cpp" line="828"/>
        <location filename="dialogelencoprotocolli.cpp" line="842"/>
        <location filename="dialogelencoprotocolli.cpp" line="856"/>
        <location filename="dialogelencoprotocolli.cpp" line="1071"/>
        <location filename="dialogelencoprotocolli.cpp" line="1085"/>
        <location filename="dialogelencoprotocolli.cpp" line="1099"/>
        <location filename="dialogelencoprotocolli.cpp" line="1330"/>
        <location filename="dialogelencoprotocolli.cpp" line="1344"/>
        <location filename="dialogelencoprotocolli.cpp" line="1562"/>
        <location filename="dialogelencoprotocolli.cpp" line="1710"/>
        <location filename="dialogelencoprotocolli.cpp" line="1724"/>
        <location filename="dialogelencoprotocolli.cpp" line="1738"/>
        <location filename="dialogelencoprotocolli.cpp" line="1752"/>
        <location filename="dialogelencoprotocolli.cpp" line="1775"/>
        <location filename="dialogelencoprotocolli.cpp" line="1789"/>
        <location filename="dialogelencoprotocolli.cpp" line="1863"/>
        <location filename="dialogelencoprotocolli.cpp" line="1877"/>
        <location filename="dialogelencoprotocolli.cpp" line="2246"/>
        <source>addome</source>
        <translation>腹部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="258"/>
        <location filename="dialogelencoprotocolli.cpp" line="272"/>
        <location filename="dialogelencoprotocolli.cpp" line="286"/>
        <location filename="dialogelencoprotocolli.cpp" line="300"/>
        <location filename="dialogelencoprotocolli.cpp" line="542"/>
        <location filename="dialogelencoprotocolli.cpp" line="556"/>
        <location filename="dialogelencoprotocolli.cpp" line="570"/>
        <location filename="dialogelencoprotocolli.cpp" line="584"/>
        <location filename="dialogelencoprotocolli.cpp" line="815"/>
        <location filename="dialogelencoprotocolli.cpp" line="829"/>
        <location filename="dialogelencoprotocolli.cpp" line="843"/>
        <location filename="dialogelencoprotocolli.cpp" line="857"/>
        <location filename="dialogelencoprotocolli.cpp" line="1072"/>
        <location filename="dialogelencoprotocolli.cpp" line="1086"/>
        <location filename="dialogelencoprotocolli.cpp" line="1100"/>
        <location filename="dialogelencoprotocolli.cpp" line="1331"/>
        <location filename="dialogelencoprotocolli.cpp" line="1345"/>
        <location filename="dialogelencoprotocolli.cpp" line="1563"/>
        <location filename="dialogelencoprotocolli.cpp" line="1711"/>
        <location filename="dialogelencoprotocolli.cpp" line="1725"/>
        <location filename="dialogelencoprotocolli.cpp" line="1739"/>
        <location filename="dialogelencoprotocolli.cpp" line="1753"/>
        <source>zona dolente</source>
        <translation>疼痛部位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="259"/>
        <location filename="dialogelencoprotocolli.cpp" line="273"/>
        <location filename="dialogelencoprotocolli.cpp" line="337"/>
        <location filename="dialogelencoprotocolli.cpp" line="351"/>
        <location filename="dialogelencoprotocolli.cpp" line="365"/>
        <location filename="dialogelencoprotocolli.cpp" line="472"/>
        <location filename="dialogelencoprotocolli.cpp" line="486"/>
        <location filename="dialogelencoprotocolli.cpp" line="500"/>
        <location filename="dialogelencoprotocolli.cpp" line="543"/>
        <location filename="dialogelencoprotocolli.cpp" line="557"/>
        <location filename="dialogelencoprotocolli.cpp" line="607"/>
        <location filename="dialogelencoprotocolli.cpp" line="621"/>
        <location filename="dialogelencoprotocolli.cpp" line="709"/>
        <location filename="dialogelencoprotocolli.cpp" line="743"/>
        <location filename="dialogelencoprotocolli.cpp" line="757"/>
        <location filename="dialogelencoprotocolli.cpp" line="771"/>
        <location filename="dialogelencoprotocolli.cpp" line="816"/>
        <location filename="dialogelencoprotocolli.cpp" line="830"/>
        <location filename="dialogelencoprotocolli.cpp" line="881"/>
        <location filename="dialogelencoprotocolli.cpp" line="895"/>
        <location filename="dialogelencoprotocolli.cpp" line="909"/>
        <location filename="dialogelencoprotocolli.cpp" line="923"/>
        <location filename="dialogelencoprotocolli.cpp" line="1002"/>
        <location filename="dialogelencoprotocolli.cpp" line="1016"/>
        <location filename="dialogelencoprotocolli.cpp" line="1030"/>
        <location filename="dialogelencoprotocolli.cpp" line="1073"/>
        <location filename="dialogelencoprotocolli.cpp" line="1087"/>
        <location filename="dialogelencoprotocolli.cpp" line="1138"/>
        <location filename="dialogelencoprotocolli.cpp" line="1166"/>
        <location filename="dialogelencoprotocolli.cpp" line="1260"/>
        <location filename="dialogelencoprotocolli.cpp" line="1274"/>
        <location filename="dialogelencoprotocolli.cpp" line="1288"/>
        <location filename="dialogelencoprotocolli.cpp" line="1332"/>
        <location filename="dialogelencoprotocolli.cpp" line="1369"/>
        <location filename="dialogelencoprotocolli.cpp" line="1383"/>
        <location filename="dialogelencoprotocolli.cpp" line="1397"/>
        <location filename="dialogelencoprotocolli.cpp" line="1411"/>
        <location filename="dialogelencoprotocolli.cpp" line="1457"/>
        <location filename="dialogelencoprotocolli.cpp" line="1492"/>
        <location filename="dialogelencoprotocolli.cpp" line="1506"/>
        <location filename="dialogelencoprotocolli.cpp" line="1520"/>
        <location filename="dialogelencoprotocolli.cpp" line="1601"/>
        <location filename="dialogelencoprotocolli.cpp" line="1629"/>
        <location filename="dialogelencoprotocolli.cpp" line="1651"/>
        <location filename="dialogelencoprotocolli.cpp" line="1712"/>
        <location filename="dialogelencoprotocolli.cpp" line="1726"/>
        <location filename="dialogelencoprotocolli.cpp" line="1740"/>
        <location filename="dialogelencoprotocolli.cpp" line="1777"/>
        <location filename="dialogelencoprotocolli.cpp" line="1791"/>
        <location filename="dialogelencoprotocolli.cpp" line="1815"/>
        <location filename="dialogelencoprotocolli.cpp" line="1865"/>
        <location filename="dialogelencoprotocolli.cpp" line="1879"/>
        <location filename="dialogelencoprotocolli.cpp" line="1954"/>
        <location filename="dialogelencoprotocolli.cpp" line="1968"/>
        <location filename="dialogelencoprotocolli.cpp" line="2033"/>
        <location filename="dialogelencoprotocolli.cpp" line="2047"/>
        <location filename="dialogelencoprotocolli.cpp" line="2075"/>
        <location filename="dialogelencoprotocolli.cpp" line="2127"/>
        <location filename="dialogelencoprotocolli.cpp" line="2141"/>
        <location filename="dialogelencoprotocolli.cpp" line="2169"/>
        <location filename="dialogelencoprotocolli.cpp" line="2220"/>
        <location filename="dialogelencoprotocolli.cpp" line="2234"/>
        <location filename="dialogelencoprotocolli.cpp" line="2248"/>
        <location filename="dialogelencoprotocolli.cpp" line="2299"/>
        <location filename="dialogelencoprotocolli.cpp" line="2313"/>
        <location filename="dialogelencoprotocolli.cpp" line="2327"/>
        <location filename="dialogelencoprotocolli.cpp" line="2378"/>
        <source>DINAMICO 60</source>
        <translation>动态电极</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="264"/>
        <location filename="dialogelencoprotocolli.cpp" line="292"/>
        <location filename="dialogelencoprotocolli.cpp" line="328"/>
        <location filename="dialogelencoprotocolli.cpp" line="356"/>
        <location filename="dialogelencoprotocolli.cpp" line="414"/>
        <location filename="dialogelencoprotocolli.cpp" line="463"/>
        <location filename="dialogelencoprotocolli.cpp" line="491"/>
        <location filename="dialogelencoprotocolli.cpp" line="548"/>
        <location filename="dialogelencoprotocolli.cpp" line="576"/>
        <location filename="dialogelencoprotocolli.cpp" line="612"/>
        <location filename="dialogelencoprotocolli.cpp" line="648"/>
        <location filename="dialogelencoprotocolli.cpp" line="672"/>
        <location filename="dialogelencoprotocolli.cpp" line="686"/>
        <location filename="dialogelencoprotocolli.cpp" line="700"/>
        <location filename="dialogelencoprotocolli.cpp" line="734"/>
        <location filename="dialogelencoprotocolli.cpp" line="762"/>
        <location filename="dialogelencoprotocolli.cpp" line="821"/>
        <location filename="dialogelencoprotocolli.cpp" line="849"/>
        <location filename="dialogelencoprotocolli.cpp" line="886"/>
        <location filename="dialogelencoprotocolli.cpp" line="914"/>
        <location filename="dialogelencoprotocolli.cpp" line="973"/>
        <location filename="dialogelencoprotocolli.cpp" line="993"/>
        <location filename="dialogelencoprotocolli.cpp" line="1021"/>
        <location filename="dialogelencoprotocolli.cpp" line="1092"/>
        <location filename="dialogelencoprotocolli.cpp" line="1129"/>
        <location filename="dialogelencoprotocolli.cpp" line="1157"/>
        <location filename="dialogelencoprotocolli.cpp" line="1194"/>
        <location filename="dialogelencoprotocolli.cpp" line="1231"/>
        <location filename="dialogelencoprotocolli.cpp" line="1251"/>
        <location filename="dialogelencoprotocolli.cpp" line="1279"/>
        <location filename="dialogelencoprotocolli.cpp" line="1374"/>
        <location filename="dialogelencoprotocolli.cpp" line="1402"/>
        <location filename="dialogelencoprotocolli.cpp" line="1483"/>
        <location filename="dialogelencoprotocolli.cpp" line="1511"/>
        <location filename="dialogelencoprotocolli.cpp" line="1569"/>
        <location filename="dialogelencoprotocolli.cpp" line="1592"/>
        <location filename="dialogelencoprotocolli.cpp" line="1620"/>
        <location filename="dialogelencoprotocolli.cpp" line="1656"/>
        <location filename="dialogelencoprotocolli.cpp" line="1717"/>
        <location filename="dialogelencoprotocolli.cpp" line="1731"/>
        <location filename="dialogelencoprotocolli.cpp" line="1745"/>
        <location filename="dialogelencoprotocolli.cpp" line="1759"/>
        <location filename="dialogelencoprotocolli.cpp" line="1796"/>
        <location filename="dialogelencoprotocolli.cpp" line="1884"/>
        <location filename="dialogelencoprotocolli.cpp" line="1908"/>
        <location filename="dialogelencoprotocolli.cpp" line="1959"/>
        <location filename="dialogelencoprotocolli.cpp" line="1973"/>
        <location filename="dialogelencoprotocolli.cpp" line="1987"/>
        <location filename="dialogelencoprotocolli.cpp" line="2038"/>
        <location filename="dialogelencoprotocolli.cpp" line="2066"/>
        <location filename="dialogelencoprotocolli.cpp" line="2132"/>
        <location filename="dialogelencoprotocolli.cpp" line="2160"/>
        <location filename="dialogelencoprotocolli.cpp" line="2225"/>
        <location filename="dialogelencoprotocolli.cpp" line="2253"/>
        <location filename="dialogelencoprotocolli.cpp" line="2332"/>
        <location filename="dialogelencoprotocolli.cpp" line="2397"/>
        <source>RESISTIVO</source>
        <translation>阻</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="278"/>
        <location filename="dialogelencoprotocolli.cpp" line="306"/>
        <location filename="dialogelencoprotocolli.cpp" line="342"/>
        <location filename="dialogelencoprotocolli.cpp" line="370"/>
        <location filename="dialogelencoprotocolli.cpp" line="392"/>
        <location filename="dialogelencoprotocolli.cpp" line="428"/>
        <location filename="dialogelencoprotocolli.cpp" line="442"/>
        <location filename="dialogelencoprotocolli.cpp" line="477"/>
        <location filename="dialogelencoprotocolli.cpp" line="505"/>
        <location filename="dialogelencoprotocolli.cpp" line="562"/>
        <location filename="dialogelencoprotocolli.cpp" line="590"/>
        <location filename="dialogelencoprotocolli.cpp" line="626"/>
        <location filename="dialogelencoprotocolli.cpp" line="714"/>
        <location filename="dialogelencoprotocolli.cpp" line="748"/>
        <location filename="dialogelencoprotocolli.cpp" line="776"/>
        <location filename="dialogelencoprotocolli.cpp" line="835"/>
        <location filename="dialogelencoprotocolli.cpp" line="863"/>
        <location filename="dialogelencoprotocolli.cpp" line="900"/>
        <location filename="dialogelencoprotocolli.cpp" line="928"/>
        <location filename="dialogelencoprotocolli.cpp" line="950"/>
        <location filename="dialogelencoprotocolli.cpp" line="1007"/>
        <location filename="dialogelencoprotocolli.cpp" line="1035"/>
        <location filename="dialogelencoprotocolli.cpp" line="1078"/>
        <location filename="dialogelencoprotocolli.cpp" line="1106"/>
        <location filename="dialogelencoprotocolli.cpp" line="1143"/>
        <location filename="dialogelencoprotocolli.cpp" line="1171"/>
        <location filename="dialogelencoprotocolli.cpp" line="1208"/>
        <location filename="dialogelencoprotocolli.cpp" line="1265"/>
        <location filename="dialogelencoprotocolli.cpp" line="1293"/>
        <location filename="dialogelencoprotocolli.cpp" line="1337"/>
        <location filename="dialogelencoprotocolli.cpp" line="1351"/>
        <location filename="dialogelencoprotocolli.cpp" line="1388"/>
        <location filename="dialogelencoprotocolli.cpp" line="1416"/>
        <location filename="dialogelencoprotocolli.cpp" line="1438"/>
        <location filename="dialogelencoprotocolli.cpp" line="1462"/>
        <location filename="dialogelencoprotocolli.cpp" line="1497"/>
        <location filename="dialogelencoprotocolli.cpp" line="1525"/>
        <location filename="dialogelencoprotocolli.cpp" line="1606"/>
        <location filename="dialogelencoprotocolli.cpp" line="1634"/>
        <location filename="dialogelencoprotocolli.cpp" line="1670"/>
        <location filename="dialogelencoprotocolli.cpp" line="1782"/>
        <location filename="dialogelencoprotocolli.cpp" line="1820"/>
        <location filename="dialogelencoprotocolli.cpp" line="1870"/>
        <location filename="dialogelencoprotocolli.cpp" line="2052"/>
        <location filename="dialogelencoprotocolli.cpp" line="2080"/>
        <location filename="dialogelencoprotocolli.cpp" line="2146"/>
        <location filename="dialogelencoprotocolli.cpp" line="2174"/>
        <location filename="dialogelencoprotocolli.cpp" line="2239"/>
        <location filename="dialogelencoprotocolli.cpp" line="2304"/>
        <location filename="dialogelencoprotocolli.cpp" line="2318"/>
        <location filename="dialogelencoprotocolli.cpp" line="2383"/>
        <source>CAPACITIVO</source>
        <translation>电容</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="301"/>
        <source>STATICO</source>
        <translation>静态电极</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="320"/>
        <location filename="dialogelencoprotocolli.cpp" line="334"/>
        <location filename="dialogelencoprotocolli.cpp" line="362"/>
        <location filename="dialogelencoprotocolli.cpp" line="384"/>
        <location filename="dialogelencoprotocolli.cpp" line="406"/>
        <location filename="dialogelencoprotocolli.cpp" line="420"/>
        <location filename="dialogelencoprotocolli.cpp" line="434"/>
        <location filename="dialogelencoprotocolli.cpp" line="455"/>
        <location filename="dialogelencoprotocolli.cpp" line="469"/>
        <location filename="dialogelencoprotocolli.cpp" line="497"/>
        <location filename="dialogelencoprotocolli.cpp" line="640"/>
        <location filename="dialogelencoprotocolli.cpp" line="664"/>
        <location filename="dialogelencoprotocolli.cpp" line="678"/>
        <location filename="dialogelencoprotocolli.cpp" line="692"/>
        <location filename="dialogelencoprotocolli.cpp" line="706"/>
        <location filename="dialogelencoprotocolli.cpp" line="726"/>
        <location filename="dialogelencoprotocolli.cpp" line="740"/>
        <location filename="dialogelencoprotocolli.cpp" line="768"/>
        <location filename="dialogelencoprotocolli.cpp" line="878"/>
        <location filename="dialogelencoprotocolli.cpp" line="892"/>
        <location filename="dialogelencoprotocolli.cpp" line="942"/>
        <location filename="dialogelencoprotocolli.cpp" line="985"/>
        <location filename="dialogelencoprotocolli.cpp" line="999"/>
        <location filename="dialogelencoprotocolli.cpp" line="1027"/>
        <location filename="dialogelencoprotocolli.cpp" line="1121"/>
        <location filename="dialogelencoprotocolli.cpp" line="1186"/>
        <location filename="dialogelencoprotocolli.cpp" line="1200"/>
        <location filename="dialogelencoprotocolli.cpp" line="1243"/>
        <location filename="dialogelencoprotocolli.cpp" line="1257"/>
        <location filename="dialogelencoprotocolli.cpp" line="1285"/>
        <location filename="dialogelencoprotocolli.cpp" line="1366"/>
        <location filename="dialogelencoprotocolli.cpp" line="1380"/>
        <location filename="dialogelencoprotocolli.cpp" line="1430"/>
        <location filename="dialogelencoprotocolli.cpp" line="1454"/>
        <location filename="dialogelencoprotocolli.cpp" line="1475"/>
        <location filename="dialogelencoprotocolli.cpp" line="1489"/>
        <location filename="dialogelencoprotocolli.cpp" line="1517"/>
        <location filename="dialogelencoprotocolli.cpp" line="1584"/>
        <location filename="dialogelencoprotocolli.cpp" line="1951"/>
        <location filename="dialogelencoprotocolli.cpp" line="1965"/>
        <location filename="dialogelencoprotocolli.cpp" line="1979"/>
        <location filename="dialogelencoprotocolli.cpp" line="2030"/>
        <location filename="dialogelencoprotocolli.cpp" line="2044"/>
        <location filename="dialogelencoprotocolli.cpp" line="2058"/>
        <location filename="dialogelencoprotocolli.cpp" line="2072"/>
        <location filename="dialogelencoprotocolli.cpp" line="2124"/>
        <location filename="dialogelencoprotocolli.cpp" line="2138"/>
        <location filename="dialogelencoprotocolli.cpp" line="2152"/>
        <location filename="dialogelencoprotocolli.cpp" line="2166"/>
        <location filename="dialogelencoprotocolli.cpp" line="2217"/>
        <location filename="dialogelencoprotocolli.cpp" line="2310"/>
        <source>supino</source>
        <translation>仰卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="321"/>
        <location filename="dialogelencoprotocolli.cpp" line="349"/>
        <location filename="dialogelencoprotocolli.cpp" line="407"/>
        <location filename="dialogelencoprotocolli.cpp" line="456"/>
        <location filename="dialogelencoprotocolli.cpp" line="484"/>
        <location filename="dialogelencoprotocolli.cpp" line="727"/>
        <location filename="dialogelencoprotocolli.cpp" line="755"/>
        <location filename="dialogelencoprotocolli.cpp" line="966"/>
        <location filename="dialogelencoprotocolli.cpp" line="986"/>
        <location filename="dialogelencoprotocolli.cpp" line="1014"/>
        <location filename="dialogelencoprotocolli.cpp" line="1244"/>
        <location filename="dialogelencoprotocolli.cpp" line="1272"/>
        <location filename="dialogelencoprotocolli.cpp" line="1476"/>
        <location filename="dialogelencoprotocolli.cpp" line="1504"/>
        <source>diaframma</source>
        <translation>膈肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="408"/>
        <location filename="dialogelencoprotocolli.cpp" line="422"/>
        <location filename="dialogelencoprotocolli.cpp" line="436"/>
        <source>sulla lesione</source>
        <translation>在损伤处</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="287"/>
        <location filename="dialogelencoprotocolli.cpp" line="323"/>
        <location filename="dialogelencoprotocolli.cpp" line="409"/>
        <location filename="dialogelencoprotocolli.cpp" line="423"/>
        <location filename="dialogelencoprotocolli.cpp" line="437"/>
        <location filename="dialogelencoprotocolli.cpp" line="458"/>
        <location filename="dialogelencoprotocolli.cpp" line="571"/>
        <location filename="dialogelencoprotocolli.cpp" line="585"/>
        <location filename="dialogelencoprotocolli.cpp" line="667"/>
        <location filename="dialogelencoprotocolli.cpp" line="729"/>
        <location filename="dialogelencoprotocolli.cpp" line="844"/>
        <location filename="dialogelencoprotocolli.cpp" line="858"/>
        <location filename="dialogelencoprotocolli.cpp" line="988"/>
        <location filename="dialogelencoprotocolli.cpp" line="1246"/>
        <location filename="dialogelencoprotocolli.cpp" line="1346"/>
        <location filename="dialogelencoprotocolli.cpp" line="1478"/>
        <location filename="dialogelencoprotocolli.cpp" line="1564"/>
        <source>STATICO grande</source>
        <translation>大静态电极</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="128"/>
        <source>LOMBALGIA</source>
        <translation>下腰背疼痛</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="129"/>
        <source>LOMBOSCIALTAGIA SUB ACUTA</source>
        <translation>腰背痛 亚急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="130"/>
        <source>PERIARTRITE SPALLA FASE ACUTA (DINAMICI)</source>
        <translation>肩周炎 急性期 动态</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="131"/>
        <source>PERIARTRITE SPALLA FASE ACUTA (STATICI)</source>
        <translation>肩周炎 急性期 静止</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="132"/>
        <source>PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (DINAMICI)</source>
        <translation>肩周炎 亚急性期或慢性期 动态</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="133"/>
        <source>PERIARTRITE SPALLA FASE SUB ACUTA-CRONICA (STATICI)</source>
        <translation>肩周炎 亚急性期或慢性期 静止</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="134"/>
        <source>SPALLA FASE ACUTA</source>
        <translation>肩部 急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="136"/>
        <source>SPALLA FASE SUB CUTA</source>
        <translation>肩周炎 亚急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="137"/>
        <source>EPICONDILITE DA SOVRACCARICO</source>
        <translation>过载引起的上踝炎</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="138"/>
        <source>POLSO FASE ACUTA</source>
        <translation>手腕疼痛 急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="139"/>
        <source>POLSO FASE SUB  ACUTA O CRONICA</source>
        <translation>手腕疼痛 亚急性期或恢复期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="140"/>
        <source>COXARTROSI SUB ACUTA E CRONICA</source>
        <translation>股骨头坏死 亚急性期或恢复期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="141"/>
        <source>DISTORSIONE CAVIGLIA FASE ACUTA</source>
        <translation>踝关节扭伤 急性期</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="142"/>
        <source>DISTORSIONE CAVIGLIA FASE SUB ACUTA</source>
        <translation>踝关节变形 亚急性阶段</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="162"/>
        <source>GINOCCHIO POST CHIRURGICO</source>
        <translation>膝部（术后）</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="164"/>
        <source>PRIMA FASE - DRENAGGIO</source>
        <translation>第一阶段：淋巴引流</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="165"/>
        <source>PRIMA FASE - ANTINFIAMMATORIO</source>
        <translation>第一阶段：抗发炎</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="166"/>
        <source>SECONDA FASE - MOBILIZZAZZIONE PASSIVA</source>
        <translation>第二阶段：被动运动</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="167"/>
        <source>TERZA FASE - MOBILIZZAZZIONE ATTIVA</source>
        <translation>第三阶段：主动运动</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="168"/>
        <source>QUARTA FASE - MOBILIZZAZZIONE ATTIVA CON RESISTENZA</source>
        <translation>第四阶段：主动抗阻运动</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="169"/>
        <source>QUINTA FASE - RECUPERO DEFICIT DI FLESSIONE</source>
        <translation>第五阶段：恢复屈曲不足</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="170"/>
        <source>SESTA FASE - RECUPERO DEFICIT IN FLESSO/ESTENSIONE</source>
        <translation>第六阶段：恢复屈/伸不足</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="172"/>
        <source>SESTA FASE - RECUPERO MOBILITA ARTICOLARE</source>
        <translation>第六阶段：恢复关节活动度</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="204"/>
        <source>TRATTAMENTO DELLE LESIONI SPINALI PERIFERICHE</source>
        <translation>周围脊柱损伤</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="226"/>
        <source>PERIARTRITE DELLA SPALLA</source>
        <translation>肩周炎</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="227"/>
        <source>SPONDILOSI CERVICALE</source>
        <translation>颈椎病</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="228"/>
        <source>ERNIA DEL DISCO LOMBARE</source>
        <translation>腰椎间盘突出症</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="229"/>
        <source>SFORZO MUSCOLARE</source>
        <translation>肌肉劳损</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="230"/>
        <source>CUSTOM</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="336"/>
        <location filename="dialogelencoprotocolli.cpp" line="471"/>
        <location filename="dialogelencoprotocolli.cpp" line="742"/>
        <location filename="dialogelencoprotocolli.cpp" line="1001"/>
        <location filename="dialogelencoprotocolli.cpp" line="1259"/>
        <location filename="dialogelencoprotocolli.cpp" line="1491"/>
        <source>muscolo psoas</source>
        <translation>腰大肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="350"/>
        <location filename="dialogelencoprotocolli.cpp" line="485"/>
        <location filename="dialogelencoprotocolli.cpp" line="756"/>
        <location filename="dialogelencoprotocolli.cpp" line="1015"/>
        <location filename="dialogelencoprotocolli.cpp" line="1273"/>
        <location filename="dialogelencoprotocolli.cpp" line="1505"/>
        <source>L1-L4 e piliforme</source>
        <translation>L1-L4梨状肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="364"/>
        <location filename="dialogelencoprotocolli.cpp" line="499"/>
        <location filename="dialogelencoprotocolli.cpp" line="770"/>
        <location filename="dialogelencoprotocolli.cpp" line="1029"/>
        <location filename="dialogelencoprotocolli.cpp" line="1287"/>
        <location filename="dialogelencoprotocolli.cpp" line="1519"/>
        <source>zona lombare e glutei</source>
        <translation>腰部和臀肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="421"/>
        <location filename="dialogelencoprotocolli.cpp" line="694"/>
        <location filename="dialogelencoprotocolli.cpp" line="2032"/>
        <location filename="dialogelencoprotocolli.cpp" line="2060"/>
        <location filename="dialogelencoprotocolli.cpp" line="2126"/>
        <location filename="dialogelencoprotocolli.cpp" line="2154"/>
        <source>polso</source>
        <translation>手腕</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="435"/>
        <location filename="dialogelencoprotocolli.cpp" line="2297"/>
        <location filename="dialogelencoprotocolli.cpp" line="2376"/>
        <location filename="dialogelencoprotocolli.cpp" line="2391"/>
        <source>pianta del piede</source>
        <translation>足底</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="606"/>
        <source>inserzione nervo sciatico fino irradiazione dolore</source>
        <translation>痛点位置</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="620"/>
        <source>muscoli paravertebrali, glutei, bicipiti femorali, polpacci e tibiali</source>
        <translation>椎旁肌肉，臀肌，股二头肌，小腿肌肉，胫骨肌肉</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1587"/>
        <source>RESISTIVO piccolo</source>
        <translation>小电阻</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1649"/>
        <location filename="dialogelencoprotocolli.cpp" line="1663"/>
        <location filename="dialogelencoprotocolli.cpp" line="1901"/>
        <source>bicipite femorale</source>
        <translation>肱二头肌部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1650"/>
        <source>inserzione vasto mediale,vasto laterale,retto femorale</source>
        <translation>股内侧肌，股外侧肌附着点</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1776"/>
        <location filename="dialogelencoprotocolli.cpp" line="1790"/>
        <location filename="dialogelencoprotocolli.cpp" line="1864"/>
        <location filename="dialogelencoprotocolli.cpp" line="1878"/>
        <source>tricipite brachiale,sovraspinato,infraspinato,trapezio,pettorali,bicipite brachiale</source>
        <translation>肱三头肌，冈上肌，冈下肌，斜方肌，胸肌，肱二头肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1814"/>
        <source>bicipite femorale,polpaccio,tibiale</source>
        <translation>股二头肌，小腿肌肉，胫骨肌肉</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1953"/>
        <source>braccio e avambraccio</source>
        <translation>上臂和前臂</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1967"/>
        <source>inserzione tendini</source>
        <translation>肌腱附着点</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2218"/>
        <source>lombo sacrale</source>
        <translation>腰骶部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2219"/>
        <source>zona trocanterica e ischiatica</source>
        <translation>股骨大转子和坐骨部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2231"/>
        <source>supina</source>
        <translation>仰卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2247"/>
        <source>gluteo e bicipite femorale</source>
        <translation>臀肌和股二头肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2298"/>
        <location filename="dialogelencoprotocolli.cpp" line="2377"/>
        <source>gastrocnemius mediale e laterale e soleo</source>
        <translation>内外侧腓肠肌和比目鱼肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2311"/>
        <location filename="dialogelencoprotocolli.cpp" line="2390"/>
        <source>gastrocnemius</source>
        <translation>腓肠肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2312"/>
        <source>dorso del piede</source>
        <translation>脚背</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2325"/>
        <source>gastrocnemius pos</source>
        <translation>腓肠肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2326"/>
        <source>pianta del piede, soleo e gastrocnemius laterale</source>
        <translation>足底，比目鱼肌和外侧腓肠肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="665"/>
        <location filename="dialogelencoprotocolli.cpp" line="679"/>
        <location filename="dialogelencoprotocolli.cpp" line="693"/>
        <location filename="dialogelencoprotocolli.cpp" line="707"/>
        <source>zona cervico/dorsale</source>
        <translation>颈/背部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="666"/>
        <source>muscolo retto femorale</source>
        <translation>股直肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="680"/>
        <source>articolazione gleno omerale</source>
        <translation>盂肱关节</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="387"/>
        <location filename="dialogelencoprotocolli.cpp" line="643"/>
        <location filename="dialogelencoprotocolli.cpp" line="681"/>
        <location filename="dialogelencoprotocolli.cpp" line="695"/>
        <location filename="dialogelencoprotocolli.cpp" line="945"/>
        <location filename="dialogelencoprotocolli.cpp" line="968"/>
        <location filename="dialogelencoprotocolli.cpp" line="1101"/>
        <location filename="dialogelencoprotocolli.cpp" line="1124"/>
        <location filename="dialogelencoprotocolli.cpp" line="1152"/>
        <location filename="dialogelencoprotocolli.cpp" line="1189"/>
        <location filename="dialogelencoprotocolli.cpp" line="1203"/>
        <location filename="dialogelencoprotocolli.cpp" line="1226"/>
        <location filename="dialogelencoprotocolli.cpp" line="1433"/>
        <location filename="dialogelencoprotocolli.cpp" line="1615"/>
        <location filename="dialogelencoprotocolli.cpp" line="1665"/>
        <location filename="dialogelencoprotocolli.cpp" line="1754"/>
        <location filename="dialogelencoprotocolli.cpp" line="1903"/>
        <location filename="dialogelencoprotocolli.cpp" line="1982"/>
        <location filename="dialogelencoprotocolli.cpp" line="2061"/>
        <location filename="dialogelencoprotocolli.cpp" line="2155"/>
        <location filename="dialogelencoprotocolli.cpp" line="2392"/>
        <source>STATICO piccolo</source>
        <translation>小静态电极</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="604"/>
        <location filename="dialogelencoprotocolli.cpp" line="618"/>
        <source>decubito laterale prono</source>
        <translation>侧卧位或俯卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="641"/>
        <location filename="dialogelencoprotocolli.cpp" line="1187"/>
        <source>gadtrocnemius</source>
        <translation>腓肠肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="642"/>
        <location filename="dialogelencoprotocolli.cpp" line="944"/>
        <location filename="dialogelencoprotocolli.cpp" line="1188"/>
        <location filename="dialogelencoprotocolli.cpp" line="1202"/>
        <location filename="dialogelencoprotocolli.cpp" line="1432"/>
        <location filename="dialogelencoprotocolli.cpp" line="1664"/>
        <location filename="dialogelencoprotocolli.cpp" line="1813"/>
        <location filename="dialogelencoprotocolli.cpp" line="1902"/>
        <source>retto femorale</source>
        <translation>股直肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="708"/>
        <source>arto superiore plegico</source>
        <translation>上肢偏瘫部位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1149"/>
        <location filename="dialogelencoprotocolli.cpp" line="1612"/>
        <location filename="dialogelencoprotocolli.cpp" line="1648"/>
        <location filename="dialogelencoprotocolli.cpp" line="1662"/>
        <location filename="dialogelencoprotocolli.cpp" line="1774"/>
        <location filename="dialogelencoprotocolli.cpp" line="1788"/>
        <location filename="dialogelencoprotocolli.cpp" line="1862"/>
        <location filename="dialogelencoprotocolli.cpp" line="1876"/>
        <location filename="dialogelencoprotocolli.cpp" line="1900"/>
        <source>seduto</source>
        <translation>坐位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="965"/>
        <location filename="dialogelencoprotocolli.cpp" line="1223"/>
        <source>confortevole</source>
        <translation>舒适体位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="967"/>
        <source>compressione intervertebrale</source>
        <translation>椎体压痛点</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="335"/>
        <location filename="dialogelencoprotocolli.cpp" line="363"/>
        <location filename="dialogelencoprotocolli.cpp" line="385"/>
        <location filename="dialogelencoprotocolli.cpp" line="470"/>
        <location filename="dialogelencoprotocolli.cpp" line="498"/>
        <location filename="dialogelencoprotocolli.cpp" line="741"/>
        <location filename="dialogelencoprotocolli.cpp" line="769"/>
        <location filename="dialogelencoprotocolli.cpp" line="893"/>
        <location filename="dialogelencoprotocolli.cpp" line="943"/>
        <location filename="dialogelencoprotocolli.cpp" line="1000"/>
        <location filename="dialogelencoprotocolli.cpp" line="1028"/>
        <location filename="dialogelencoprotocolli.cpp" line="1201"/>
        <location filename="dialogelencoprotocolli.cpp" line="1258"/>
        <location filename="dialogelencoprotocolli.cpp" line="1286"/>
        <location filename="dialogelencoprotocolli.cpp" line="1381"/>
        <location filename="dialogelencoprotocolli.cpp" line="1431"/>
        <location filename="dialogelencoprotocolli.cpp" line="1490"/>
        <location filename="dialogelencoprotocolli.cpp" line="1518"/>
        <source>zona lombare</source>
        <translation>腰部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="879"/>
        <location filename="dialogelencoprotocolli.cpp" line="1367"/>
        <location filename="dialogelencoprotocolli.cpp" line="1952"/>
        <location filename="dialogelencoprotocolli.cpp" line="1966"/>
        <location filename="dialogelencoprotocolli.cpp" line="1980"/>
        <source>scapola</source>
        <translation>肩胛骨</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="880"/>
        <location filename="dialogelencoprotocolli.cpp" line="1368"/>
        <source>deltoide e bicipiti brachiali</source>
        <translation>三角肌和肱二头肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="894"/>
        <location filename="dialogelencoprotocolli.cpp" line="1382"/>
        <source>pettorale e bicipiti brachiali</source>
        <translation>胸肌和肱二头肌</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="907"/>
        <location filename="dialogelencoprotocolli.cpp" line="1122"/>
        <location filename="dialogelencoprotocolli.cpp" line="1395"/>
        <location filename="dialogelencoprotocolli.cpp" line="1585"/>
        <location filename="dialogelencoprotocolli.cpp" line="2031"/>
        <location filename="dialogelencoprotocolli.cpp" line="2045"/>
        <location filename="dialogelencoprotocolli.cpp" line="2059"/>
        <location filename="dialogelencoprotocolli.cpp" line="2073"/>
        <location filename="dialogelencoprotocolli.cpp" line="2125"/>
        <location filename="dialogelencoprotocolli.cpp" line="2139"/>
        <location filename="dialogelencoprotocolli.cpp" line="2153"/>
        <location filename="dialogelencoprotocolli.cpp" line="2167"/>
        <location filename="dialogelencoprotocolli.cpp" line="2232"/>
        <source>braccio</source>
        <translation>上臂</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="908"/>
        <location filename="dialogelencoprotocolli.cpp" line="1396"/>
        <source>sotto scapolare e scapolare</source>
        <translation>肩胛骨下部和肩胛部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="920"/>
        <location filename="dialogelencoprotocolli.cpp" line="1135"/>
        <location filename="dialogelencoprotocolli.cpp" line="1163"/>
        <location filename="dialogelencoprotocolli.cpp" line="1408"/>
        <location filename="dialogelencoprotocolli.cpp" line="1598"/>
        <location filename="dialogelencoprotocolli.cpp" line="1626"/>
        <source>su di un lato</source>
        <translation>侧卧位</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="921"/>
        <location filename="dialogelencoprotocolli.cpp" line="1136"/>
        <location filename="dialogelencoprotocolli.cpp" line="1164"/>
        <location filename="dialogelencoprotocolli.cpp" line="1409"/>
        <location filename="dialogelencoprotocolli.cpp" line="1599"/>
        <location filename="dialogelencoprotocolli.cpp" line="1627"/>
        <source>gomito</source>
        <translation>肘部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="922"/>
        <location filename="dialogelencoprotocolli.cpp" line="1137"/>
        <location filename="dialogelencoprotocolli.cpp" line="1165"/>
        <location filename="dialogelencoprotocolli.cpp" line="1410"/>
        <location filename="dialogelencoprotocolli.cpp" line="1600"/>
        <location filename="dialogelencoprotocolli.cpp" line="1628"/>
        <source>deltoide e zona scapolare</source>
        <translation>三角肌和肩胛部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1123"/>
        <location filename="dialogelencoprotocolli.cpp" line="1586"/>
        <source>zona scapolare</source>
        <translation>肩胛部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1151"/>
        <location filename="dialogelencoprotocolli.cpp" line="1614"/>
        <source>bicipite brachiale</source>
        <translation>颈-背部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1394"/>
        <location filename="dialogelencoprotocolli.cpp" line="2245"/>
        <source>prona</source>
        <translation>俯卧</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1455"/>
        <source>lombare</source>
        <translation>腰部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1456"/>
        <source>muscoli denervati</source>
        <translation>肌肉损伤</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1224"/>
        <source>plesso brachiale posteriore</source>
        <translation>后臂丛神经</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1225"/>
        <source>plesso brachiale anteriore</source>
        <translation>前臂丛神经</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="1150"/>
        <location filename="dialogelencoprotocolli.cpp" line="1613"/>
        <location filename="dialogelencoprotocolli.cpp" line="1981"/>
        <location filename="dialogelencoprotocolli.cpp" line="2046"/>
        <location filename="dialogelencoprotocolli.cpp" line="2074"/>
        <location filename="dialogelencoprotocolli.cpp" line="2140"/>
        <location filename="dialogelencoprotocolli.cpp" line="2168"/>
        <source>avambraccio</source>
        <translation>前臂</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="322"/>
        <location filename="dialogelencoprotocolli.cpp" line="457"/>
        <location filename="dialogelencoprotocolli.cpp" line="728"/>
        <location filename="dialogelencoprotocolli.cpp" line="987"/>
        <location filename="dialogelencoprotocolli.cpp" line="1245"/>
        <location filename="dialogelencoprotocolli.cpp" line="1477"/>
        <source>lombosacrale</source>
        <translation>腰骶部</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="2233"/>
        <source>coscia</source>
        <translation>大腿</translation>
    </message>
    <message>
        <location filename="dialogelencoprotocolli.cpp" line="386"/>
        <source>cavo popliteo</source>
        <translation>腘窝</translation>
    </message>
</context>
<context>
    <name>dialogMenuGuida</name>
    <message>
        <location filename="dialogmenuguida.cpp" line="37"/>
        <source>PROTOCOLLI</source>
        <translation>处方</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="38"/>
        <source>GENERICI</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="39"/>
        <location filename="dialogmenuguida.cpp" line="41"/>
        <source>PROTOCOLLI PATOLOGIE</source>
        <translation>常见病症处方</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="40"/>
        <source>NEUROLOGICI</source>
        <translation>神經</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="42"/>
        <source>ORTOPEDICI</source>
        <translation>骨科</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="44"/>
        <source>Descrizione protocolli generici</source>
        <translation>结合运动疗法的动/静态治疗处方</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="45"/>
        <source>Descrizione protocolli neurologici</source>
        <translation>适用于神经损伤患者的治疗处方</translation>
    </message>
    <message>
        <location filename="dialogmenuguida.cpp" line="46"/>
        <source>Descrizione protocolli ortopedici</source>
        <translation>适用于骨损伤患者的治疗处方</translation>
    </message>
</context>
<context>
    <name>dialogSelezionaProgramma</name>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="58"/>
        <location filename="dialogselezionaprogramma.cpp" line="59"/>
        <location filename="dialogselezionaprogramma.cpp" line="60"/>
        <location filename="dialogselezionaprogramma.cpp" line="61"/>
        <source>Posizione Neutro :</source>
        <translation>负极位置 :</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="53"/>
        <location filename="dialogselezionaprogramma.cpp" line="54"/>
        <location filename="dialogselezionaprogramma.cpp" line="55"/>
        <location filename="dialogselezionaprogramma.cpp" line="56"/>
        <source>Posizione Paziente :</source>
        <translation>患者体位 :</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="63"/>
        <location filename="dialogselezionaprogramma.cpp" line="64"/>
        <location filename="dialogselezionaprogramma.cpp" line="65"/>
        <location filename="dialogselezionaprogramma.cpp" line="66"/>
        <source>Posizione Elettrodo :</source>
        <translation>工作电极位置 :</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="68"/>
        <location filename="dialogselezionaprogramma.cpp" line="69"/>
        <location filename="dialogselezionaprogramma.cpp" line="70"/>
        <location filename="dialogselezionaprogramma.cpp" line="71"/>
        <source>Elettrodo</source>
        <translation>电极</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="84"/>
        <location filename="dialogselezionaprogramma.cpp" line="164"/>
        <location filename="dialogselezionaprogramma.cpp" line="189"/>
        <location filename="dialogselezionaprogramma.cpp" line="214"/>
        <location filename="dialogselezionaprogramma.cpp" line="239"/>
        <source>POTENZA</source>
        <translation>功率</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="85"/>
        <location filename="dialogselezionaprogramma.cpp" line="165"/>
        <location filename="dialogselezionaprogramma.cpp" line="190"/>
        <location filename="dialogselezionaprogramma.cpp" line="215"/>
        <location filename="dialogselezionaprogramma.cpp" line="240"/>
        <source>MODALITA</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="86"/>
        <location filename="dialogselezionaprogramma.cpp" line="166"/>
        <location filename="dialogselezionaprogramma.cpp" line="167"/>
        <location filename="dialogselezionaprogramma.cpp" line="191"/>
        <location filename="dialogselezionaprogramma.cpp" line="192"/>
        <location filename="dialogselezionaprogramma.cpp" line="216"/>
        <location filename="dialogselezionaprogramma.cpp" line="217"/>
        <location filename="dialogselezionaprogramma.cpp" line="241"/>
        <location filename="dialogselezionaprogramma.cpp" line="242"/>
        <source>FREQUENZA</source>
        <translation>频率</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="87"/>
        <location filename="dialogselezionaprogramma.cpp" line="168"/>
        <location filename="dialogselezionaprogramma.cpp" line="169"/>
        <location filename="dialogselezionaprogramma.cpp" line="170"/>
        <location filename="dialogselezionaprogramma.cpp" line="193"/>
        <location filename="dialogselezionaprogramma.cpp" line="194"/>
        <location filename="dialogselezionaprogramma.cpp" line="195"/>
        <location filename="dialogselezionaprogramma.cpp" line="218"/>
        <location filename="dialogselezionaprogramma.cpp" line="219"/>
        <location filename="dialogselezionaprogramma.cpp" line="220"/>
        <location filename="dialogselezionaprogramma.cpp" line="243"/>
        <location filename="dialogselezionaprogramma.cpp" line="244"/>
        <location filename="dialogselezionaprogramma.cpp" line="245"/>
        <source>SLF</source>
        <translation>调制频率</translation>
    </message>
    <message>
        <location filename="dialogselezionaprogramma.cpp" line="163"/>
        <location filename="dialogselezionaprogramma.cpp" line="188"/>
        <location filename="dialogselezionaprogramma.cpp" line="213"/>
        <location filename="dialogselezionaprogramma.cpp" line="238"/>
        <source>TEMPO</source>
        <translation>时间</translation>
    </message>
</context>
<context>
    <name>dialogSelezionaabcd</name>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="42"/>
        <source>CAPACITIVO DINAMICO</source>
        <translation>动态电容</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="43"/>
        <source>CAPACITIVO STATICO GRANDE</source>
        <translation>大静态电容</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="44"/>
        <source>CAPACITIVO STATICO PICCOLO</source>
        <translation>小静态电容</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="45"/>
        <source>RESISTIVO DINAMICO</source>
        <translation>动态电阻</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="46"/>
        <source>RESISTIVO STATICO GRANDE</source>
        <translation>大静态电阻</translation>
    </message>
    <message>
        <location filename="dialogselezionatrattamento.cpp" line="47"/>
        <source>RESISTIVO STATICO PICCOLO</source>
        <translation>小静态电阻</translation>
    </message>
</context>
<context>
    <name>dialogSetup</name>
    <message>
        <location filename="dialogsetup.ui" line="47"/>
        <source>Setup</source>
        <translation>組態</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="47"/>
        <source>Tipo 1</source>
        <translation>CT200</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="48"/>
        <source>Tipo 2</source>
        <translation>CT250</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="49"/>
        <source>Tipo 3</source>
        <translation>CT300</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="51"/>
        <source>Tipo di macchina</source>
        <translation>機的類型</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="98"/>
        <source>Vecchia password</source>
        <translation>舊密碼</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="110"/>
        <source>Nuova password</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="dialogsetup.cpp" line="113"/>
        <source>Riscrivi password</source>
        <translation>重新輸入密碼</translation>
    </message>
</context>
<context>
    <name>dialogStartCalibrazione</name>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="31"/>
        <source>PROCEDURA DI CALIBRAZIONE</source>
        <translation>校準程序</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="32"/>
        <source>Info calibrazione</source>
        <translation>请正确插入电极后开始校准过程</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="79"/>
        <source>Vecchia password</source>
        <translation>舊密碼</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="92"/>
        <source>Nuova password</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="dialogstartcalibrazione.cpp" line="95"/>
        <source>Riscrivi password</source>
        <translation>重新輸入密碼</translation>
    </message>
</context>
<context>
    <name>dialogUser</name>
    <message>
        <location filename="dieloguser.cpp" line="79"/>
        <source>CALIBRA</source>
        <translation>校准</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="80"/>
        <source>OROLOGIO</source>
        <translation>时钟</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="142"/>
        <source>Vecchia password</source>
        <translation>舊密碼</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="154"/>
        <source>Nuova password</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="dieloguser.cpp" line="157"/>
        <source>Riscrivi password</source>
        <translation>重新輸入密碼</translation>
    </message>
</context>
<context>
    <name>dielogUser</name>
    <message>
        <source>CALIBRA</source>
        <translation type="obsolete">校准</translation>
    </message>
    <message>
        <source>OROLOGIO</source>
        <translation type="obsolete">时钟</translation>
    </message>
    <message>
        <source>Vecchia password</source>
        <translation type="obsolete">舊密碼</translation>
    </message>
    <message>
        <source>Nuova password</source>
        <translation type="obsolete">新密碼</translation>
    </message>
    <message>
        <source>Riscrivi password</source>
        <translation type="obsolete">重新輸入密碼</translation>
    </message>
</context>
<context>
    <name>ufgOrologio</name>
    <message>
        <location filename="ufgorologio.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
</TS>
