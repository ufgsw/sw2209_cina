#include "dialodselezionaab.h"
#include "ui_dialodselezionaab.h"
#include "QPainter"
#include <QPropertyAnimation>

dialodSelezionaAB::dialodSelezionaAB(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialodSelezionaAB)
{
    ui->setupUi(this);
    ui->buttonOk->setImage(":/img/ico/ok.png",":/img/ico/ok.png",":/img/ico/ok.png", 100 );
    //ui->buttonOk->setBordoDimensione(2);

    ui->selezioneA->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->selezioneB->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );

    connect( ui->selezioneA,  SIGNAL(buttonClicked()), this, SLOT(selezionatoA()));
    connect( ui->selezioneB,  SIGNAL(buttonClicked()), this, SLOT(selezionatoB()));
    connect( ui->buttonOk, SIGNAL(buttonClicked()), this, SLOT(esci()));

    ui->selezioneA->setFocus();

    //rotellaOld = 0;
    //tastoOld   = 0;
    animated   = false;
}

dialodSelezionaAB::~dialodSelezionaAB()
{
    delete ui;
}

//void dialodSelezionaAB::setRotella(Rotella *pRotella)
//{
//    if( pRotella != NULL )
//    {
//        rotella = pRotella;
//        connect( rotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
//        rotella->delegate = this;
//    }
//}

//void dialodSelezionaAB::setRotella(int rotella, int tasto)
//{

//    if( rotellaOld == 0 ) rotellaOld = rotella;

//    qDebug( "%d,%d", rotella, rotellaOld );

//    if( (rotella - rotellaOld ) > 10 ) {
//        this->focusNextChild();
//        rotellaOld = rotella;
//        emit beep(1);
//    }
//    else if( (rotella - rotellaOld ) < -10 )
//    {
//        this->focusPreviousChild();
//        rotellaOld = rotella;
//        emit beep(1);
//    }
//    if( tasto == 0 && tastoOld == 1 )
//    {
//        if(     this->focusWidget() == ui->selezioneA  ) selezionatoA();
//        else if(this->focusWidget() == ui->selezioneB  ) selezionatoB();
//        else if(this->focusWidget() == ui->buttonClose ) esci();

//        emit beep(1);
//    }
//    tastoOld = tasto;
//}

void dialodSelezionaAB::selezionatoA()
{
    ui->selezioneB->setChecked(false);
    ui->selezioneA->setChecked(true);
}

void dialodSelezionaAB::selezionatoB()
{
    ui->selezioneA->setChecked(false);
    ui->selezioneB->setChecked(true);
}

void dialodSelezionaAB::setTitle(QString title)
{
    ui->labelTitle->setText(title);
}

void dialodSelezionaAB::setVariabileA(bool *var, QString name)
{
    variabileA = var;
    ui->selezioneA->setTitle(name);
    ui->selezioneA->setChecked(*variabileA);
}

void dialodSelezionaAB::setVariabileB(bool *var, QString name)
{
    variabileB = var;
    ui->selezioneB->setTitle(name);
    ui->selezioneB->setChecked(*variabileB);
}

void dialodSelezionaAB::esci()
{
    if( variabileA != NULL ) *variabileA = ui->selezioneA->checked;
    if( variabileB != NULL ) *variabileB = ui->selezioneB->checked;
    this->close();
    emit onClose();
}

void dialodSelezionaAB::paintEvent(QPaintEvent *e)
{
    //QPainterPath path;
    QPainter painter(this);
    QRect area = this->rect();

    area.setX(2);
    area.setY(2);
    area.setWidth( area.width()-2 );
    area.setHeight( area.height()-2 );


    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(4);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);


    painter.setPen(pen);

    //path.addRoundedRect( area, 15, 15);
    //painter.drawPath( path );
    //painter.fillPath( path , Qt::blue );
    painter.drawRoundRect( area ,4,4 );
}

void dialodSelezionaAB::showEvent(QShowEvent *e)
{
//    if( animated )
//    {
//        QRect reca(0,0,400,300);
//        QRect recb(200,0,400,300);

//        //reca.setX(400);

//        //create animation for "geometry" property
//        QPropertyAnimation *animation = new QPropertyAnimation(this, "geometry");

//        //duration in ms
//        animation->setDuration(5000);

//        //starting geometry
//        QRect startRect(reca);

//        //final geometry
//        QRect endRect(recb);

//        animation->setStartValue(startRect);
//        animation->setEndValue(endRect);

//        //starts animation which will be deleted when finished
//        animation->start(QAbstractAnimation::DeleteWhenStopped);
//    }
//    QPropertyAnimation *animation = new QPropertyAnimation(this, "windowOpacity");
//    //duration in ms
//    animation->setDuration(5000);

//    animation->setStartValue(0);
//    animation->setEndValue(1);

//    //starts animation which will be deleted when finished
//    animation->start(QAbstractAnimation::DeleteWhenStopped);


}

void dialodSelezionaAB::rotellaMove(bool up)
{
    if( ui->selezioneA->focus ) ui->selezioneB->setFocus();
    else ui->selezioneA->setFocus();

    if(     this->focusWidget() == ui->selezioneA  ) selezionatoA();
    else if(this->focusWidget() == ui->selezioneB  ) selezionatoB();
}

void dialodSelezionaAB::rotellaClick()
{
    esci();
    return;

//    if(     this->focusWidget() == ui->selezioneA  ) selezionatoA();
//    else if(this->focusWidget() == ui->selezioneB  ) selezionatoB();
//    else if(this->focusWidget() == ui->buttonOk    ) esci();
}

