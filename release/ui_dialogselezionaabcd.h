/********************************************************************************
** Form generated from reading UI file 'dialogselezionaabcd.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGSELEZIONAABCD_H
#define UI_DIALOGSELEZIONAABCD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <ufgbutton.h>
#include "ufgcheckbox.h"

QT_BEGIN_NAMESPACE

class Ui_dialogSelezionaabcd
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *labelTitolo;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_23;
    ufgCheckBox *selezione_1;
    ufgCheckBox *selezione_4;
    QSpacerItem *horizontalSpacer_24;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_19;
    ufgCheckBox *selezione_2;
    ufgCheckBox *selezione_5;
    QSpacerItem *horizontalSpacer_20;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_17;
    ufgCheckBox *selezione_3;
    ufgCheckBox *selezione_6;
    QSpacerItem *horizontalSpacer_18;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_21;
    ufgButton *button_ok;
    QSpacerItem *horizontalSpacer_22;

    void setupUi(QDialog *dialogSelezionaabcd)
    {
        if (dialogSelezionaabcd->objectName().isEmpty())
            dialogSelezionaabcd->setObjectName(QStringLiteral("dialogSelezionaabcd"));
        dialogSelezionaabcd->setWindowModality(Qt::WindowModal);
        dialogSelezionaabcd->resize(700, 370);
        dialogSelezionaabcd->setMinimumSize(QSize(400, 0));
        dialogSelezionaabcd->setMaximumSize(QSize(700, 16777215));
        dialogSelezionaabcd->setWindowTitle(QStringLiteral("Dialog"));
        gridLayout = new QGridLayout(dialogSelezionaabcd);
        gridLayout->setSpacing(3);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(6, 6, 6, 6);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        labelTitolo = new QLabel(dialogSelezionaabcd);
        labelTitolo->setObjectName(QStringLiteral("labelTitolo"));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(20);
        labelTitolo->setFont(font);
        labelTitolo->setStyleSheet(QStringLiteral("color: rgb(0, 6, 255);"));
        labelTitolo->setText(QStringLiteral("TextLabel"));
        labelTitolo->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(labelTitolo);

        horizontalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 19, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalSpacer_23 = new QSpacerItem(6, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_23);

        selezione_1 = new ufgCheckBox(dialogSelezionaabcd);
        selezione_1->setObjectName(QStringLiteral("selezione_1"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(selezione_1->sizePolicy().hasHeightForWidth());
        selezione_1->setSizePolicy(sizePolicy);
        selezione_1->setMinimumSize(QSize(0, 50));
        selezione_1->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_6->addWidget(selezione_1);

        selezione_4 = new ufgCheckBox(dialogSelezionaabcd);
        selezione_4->setObjectName(QStringLiteral("selezione_4"));
        sizePolicy.setHeightForWidth(selezione_4->sizePolicy().hasHeightForWidth());
        selezione_4->setSizePolicy(sizePolicy);
        selezione_4->setMinimumSize(QSize(0, 50));
        selezione_4->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_6->addWidget(selezione_4);

        horizontalSpacer_24 = new QSpacerItem(6, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_24);


        gridLayout->addLayout(horizontalLayout_6, 2, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_19 = new QSpacerItem(6, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_19);

        selezione_2 = new ufgCheckBox(dialogSelezionaabcd);
        selezione_2->setObjectName(QStringLiteral("selezione_2"));
        sizePolicy.setHeightForWidth(selezione_2->sizePolicy().hasHeightForWidth());
        selezione_2->setSizePolicy(sizePolicy);
        selezione_2->setMinimumSize(QSize(0, 50));
        selezione_2->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_5->addWidget(selezione_2);

        selezione_5 = new ufgCheckBox(dialogSelezionaabcd);
        selezione_5->setObjectName(QStringLiteral("selezione_5"));
        sizePolicy.setHeightForWidth(selezione_5->sizePolicy().hasHeightForWidth());
        selezione_5->setSizePolicy(sizePolicy);
        selezione_5->setMinimumSize(QSize(0, 50));
        selezione_5->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_5->addWidget(selezione_5);

        horizontalSpacer_20 = new QSpacerItem(6, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_20);


        gridLayout->addLayout(horizontalLayout_5, 3, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_17 = new QSpacerItem(6, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_17);

        selezione_3 = new ufgCheckBox(dialogSelezionaabcd);
        selezione_3->setObjectName(QStringLiteral("selezione_3"));
        sizePolicy.setHeightForWidth(selezione_3->sizePolicy().hasHeightForWidth());
        selezione_3->setSizePolicy(sizePolicy);
        selezione_3->setMinimumSize(QSize(0, 50));
        selezione_3->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_4->addWidget(selezione_3);

        selezione_6 = new ufgCheckBox(dialogSelezionaabcd);
        selezione_6->setObjectName(QStringLiteral("selezione_6"));
        sizePolicy.setHeightForWidth(selezione_6->sizePolicy().hasHeightForWidth());
        selezione_6->setSizePolicy(sizePolicy);
        selezione_6->setMinimumSize(QSize(0, 50));
        selezione_6->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_4->addWidget(selezione_6);

        horizontalSpacer_18 = new QSpacerItem(6, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_18);


        gridLayout->addLayout(horizontalLayout_4, 4, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 5, 0, 1, 1);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_21);

        button_ok = new ufgButton(dialogSelezionaabcd);
        button_ok->setObjectName(QStringLiteral("button_ok"));
        button_ok->setMinimumSize(QSize(80, 80));

        horizontalLayout_11->addWidget(button_ok);

        horizontalSpacer_22 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_22);


        gridLayout->addLayout(horizontalLayout_11, 6, 0, 1, 1);


        retranslateUi(dialogSelezionaabcd);

        QMetaObject::connectSlotsByName(dialogSelezionaabcd);
    } // setupUi

    void retranslateUi(QDialog *dialogSelezionaabcd)
    {
        Q_UNUSED(dialogSelezionaabcd);
    } // retranslateUi

};

namespace Ui {
    class dialogSelezionaabcd: public Ui_dialogSelezionaabcd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGSELEZIONAABCD_H
