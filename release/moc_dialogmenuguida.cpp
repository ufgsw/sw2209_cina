/****************************************************************************
** Meta object code from reading C++ file 'dialogmenuguida.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialogmenuguida.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialogmenuguida.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_dialogMenuGuida_t {
    QByteArrayData data[19];
    char stringdata0[205];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_dialogMenuGuida_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_dialogMenuGuida_t qt_meta_stringdata_dialogMenuGuida = {
    {
QT_MOC_LITERAL(0, 0, 15), // "dialogMenuGuida"
QT_MOC_LITERAL(1, 16, 7), // "onClose"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 4), // "beep"
QT_MOC_LITERAL(4, 30, 5), // "tempo"
QT_MOC_LITERAL(5, 36, 10), // "showElenco"
QT_MOC_LITERAL(6, 47, 6), // "elenco"
QT_MOC_LITERAL(7, 54, 15), // "caricaProgramma"
QT_MOC_LITERAL(8, 70, 10), // "sProgramma"
QT_MOC_LITERAL(9, 81, 9), // "programma"
QT_MOC_LITERAL(10, 91, 15), // "riprendoRotella"
QT_MOC_LITERAL(11, 107, 11), // "rotellaMove"
QT_MOC_LITERAL(12, 119, 2), // "up"
QT_MOC_LITERAL(13, 122, 12), // "rotellaClick"
QT_MOC_LITERAL(14, 135, 12), // "showGenerici"
QT_MOC_LITERAL(15, 148, 14), // "showOrtopedici"
QT_MOC_LITERAL(16, 163, 15), // "showNeurologici"
QT_MOC_LITERAL(17, 179, 20), // "selezionatoProgramma"
QT_MOC_LITERAL(18, 200, 4) // "esci"

    },
    "dialogMenuGuida\0onClose\0\0beep\0tempo\0"
    "showElenco\0elenco\0caricaProgramma\0"
    "sProgramma\0programma\0riprendoRotella\0"
    "rotellaMove\0up\0rotellaClick\0showGenerici\0"
    "showOrtopedici\0showNeurologici\0"
    "selezionatoProgramma\0esci"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_dialogMenuGuida[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x06 /* Public */,
       3,    1,   75,    2, 0x06 /* Public */,
       5,    1,   78,    2, 0x06 /* Public */,
       7,    1,   81,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,   84,    2, 0x0a /* Public */,
      11,    1,   85,    2, 0x0a /* Public */,
      13,    0,   88,    2, 0x0a /* Public */,
      14,    0,   89,    2, 0x0a /* Public */,
      15,    0,   90,    2, 0x0a /* Public */,
      16,    0,   91,    2, 0x0a /* Public */,
      17,    1,   92,    2, 0x0a /* Public */,
      18,    0,   95,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, 0x80000000 | 8,    9,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,

       0        // eod
};

void dialogMenuGuida::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        dialogMenuGuida *_t = static_cast<dialogMenuGuida *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onClose(); break;
        case 1: _t->beep((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->showElenco((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->caricaProgramma((*reinterpret_cast< sProgramma(*)>(_a[1]))); break;
        case 4: _t->riprendoRotella(); break;
        case 5: _t->rotellaMove((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->rotellaClick(); break;
        case 7: _t->showGenerici(); break;
        case 8: _t->showOrtopedici(); break;
        case 9: _t->showNeurologici(); break;
        case 10: _t->selezionatoProgramma((*reinterpret_cast< sProgramma(*)>(_a[1]))); break;
        case 11: _t->esci(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (dialogMenuGuida::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialogMenuGuida::onClose)) {
                *result = 0;
            }
        }
        {
            typedef void (dialogMenuGuida::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialogMenuGuida::beep)) {
                *result = 1;
            }
        }
        {
            typedef void (dialogMenuGuida::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialogMenuGuida::showElenco)) {
                *result = 2;
            }
        }
        {
            typedef void (dialogMenuGuida::*_t)(sProgramma );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialogMenuGuida::caricaProgramma)) {
                *result = 3;
            }
        }
    }
}

const QMetaObject dialogMenuGuida::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_dialogMenuGuida.data,
      qt_meta_data_dialogMenuGuida,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *dialogMenuGuida::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *dialogMenuGuida::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_dialogMenuGuida.stringdata0))
        return static_cast<void*>(const_cast< dialogMenuGuida*>(this));
    return QDialog::qt_metacast(_clname);
}

int dialogMenuGuida::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void dialogMenuGuida::onClose()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void dialogMenuGuida::beep(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void dialogMenuGuida::showElenco(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void dialogMenuGuida::caricaProgramma(sProgramma _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
