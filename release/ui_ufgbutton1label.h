/********************************************************************************
** Form generated from reading UI file 'ufgbutton1label.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UFGBUTTON1LABEL_H
#define UI_UFGBUTTON1LABEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ufgButton1label
{
public:
    QGridLayout *gridLayout;
    QLabel *labelTesto;

    void setupUi(QWidget *ufgButton1label)
    {
        if (ufgButton1label->objectName().isEmpty())
            ufgButton1label->setObjectName(QStringLiteral("ufgButton1label"));
        ufgButton1label->resize(203, 62);
        ufgButton1label->setWindowTitle(QStringLiteral("Form"));
        ufgButton1label->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        gridLayout = new QGridLayout(ufgButton1label);
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(6, 1, 6, 1);
        labelTesto = new QLabel(ufgButton1label);
        labelTesto->setObjectName(QStringLiteral("labelTesto"));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(16);
        labelTesto->setFont(font);
        labelTesto->setStyleSheet(QLatin1String("color: rgb(255, 255, 255);\n"
"background-color: rgba(255, 255, 255, 0);"));
        labelTesto->setText(QStringLiteral("-------------------"));
        labelTesto->setTextFormat(Qt::PlainText);
        labelTesto->setAlignment(Qt::AlignCenter);
        labelTesto->setWordWrap(true);
        labelTesto->setTextInteractionFlags(Qt::NoTextInteraction);

        gridLayout->addWidget(labelTesto, 0, 0, 1, 1);


        retranslateUi(ufgButton1label);

        QMetaObject::connectSlotsByName(ufgButton1label);
    } // setupUi

    void retranslateUi(QWidget *ufgButton1label)
    {
        Q_UNUSED(ufgButton1label);
    } // retranslateUi

};

namespace Ui {
    class ufgButton1label: public Ui_ufgButton1label {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UFGBUTTON1LABEL_H
