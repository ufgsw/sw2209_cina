/********************************************************************************
** Form generated from reading UI file 'ufgbutton2label.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UFGBUTTON2LABEL_H
#define UI_UFGBUTTON2LABEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ufgButton2Label
{
public:
    QGridLayout *gridLayout;
    QLabel *labelTitolo;
    QLabel *labelTesto;

    void setupUi(QWidget *ufgButton2Label)
    {
        if (ufgButton2Label->objectName().isEmpty())
            ufgButton2Label->setObjectName(QStringLiteral("ufgButton2Label"));
        ufgButton2Label->resize(189, 71);
        QFont font;
        font.setFamily(QStringLiteral("Vemana2000"));
        ufgButton2Label->setFont(font);
        ufgButton2Label->setFocusPolicy(Qt::StrongFocus);
        ufgButton2Label->setWindowTitle(QStringLiteral("Form"));
        ufgButton2Label->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        gridLayout = new QGridLayout(ufgButton2Label);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        labelTitolo = new QLabel(ufgButton2Label);
        labelTitolo->setObjectName(QStringLiteral("labelTitolo"));
        labelTitolo->setEnabled(true);
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        font1.setPointSize(11);
        font1.setBold(true);
        font1.setWeight(75);
        labelTitolo->setFont(font1);
        labelTitolo->setLayoutDirection(Qt::LeftToRight);
        labelTitolo->setStyleSheet(QStringLiteral("color: rgb(255, 255, 255);"));
        labelTitolo->setText(QStringLiteral("TIPO TRATTAMENTO"));
        labelTitolo->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(labelTitolo, 0, 0, 1, 1);

        labelTesto = new QLabel(ufgButton2Label);
        labelTesto->setObjectName(QStringLiteral("labelTesto"));
        QFont font2;
        font2.setFamily(QStringLiteral("DejaVu Sans"));
        font2.setPointSize(18);
        labelTesto->setFont(font2);
        labelTesto->setStyleSheet(QStringLiteral("color: rgb(255, 255, 255);"));
        labelTesto->setText(QStringLiteral("CAPACITIVO"));
        labelTesto->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(labelTesto, 1, 0, 1, 1);


        retranslateUi(ufgButton2Label);

        QMetaObject::connectSlotsByName(ufgButton2Label);
    } // setupUi

    void retranslateUi(QWidget *ufgButton2Label)
    {
        Q_UNUSED(ufgButton2Label);
    } // retranslateUi

};

namespace Ui {
    class ufgButton2Label: public Ui_ufgButton2Label {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UFGBUTTON2LABEL_H
