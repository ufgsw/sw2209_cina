/********************************************************************************
** Form generated from reading UI file 'dialogselezionaabc.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGSELEZIONAABC_H
#define UI_DIALOGSELEZIONAABC_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <ufgbutton.h>
#include "ufgcheckbox.h"

QT_BEGIN_NAMESPACE

class Ui_dialogSelezionaabc
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QLabel *labelTitle;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_9;
    ufgCheckBox *selezionaA;
    QSpacerItem *horizontalSpacer_10;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_11;
    ufgCheckBox *selezionaB;
    QSpacerItem *horizontalSpacer_12;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_13;
    ufgCheckBox *selezionaC;
    QSpacerItem *horizontalSpacer_14;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_15;
    ufgButton *buttonOk;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QDialog *dialogSelezionaabc)
    {
        if (dialogSelezionaabc->objectName().isEmpty())
            dialogSelezionaabc->setObjectName(QStringLiteral("dialogSelezionaabc"));
        dialogSelezionaabc->setWindowModality(Qt::ApplicationModal);
        dialogSelezionaabc->resize(340, 370);
        dialogSelezionaabc->setWindowTitle(QStringLiteral("Dialog"));
        gridLayout = new QGridLayout(dialogSelezionaabc);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, 0, -1, -1);
        horizontalSpacer_7 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);

        labelTitle = new QLabel(dialogSelezionaabc);
        labelTitle->setObjectName(QStringLiteral("labelTitle"));
        labelTitle->setMinimumSize(QSize(0, 50));
        labelTitle->setMaximumSize(QSize(800, 85));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(16);
        labelTitle->setFont(font);
        labelTitle->setStyleSheet(QStringLiteral("color: rgb(0, 0, 255);"));
        labelTitle->setText(QStringLiteral("TextLabel"));
        labelTitle->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(labelTitle);

        horizontalSpacer_8 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);


        gridLayout->addLayout(horizontalLayout_4, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 41, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_9 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_9);

        selezionaA = new ufgCheckBox(dialogSelezionaabc);
        selezionaA->setObjectName(QStringLiteral("selezionaA"));
        selezionaA->setMinimumSize(QSize(0, 55));
        selezionaA->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_5->addWidget(selezionaA);

        horizontalSpacer_10 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_10);


        gridLayout->addLayout(horizontalLayout_5, 2, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalSpacer_11 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_11);

        selezionaB = new ufgCheckBox(dialogSelezionaabc);
        selezionaB->setObjectName(QStringLiteral("selezionaB"));
        selezionaB->setMinimumSize(QSize(0, 55));
        selezionaB->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_6->addWidget(selezionaB);

        horizontalSpacer_12 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_12);


        gridLayout->addLayout(horizontalLayout_6, 3, 0, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalSpacer_13 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_13);

        selezionaC = new ufgCheckBox(dialogSelezionaabc);
        selezionaC->setObjectName(QStringLiteral("selezionaC"));
        selezionaC->setMinimumSize(QSize(0, 55));
        selezionaC->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_7->addWidget(selezionaC);

        horizontalSpacer_14 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_14);


        gridLayout->addLayout(horizontalLayout_7, 4, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 5, 0, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_15);

        buttonOk = new ufgButton(dialogSelezionaabc);
        buttonOk->setObjectName(QStringLiteral("buttonOk"));
        buttonOk->setMinimumSize(QSize(80, 80));
        buttonOk->setMaximumSize(QSize(80, 80));

        horizontalLayout_8->addWidget(buttonOk);

        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_16);


        gridLayout->addLayout(horizontalLayout_8, 6, 0, 1, 1);


        retranslateUi(dialogSelezionaabc);

        QMetaObject::connectSlotsByName(dialogSelezionaabc);
    } // setupUi

    void retranslateUi(QDialog *dialogSelezionaabc)
    {
        Q_UNUSED(dialogSelezionaabc);
    } // retranslateUi

};

namespace Ui {
    class dialogSelezionaabc: public Ui_dialogSelezionaabc {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGSELEZIONAABC_H
