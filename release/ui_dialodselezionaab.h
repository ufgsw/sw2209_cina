/********************************************************************************
** Form generated from reading UI file 'dialodselezionaab.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALODSELEZIONAAB_H
#define UI_DIALODSELEZIONAAB_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <ufgbutton.h>
#include "ufgcheckbox.h"

QT_BEGIN_NAMESPACE

class Ui_dialodSelezionaAB
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QLabel *labelTitle;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_5;
    ufgCheckBox *selezioneA;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    ufgCheckBox *selezioneB;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    ufgButton *buttonOk;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *dialodSelezionaAB)
    {
        if (dialodSelezionaAB->objectName().isEmpty())
            dialodSelezionaAB->setObjectName(QStringLiteral("dialodSelezionaAB"));
        dialodSelezionaAB->setWindowModality(Qt::WindowModal);
        dialodSelezionaAB->resize(340, 370);
        dialodSelezionaAB->setWindowTitle(QStringLiteral(""));
        dialodSelezionaAB->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        gridLayout = new QGridLayout(dialodSelezionaAB);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_7 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);

        labelTitle = new QLabel(dialodSelezionaAB);
        labelTitle->setObjectName(QStringLiteral("labelTitle"));
        labelTitle->setMinimumSize(QSize(0, 60));
        labelTitle->setMaximumSize(QSize(800, 85));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(18);
        labelTitle->setFont(font);
        labelTitle->setStyleSheet(QLatin1String("color: rgb(0, 0, 255);\n"
"background-color: rgba(255, 255, 255, 0);"));
        labelTitle->setText(QStringLiteral("TextLabel"));
        labelTitle->setAlignment(Qt::AlignCenter);
        labelTitle->setTextInteractionFlags(Qt::NoTextInteraction);

        horizontalLayout_4->addWidget(labelTitle);

        horizontalSpacer_8 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);


        gridLayout->addLayout(horizontalLayout_4, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 36, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_5 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        selezioneA = new ufgCheckBox(dialodSelezionaAB);
        selezioneA->setObjectName(QStringLiteral("selezioneA"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(selezioneA->sizePolicy().hasHeightForWidth());
        selezioneA->setSizePolicy(sizePolicy);
        selezioneA->setMinimumSize(QSize(100, 50));
        selezioneA->setMaximumSize(QSize(16777215, 80));
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        selezioneA->setFont(font1);
        selezioneA->setFocusPolicy(Qt::StrongFocus);
        selezioneA->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_3->addWidget(selezioneA);

        horizontalSpacer_6 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);


        gridLayout->addLayout(horizontalLayout_3, 2, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        selezioneB = new ufgCheckBox(dialodSelezionaAB);
        selezioneB->setObjectName(QStringLiteral("selezioneB"));
        selezioneB->setMinimumSize(QSize(100, 50));
        selezioneB->setMaximumSize(QSize(16777215, 80));
        selezioneB->setFont(font1);
        selezioneB->setFocusPolicy(Qt::StrongFocus);
        selezioneB->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));

        horizontalLayout_2->addWidget(selezioneB);

        horizontalSpacer_4 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        gridLayout->addLayout(horizontalLayout_2, 3, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 35, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 4, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        buttonOk = new ufgButton(dialodSelezionaAB);
        buttonOk->setObjectName(QStringLiteral("buttonOk"));
        buttonOk->setMinimumSize(QSize(80, 80));
        buttonOk->setMaximumSize(QSize(80, 80));

        horizontalLayout->addWidget(buttonOk);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout, 5, 0, 1, 1);


        retranslateUi(dialodSelezionaAB);

        QMetaObject::connectSlotsByName(dialodSelezionaAB);
    } // setupUi

    void retranslateUi(QDialog *dialodSelezionaAB)
    {
        Q_UNUSED(dialodSelezionaAB);
    } // retranslateUi

};

namespace Ui {
    class dialodSelezionaAB: public Ui_dialodSelezionaAB {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALODSELEZIONAAB_H
