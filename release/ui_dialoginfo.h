/********************************************************************************
** Form generated from reading UI file 'dialoginfo.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGINFO_H
#define UI_DIALOGINFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <ufgbutton.h>

QT_BEGIN_NAMESPACE

class Ui_dialogInfo
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_7;
    QLabel *labelTitle;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_5;
    QLabel *textInfo;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    ufgButton *buttonOk;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *dialogInfo)
    {
        if (dialogInfo->objectName().isEmpty())
            dialogInfo->setObjectName(QStringLiteral("dialogInfo"));
        dialogInfo->setWindowModality(Qt::WindowModal);
        dialogInfo->resize(389, 280);
        dialogInfo->setWindowTitle(QStringLiteral("Dialog"));
        dialogInfo->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        gridLayout = new QGridLayout(dialogInfo);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(6, 6, 6, 6);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_7 = new QSpacerItem(20, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_7);

        labelTitle = new QLabel(dialogInfo);
        labelTitle->setObjectName(QStringLiteral("labelTitle"));
        labelTitle->setMinimumSize(QSize(0, 80));
        labelTitle->setMaximumSize(QSize(16777215, 120));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(16);
        labelTitle->setFont(font);
        labelTitle->setText(QStringLiteral("Info"));
        labelTitle->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(labelTitle);

        horizontalSpacer_8 = new QSpacerItem(20, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_8);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_5 = new QSpacerItem(15, 58, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        textInfo = new QLabel(dialogInfo);
        textInfo->setObjectName(QStringLiteral("textInfo"));
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        font1.setPointSize(15);
        textInfo->setFont(font1);
        textInfo->setStyleSheet(QStringLiteral("color: rgb(0, 0, 255);"));
        textInfo->setLineWidth(-1);
        textInfo->setMidLineWidth(1);
        textInfo->setText(QStringLiteral("ciao"));
        textInfo->setAlignment(Qt::AlignCenter);
        textInfo->setWordWrap(true);

        horizontalLayout_2->addWidget(textInfo);

        horizontalSpacer_6 = new QSpacerItem(15, 48, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(139, 70, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        buttonOk = new ufgButton(dialogInfo);
        buttonOk->setObjectName(QStringLiteral("buttonOk"));
        buttonOk->setMinimumSize(QSize(85, 85));
        buttonOk->setMaximumSize(QSize(85, 85));

        horizontalLayout_3->addWidget(buttonOk);

        horizontalSpacer_2 = new QSpacerItem(139, 70, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout_3, 2, 0, 1, 1);


        retranslateUi(dialogInfo);

        QMetaObject::connectSlotsByName(dialogInfo);
    } // setupUi

    void retranslateUi(QDialog *dialogInfo)
    {
        Q_UNUSED(dialogInfo);
    } // retranslateUi

};

namespace Ui {
    class dialogInfo: public Ui_dialogInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGINFO_H
