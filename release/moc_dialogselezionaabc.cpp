/****************************************************************************
** Meta object code from reading C++ file 'dialogselezionaabc.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialogselezionaabc.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialogselezionaabc.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_dialogSelezionaabc_t {
    QByteArrayData data[10];
    char stringdata0[100];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_dialogSelezionaabc_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_dialogSelezionaabc_t qt_meta_stringdata_dialogSelezionaabc = {
    {
QT_MOC_LITERAL(0, 0, 18), // "dialogSelezionaabc"
QT_MOC_LITERAL(1, 19, 7), // "onClose"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 12), // "selezionatoA"
QT_MOC_LITERAL(4, 41, 12), // "selezionatoB"
QT_MOC_LITERAL(5, 54, 12), // "selezionatoC"
QT_MOC_LITERAL(6, 67, 4), // "esci"
QT_MOC_LITERAL(7, 72, 11), // "rotellaMove"
QT_MOC_LITERAL(8, 84, 2), // "up"
QT_MOC_LITERAL(9, 87, 12) // "rotellaClick"

    },
    "dialogSelezionaabc\0onClose\0\0selezionatoA\0"
    "selezionatoB\0selezionatoC\0esci\0"
    "rotellaMove\0up\0rotellaClick"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_dialogSelezionaabc[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   50,    2, 0x0a /* Public */,
       4,    0,   51,    2, 0x0a /* Public */,
       5,    0,   52,    2, 0x0a /* Public */,
       6,    0,   53,    2, 0x0a /* Public */,
       7,    1,   54,    2, 0x0a /* Public */,
       9,    0,   57,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void,

       0        // eod
};

void dialogSelezionaabc::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        dialogSelezionaabc *_t = static_cast<dialogSelezionaabc *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onClose(); break;
        case 1: _t->selezionatoA(); break;
        case 2: _t->selezionatoB(); break;
        case 3: _t->selezionatoC(); break;
        case 4: _t->esci(); break;
        case 5: _t->rotellaMove((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->rotellaClick(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (dialogSelezionaabc::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialogSelezionaabc::onClose)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject dialogSelezionaabc::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_dialogSelezionaabc.data,
      qt_meta_data_dialogSelezionaabc,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *dialogSelezionaabc::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *dialogSelezionaabc::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_dialogSelezionaabc.stringdata0))
        return static_cast<void*>(const_cast< dialogSelezionaabc*>(this));
    return QDialog::qt_metacast(_clname);
}

int dialogSelezionaabc::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void dialogSelezionaabc::onClose()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
