/********************************************************************************
** Form generated from reading UI file 'dialogsetvar.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGSETVAR_H
#define UI_DIALOGSETVAR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <ufgbutton.h>
#include "ufgprogressbar.h"

QT_BEGIN_NAMESPACE

class Ui_dialogSetVar
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_4;
    QLabel *labelTitolo;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_7;
    QLabel *labelValore;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    ufgProgressBar *progressBar;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_10;
    ufgButton *buttonMeno;
    QSpacerItem *horizontalSpacer;
    ufgButton *buttonSalva;
    QSpacerItem *horizontalSpacer_9;
    ufgButton *buttonPiu;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *dialogSetVar)
    {
        if (dialogSetVar->objectName().isEmpty())
            dialogSetVar->setObjectName(QStringLiteral("dialogSetVar"));
        dialogSetVar->setWindowModality(Qt::WindowModal);
        dialogSetVar->resize(446, 280);
        dialogSetVar->setWindowTitle(QStringLiteral("Dialog"));
        dialogSetVar->setStyleSheet(QStringLiteral(""));
        dialogSetVar->setModal(true);
        gridLayout = new QGridLayout(dialogSetVar);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        labelTitolo = new QLabel(dialogSetVar);
        labelTitolo->setObjectName(QStringLiteral("labelTitolo"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(labelTitolo->sizePolicy().hasHeightForWidth());
        labelTitolo->setSizePolicy(sizePolicy);
        labelTitolo->setMinimumSize(QSize(200, 0));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(24);
        font.setBold(false);
        font.setWeight(50);
        font.setStyleStrategy(QFont::PreferDefault);
        labelTitolo->setFont(font);
        labelTitolo->setStyleSheet(QStringLiteral("color: rgb(0, 0, 255);"));
        labelTitolo->setText(QStringLiteral("title"));
        labelTitolo->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(labelTitolo);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);


        gridLayout->addLayout(horizontalLayout_4, 0, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_7);

        labelValore = new QLabel(dialogSetVar);
        labelValore->setObjectName(QStringLiteral("labelValore"));
        labelValore->setMinimumSize(QSize(200, 0));
        labelValore->setMaximumSize(QSize(16777215, 50));
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        font1.setPointSize(22);
        font1.setBold(false);
        font1.setWeight(50);
        labelValore->setFont(font1);
        labelValore->setText(QStringLiteral("TextLabel"));
        labelValore->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(labelValore);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_8);


        gridLayout->addLayout(horizontalLayout_3, 1, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        progressBar = new ufgProgressBar(dialogSetVar);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setMinimumSize(QSize(200, 85));
        progressBar->setMaximumSize(QSize(800, 85));

        horizontalLayout_2->addWidget(progressBar);

        horizontalSpacer_6 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);


        gridLayout->addLayout(horizontalLayout_2, 2, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_10 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_10);

        buttonMeno = new ufgButton(dialogSetVar);
        buttonMeno->setObjectName(QStringLiteral("buttonMeno"));
        buttonMeno->setMinimumSize(QSize(85, 85));
        buttonMeno->setMaximumSize(QSize(85, 85));

        horizontalLayout->addWidget(buttonMeno);

        horizontalSpacer = new QSpacerItem(50, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        buttonSalva = new ufgButton(dialogSetVar);
        buttonSalva->setObjectName(QStringLiteral("buttonSalva"));
        buttonSalva->setMinimumSize(QSize(85, 85));
        buttonSalva->setMaximumSize(QSize(85, 85));

        horizontalLayout->addWidget(buttonSalva);

        horizontalSpacer_9 = new QSpacerItem(50, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_9);

        buttonPiu = new ufgButton(dialogSetVar);
        buttonPiu->setObjectName(QStringLiteral("buttonPiu"));
        buttonPiu->setMinimumSize(QSize(85, 85));
        buttonPiu->setMaximumSize(QSize(85, 85));

        horizontalLayout->addWidget(buttonPiu);

        horizontalSpacer_2 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout, 3, 0, 1, 1);


        retranslateUi(dialogSetVar);

        QMetaObject::connectSlotsByName(dialogSetVar);
    } // setupUi

    void retranslateUi(QDialog *dialogSetVar)
    {
        Q_UNUSED(dialogSetVar);
    } // retranslateUi

};

namespace Ui {
    class dialogSetVar: public Ui_dialogSetVar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGSETVAR_H
