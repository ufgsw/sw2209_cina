/********************************************************************************
** Form generated from reading UI file 'ufgbuttontastiera.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UFGBUTTONTASTIERA_H
#define UI_UFGBUTTONTASTIERA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ufgButtonTastiera
{
public:
    QGridLayout *gridLayout;
    QLabel *label;

    void setupUi(QWidget *ufgButtonTastiera)
    {
        if (ufgButtonTastiera->objectName().isEmpty())
            ufgButtonTastiera->setObjectName(QStringLiteral("ufgButtonTastiera"));
        ufgButtonTastiera->resize(50, 50);
        ufgButtonTastiera->setWindowTitle(QStringLiteral(""));
        gridLayout = new QGridLayout(ufgButtonTastiera);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(ufgButtonTastiera);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setLayoutDirection(Qt::LeftToRight);
        label->setStyleSheet(QStringLiteral("color: rgb(4, 4, 4);"));
        label->setText(QStringLiteral("A"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 0, 1, 1);


        retranslateUi(ufgButtonTastiera);

        QMetaObject::connectSlotsByName(ufgButtonTastiera);
    } // setupUi

    void retranslateUi(QWidget *ufgButtonTastiera)
    {
        Q_UNUSED(ufgButtonTastiera);
    } // retranslateUi

};

namespace Ui {
    class ufgButtonTastiera: public Ui_ufgButtonTastiera {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UFGBUTTONTASTIERA_H
