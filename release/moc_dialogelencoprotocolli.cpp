/****************************************************************************
** Meta object code from reading C++ file 'dialogelencoprotocolli.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialogelencoprotocolli.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialogelencoprotocolli.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_dialogElencoProtocolli_t {
    QByteArrayData data[28];
    char stringdata0[368];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_dialogElencoProtocolli_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_dialogElencoProtocolli_t qt_meta_stringdata_dialogElencoProtocolli = {
    {
QT_MOC_LITERAL(0, 0, 22), // "dialogElencoProtocolli"
QT_MOC_LITERAL(1, 23, 7), // "onclose"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 15), // "caricaProgramma"
QT_MOC_LITERAL(4, 48, 10), // "sProgramma"
QT_MOC_LITERAL(5, 59, 9), // "programma"
QT_MOC_LITERAL(6, 69, 15), // "riprendoRotella"
QT_MOC_LITERAL(7, 85, 11), // "rotellaMove"
QT_MOC_LITERAL(8, 97, 2), // "up"
QT_MOC_LITERAL(9, 100, 12), // "rotellaClick"
QT_MOC_LITERAL(10, 113, 8), // "showNext"
QT_MOC_LITERAL(11, 122, 14), // "showProgrammi1"
QT_MOC_LITERAL(12, 137, 14), // "showProgrammi2"
QT_MOC_LITERAL(13, 152, 14), // "showProgrammi3"
QT_MOC_LITERAL(14, 167, 14), // "showProgrammi4"
QT_MOC_LITERAL(15, 182, 14), // "showProgrammi5"
QT_MOC_LITERAL(16, 197, 14), // "showProgrammi6"
QT_MOC_LITERAL(17, 212, 14), // "showProgrammi7"
QT_MOC_LITERAL(18, 227, 14), // "showProgrammi8"
QT_MOC_LITERAL(19, 242, 14), // "showProgrammi9"
QT_MOC_LITERAL(20, 257, 15), // "showProgrammi10"
QT_MOC_LITERAL(21, 273, 15), // "showProgrammi11"
QT_MOC_LITERAL(22, 289, 15), // "showProgrammi12"
QT_MOC_LITERAL(23, 305, 15), // "showProgrammi13"
QT_MOC_LITERAL(24, 321, 15), // "showProgrammi14"
QT_MOC_LITERAL(25, 337, 20), // "selezionatoProgramma"
QT_MOC_LITERAL(26, 358, 4), // "prog"
QT_MOC_LITERAL(27, 363, 4) // "esci"

    },
    "dialogElencoProtocolli\0onclose\0\0"
    "caricaProgramma\0sProgramma\0programma\0"
    "riprendoRotella\0rotellaMove\0up\0"
    "rotellaClick\0showNext\0showProgrammi1\0"
    "showProgrammi2\0showProgrammi3\0"
    "showProgrammi4\0showProgrammi5\0"
    "showProgrammi6\0showProgrammi7\0"
    "showProgrammi8\0showProgrammi9\0"
    "showProgrammi10\0showProgrammi11\0"
    "showProgrammi12\0showProgrammi13\0"
    "showProgrammi14\0selezionatoProgramma\0"
    "prog\0esci"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_dialogElencoProtocolli[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x06 /* Public */,
       3,    1,  125,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  128,    2, 0x0a /* Public */,
       7,    1,  129,    2, 0x0a /* Public */,
       9,    0,  132,    2, 0x0a /* Public */,
      10,    0,  133,    2, 0x0a /* Public */,
      11,    0,  134,    2, 0x0a /* Public */,
      12,    0,  135,    2, 0x0a /* Public */,
      13,    0,  136,    2, 0x0a /* Public */,
      14,    0,  137,    2, 0x0a /* Public */,
      15,    0,  138,    2, 0x0a /* Public */,
      16,    0,  139,    2, 0x0a /* Public */,
      17,    0,  140,    2, 0x0a /* Public */,
      18,    0,  141,    2, 0x0a /* Public */,
      19,    0,  142,    2, 0x0a /* Public */,
      20,    0,  143,    2, 0x0a /* Public */,
      21,    0,  144,    2, 0x0a /* Public */,
      22,    0,  145,    2, 0x0a /* Public */,
      23,    0,  146,    2, 0x0a /* Public */,
      24,    0,  147,    2, 0x0a /* Public */,
      25,    1,  148,    2, 0x0a /* Public */,
      27,    0,  151,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   26,
    QMetaType::Void,

       0        // eod
};

void dialogElencoProtocolli::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        dialogElencoProtocolli *_t = static_cast<dialogElencoProtocolli *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onclose(); break;
        case 1: _t->caricaProgramma((*reinterpret_cast< sProgramma(*)>(_a[1]))); break;
        case 2: _t->riprendoRotella(); break;
        case 3: _t->rotellaMove((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->rotellaClick(); break;
        case 5: _t->showNext(); break;
        case 6: _t->showProgrammi1(); break;
        case 7: _t->showProgrammi2(); break;
        case 8: _t->showProgrammi3(); break;
        case 9: _t->showProgrammi4(); break;
        case 10: _t->showProgrammi5(); break;
        case 11: _t->showProgrammi6(); break;
        case 12: _t->showProgrammi7(); break;
        case 13: _t->showProgrammi8(); break;
        case 14: _t->showProgrammi9(); break;
        case 15: _t->showProgrammi10(); break;
        case 16: _t->showProgrammi11(); break;
        case 17: _t->showProgrammi12(); break;
        case 18: _t->showProgrammi13(); break;
        case 19: _t->showProgrammi14(); break;
        case 20: _t->selezionatoProgramma((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->esci(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (dialogElencoProtocolli::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialogElencoProtocolli::onclose)) {
                *result = 0;
            }
        }
        {
            typedef void (dialogElencoProtocolli::*_t)(sProgramma );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&dialogElencoProtocolli::caricaProgramma)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject dialogElencoProtocolli::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_dialogElencoProtocolli.data,
      qt_meta_data_dialogElencoProtocolli,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *dialogElencoProtocolli::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *dialogElencoProtocolli::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_dialogElencoProtocolli.stringdata0))
        return static_cast<void*>(const_cast< dialogElencoProtocolli*>(this));
    return QDialog::qt_metacast(_clname);
}

int dialogElencoProtocolli::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void dialogElencoProtocolli::onclose()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void dialogElencoProtocolli::caricaProgramma(sProgramma _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
