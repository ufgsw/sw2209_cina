/********************************************************************************
** Form generated from reading UI file 'dialogsetup.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGSETUP_H
#define UI_DIALOGSETUP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <ufgbutton.h>
#include "ufgcheckbox.h"

QT_BEGIN_NAMESPACE

class Ui_dialogSetup
{
public:
    QGroupBox *boxTipoMacchina;
    ufgCheckBox *checkMacchina1;
    ufgCheckBox *checkMacchina2;
    ufgCheckBox *checkMacchina3;
    ufgButton *buttonExit;
    ufgButton *buttonPassword;

    void setupUi(QDialog *dialogSetup)
    {
        if (dialogSetup->objectName().isEmpty())
            dialogSetup->setObjectName(QStringLiteral("dialogSetup"));
        dialogSetup->resize(800, 480);
        dialogSetup->setMinimumSize(QSize(800, 480));
        dialogSetup->setMaximumSize(QSize(800, 480));
        dialogSetup->setWindowTitle(QStringLiteral("Dialog"));
        dialogSetup->setSizeGripEnabled(false);
        boxTipoMacchina = new QGroupBox(dialogSetup);
        boxTipoMacchina->setObjectName(QStringLiteral("boxTipoMacchina"));
        boxTipoMacchina->setGeometry(QRect(10, 320, 781, 131));
        boxTipoMacchina->setAutoFillBackground(false);
        boxTipoMacchina->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 200);"));
        boxTipoMacchina->setFlat(false);
        boxTipoMacchina->setCheckable(false);
        checkMacchina1 = new ufgCheckBox(boxTipoMacchina);
        checkMacchina1->setObjectName(QStringLiteral("checkMacchina1"));
        checkMacchina1->setGeometry(QRect(20, 50, 220, 70));
        checkMacchina1->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        checkMacchina2 = new ufgCheckBox(boxTipoMacchina);
        checkMacchina2->setObjectName(QStringLiteral("checkMacchina2"));
        checkMacchina2->setGeometry(QRect(280, 50, 220, 70));
        checkMacchina2->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        checkMacchina3 = new ufgCheckBox(boxTipoMacchina);
        checkMacchina3->setObjectName(QStringLiteral("checkMacchina3"));
        checkMacchina3->setGeometry(QRect(540, 50, 220, 70));
        checkMacchina3->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        buttonExit = new ufgButton(dialogSetup);
        buttonExit->setObjectName(QStringLiteral("buttonExit"));
        buttonExit->setGeometry(QRect(10, 10, 80, 80));
        buttonPassword = new ufgButton(dialogSetup);
        buttonPassword->setObjectName(QStringLiteral("buttonPassword"));
        buttonPassword->setGeometry(QRect(710, 10, 80, 80));

        retranslateUi(dialogSetup);

        QMetaObject::connectSlotsByName(dialogSetup);
    } // setupUi

    void retranslateUi(QDialog *dialogSetup)
    {
        boxTipoMacchina->setTitle(QApplication::translate("dialogSetup", "Setup", 0));
        Q_UNUSED(dialogSetup);
    } // retranslateUi

};

namespace Ui {
    class dialogSetup: public Ui_dialogSetup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGSETUP_H
