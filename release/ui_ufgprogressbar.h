/********************************************************************************
** Form generated from reading UI file 'ufgprogressbar.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UFGPROGRESSBAR_H
#define UI_UFGPROGRESSBAR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ufgProgressBar
{
public:

    void setupUi(QWidget *ufgProgressBar)
    {
        if (ufgProgressBar->objectName().isEmpty())
            ufgProgressBar->setObjectName(QStringLiteral("ufgProgressBar"));
        ufgProgressBar->resize(563, 271);
        ufgProgressBar->setMouseTracking(true);
        ufgProgressBar->setWindowTitle(QStringLiteral("Form"));

        retranslateUi(ufgProgressBar);

        QMetaObject::connectSlotsByName(ufgProgressBar);
    } // setupUi

    void retranslateUi(QWidget *ufgProgressBar)
    {
        Q_UNUSED(ufgProgressBar);
    } // retranslateUi

};

namespace Ui {
    class ufgProgressBar: public Ui_ufgProgressBar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UFGPROGRESSBAR_H
