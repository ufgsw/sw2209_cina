/********************************************************************************
** Form generated from reading UI file 'dialogorologio.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGOROLOGIO_H
#define UI_DIALOGOROLOGIO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <ufgbutton.h>
#include "ufgbutton1label.h"
#include "ufgorologio.h"

QT_BEGIN_NAMESPACE

class Ui_DialogOrologio
{
public:
    ufgButton *buttonExit;
    ufgOrologio *orologio;
    ufgButton1label *buttonOre;
    ufgButton1label *buttonMinuti;
    ufgButton1label *buttonSecondi;
    QLabel *label;
    QLabel *label_2;
    QLabel *labelTitolo;

    void setupUi(QDialog *DialogOrologio)
    {
        if (DialogOrologio->objectName().isEmpty())
            DialogOrologio->setObjectName(QStringLiteral("DialogOrologio"));
        DialogOrologio->resize(800, 480);
        buttonExit = new ufgButton(DialogOrologio);
        buttonExit->setObjectName(QStringLiteral("buttonExit"));
        buttonExit->setGeometry(QRect(10, 10, 80, 80));
        buttonExit->setFocusPolicy(Qt::StrongFocus);
        orologio = new ufgOrologio(DialogOrologio);
        orologio->setObjectName(QStringLiteral("orologio"));
        orologio->setGeometry(QRect(10, 130, 320, 300));
        buttonOre = new ufgButton1label(DialogOrologio);
        buttonOre->setObjectName(QStringLiteral("buttonOre"));
        buttonOre->setGeometry(QRect(390, 230, 111, 71));
        buttonOre->setFocusPolicy(Qt::TabFocus);
        buttonMinuti = new ufgButton1label(DialogOrologio);
        buttonMinuti->setObjectName(QStringLiteral("buttonMinuti"));
        buttonMinuti->setGeometry(QRect(520, 230, 111, 71));
        buttonMinuti->setFocusPolicy(Qt::TabFocus);
        buttonSecondi = new ufgButton1label(DialogOrologio);
        buttonSecondi->setObjectName(QStringLiteral("buttonSecondi"));
        buttonSecondi->setGeometry(QRect(650, 230, 111, 71));
        buttonSecondi->setFocusPolicy(Qt::TabFocus);
        label = new QLabel(DialogOrologio);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(505, 250, 10, 30));
        QFont font;
        font.setPointSize(22);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setText(QStringLiteral(":"));
        label_2 = new QLabel(DialogOrologio);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(635, 250, 10, 30));
        label_2->setFont(font);
        label_2->setText(QStringLiteral(":"));
        labelTitolo = new QLabel(DialogOrologio);
        labelTitolo->setObjectName(QStringLiteral("labelTitolo"));
        labelTitolo->setGeometry(QRect(110, 10, 671, 81));
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        font1.setPointSize(20);
        font1.setBold(false);
        font1.setWeight(50);
        labelTitolo->setFont(font1);
        labelTitolo->setStyleSheet(QStringLiteral("color: rgba(0, 0, 255, 231);"));
        labelTitolo->setFrameShape(QFrame::Box);
        labelTitolo->setFrameShadow(QFrame::Plain);
        labelTitolo->setText(QStringLiteral("TextLabel"));

        retranslateUi(DialogOrologio);

        QMetaObject::connectSlotsByName(DialogOrologio);
    } // setupUi

    void retranslateUi(QDialog *DialogOrologio)
    {
        DialogOrologio->setWindowTitle(QApplication::translate("DialogOrologio", "Dialog", 0));
    } // retranslateUi

};

namespace Ui {
    class DialogOrologio: public Ui_DialogOrologio {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGOROLOGIO_H
