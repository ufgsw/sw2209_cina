/********************************************************************************
** Form generated from reading UI file 'ddmbutton.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DDMBUTTON_H
#define UI_DDMBUTTON_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ddmButton
{
public:
    QGridLayout *gridLayout;
    QLabel *label;

    void setupUi(QWidget *ddmButton)
    {
        if (ddmButton->objectName().isEmpty())
            ddmButton->setObjectName(QStringLiteral("ddmButton"));
        ddmButton->resize(55, 43);
        ddmButton->setWindowTitle(QStringLiteral("Form"));
        ddmButton->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        gridLayout = new QGridLayout(ddmButton);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(ddmButton);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(16);
        label->setFont(font);
        label->setText(QStringLiteral("A"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 0, 1, 1);


        retranslateUi(ddmButton);

        QMetaObject::connectSlotsByName(ddmButton);
    } // setupUi

    void retranslateUi(QWidget *ddmButton)
    {
        Q_UNUSED(ddmButton);
    } // retranslateUi

};

namespace Ui {
    class ddmButton: public Ui_ddmButton {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DDMBUTTON_H
