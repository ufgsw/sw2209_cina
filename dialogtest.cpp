#include "dialogtest.h"
#include "ui_dialogtest.h"
#include "keyboard/widgetKeyBoard.h"

DialogTest::DialogTest(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogTest)
{

    ui->setupUi(this);

    tastiera = new widgetKeyBoard(false);
    tastiera->setGeometry(0,0,800,200);
    this->tastiera->setZoomFacility(true);
    this->tastiera->enableSwitchingEcho(false); // enable possibility to change echo through keyboard
    this->tastiera->createKeyboard(); // only create keyboard

    mAirspeedGauge = new QcGaugeWidget(this);
    mAirspeedGauge->setGeometry(100,20,200,200);

    mAirspeedGauge->addArc(55);
    mAirspeedGauge->addDegrees(65)->setValueRange(0,100);
    QcColorBand *clrBand = mAirspeedGauge->addColorBand(50);
    clrBand->setValueRange(0,100);
    mAirspeedGauge->addValues(80)->setValueRange(0,100);
    mAirspeedGauge->addLabel(70)->setText("Km/h");
    QcLabelItem *lab = mAirspeedGauge->addLabel(40);
    lab->setText("0");
    mAirspeedNeedle = mAirspeedGauge->addNeedle(60);
    mAirspeedNeedle->setLabel(lab);
    mAirspeedNeedle->setColor(Qt::blue);
    mAirspeedNeedle->setValueRange(0,100);
    mAirspeedNeedle->setCurrentValue(20);
    mAirspeedGauge->addBackground(7);
    //ui->horizontalLayout->addWidget(mAirspeedGauge);
    mAirspeedGauge->show();


    mCompassGauge = new QcGaugeWidget(this);
    mCompassGauge->setGeometry(400,20,200,200);

    mCompassGauge->addBackground(99);
    QcBackgroundItem *bkg1 = mCompassGauge->addBackground(92);
    bkg1->clearrColors();
    bkg1->addColor(0.1,Qt::black);
    bkg1->addColor(1.0,Qt::white);

    QcBackgroundItem *bkg2 = mCompassGauge->addBackground(88);
    bkg2->clearrColors();
    bkg2->addColor(0.1,Qt::white);
    bkg2->addColor(1.0,Qt::black);

    QcLabelItem *w = mCompassGauge->addLabel(80);
    w->setText("W");
    w->setAngle(0);
    w->setColor(Qt::white);

    QcLabelItem *n = mCompassGauge->addLabel(80);
    n->setText("N");
    n->setAngle(90);
    n->setColor(Qt::white);

    QcLabelItem *e = mCompassGauge->addLabel(80);
    e->setText("E");
    e->setAngle(180);
    e->setColor(Qt::white);

    QcLabelItem *s = mCompassGauge->addLabel(80);
    s->setText("S");
    s->setAngle(270);
    s->setColor(Qt::white);

    QcDegreesItem *deg = mCompassGauge->addDegrees(70);
    deg->setStep(5);
    deg->setMaxDegree(270);
    deg->setMinDegree(-75);
    deg->setColor(Qt::white);
    mCompassNeedle = mCompassGauge->addNeedle(60);
    mCompassNeedle->setNeedle(QcNeedleItem::CompassNeedle);
    mCompassNeedle->setValueRange(0,360);
    mCompassNeedle->setMaxDegree(360);
    mCompassNeedle->setMinDegree(0);
    mCompassGauge->addBackground(7);
    mCompassGauge->addGlass(88);


    mCompassGauge->show();
}

DialogTest::~DialogTest()
{
    delete ui;
}

void DialogTest::on_buttonShowTastiera_clicked()
{
    miaTastiera = new ufgTastiera(this);
    miaTastiera->setGeometry(0,0,800,200);
    miaTastiera->showNormal();

//    tastiera->show(this);
//    tastiera->focusThis(ui->mioEdit);
//    tastiera->move(0,200);

}
widgetKeyBoard *DialogTest::getKeyboard()
{
    return (this->tastiera );
}
