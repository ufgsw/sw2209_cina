#ifndef UFGCHECKBOX_H
#define UFGCHECKBOX_H

#include <QWidget>

namespace Ui {
class ufgCheckBox;
}

class ufgCheckBox : public QWidget
{
    Q_OBJECT

public:
    explicit ufgCheckBox(QWidget *parent = 0);
    ~ufgCheckBox();

    bool checked;
    bool focus;
    void setChecked( bool ck);
    void setTitle(QString title);
    void setColor( QColor col1, QColor col2, QColor cborder, QColor cfocus);

    void setFontSize(int size);

private:
    Ui::ufgCheckBox *ui;

    QColor backgroundColor1;
    QColor backgroundColor2;
    QColor borderColor;
    QColor focusColor;

    void paintEvent(QPaintEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void focusInEvent(QFocusEvent* e);
    void focusOutEvent(QFocusEvent* e);

    void aggiornaImmagine();

signals:
    void buttonClicked();

};

#endif // UFGCHECKBOX_H
