#include "dialogsetvar.h"
#include "ui_dialogsetvar.h"
#include "QPainter"

#include <string.h>

dialogSetVar::dialogSetVar(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogSetVar)
{
    ui->setupUi(this);

    ui->buttonPiu->setImage(   ":/img/ico/piu.png"  , ":/img/ico/_piu.png" , ":/img/ico/piu.png" , 100);
    ui->buttonMeno->setImage(  ":/img/ico/meno.png" , ":/img/ico/_meno.png", ":/img/ico/meno.png", 100);
    ui->buttonSalva->setImage( ":/img/ico/ok.png"   , ":/img/ico/ok.png"   , ":/img/ico/ok.png"  , 100);

//    ui->buttonPiu->setBordoDimensione(2);
//    ui->buttonMeno->setBordoDimensione(2);
//    ui->buttonSalva->setBordoDimensione(2);

    connect( ui->progressBar, SIGNAL(valueChange(int)), this, SLOT(valueChanged()));
    connect( ui->buttonMeno,  SIGNAL(buttonClicked()),  this, SLOT(decrementa()));
    connect( ui->buttonPiu,   SIGNAL(buttonClicked()),  this, SLOT(incrementa()));
    connect( ui->buttonSalva, SIGNAL(buttonClicked()),  this, SLOT(salvaEsci()));

    salva      = false;
    realtime   = false;
    tipoValore = T_INT;
    valoreInc  = 1;

}

dialogSetVar::~dialogSetVar()
{
    delete ui;
}

void dialogSetVar::updateLabel()
{
    int hh,mm,ss;
    QString vals;
    switch (tipoValore) {

    case T_INT:
        vals = QString::number(ui->progressBar->value(), 10);
        ui->labelValore->setText(vals);
        break;

    case T_FLOAT:
        vals = QString::number(ui->progressBar->value(), 10);
        ui->labelValore->setText(vals);

        break;

    case T_PERCENT:
        vals = QString("%1 %").arg(ui->progressBar->value());
        ui->labelValore->setText(vals);
        break;

    case T_TIME:
        mm = ui->progressBar->value() / 60;
        ss = ui->progressBar->value() - ( mm * 60);
        vals = QString("%1:%2  min").arg(mm,2,10,QLatin1Char('0')).arg(ss,2,10,QLatin1Char('0'));
        ui->labelValore->setText(vals);
        break;

    default:
        break;
    }
    if( realtime == true )
    {
        emit saveValueInt(ui->progressBar->value() );
    }
}

void dialogSetVar::valueChanged()
{
    this->updateLabel();
}

void dialogSetVar::setTitle(QString title)
{
    ui->labelTitolo->setText(title);
}

void dialogSetVar::setValoriInt(int min, int max, int inc, int inc2, int val)
{
    // Inc  = incremento fine ( tasti + e meno )
    // Inc2 = incremento con rotella

    ui->progressBar->setMinimum(min);
    ui->progressBar->setMaximum(max);
    ui->progressBar->setValue(val);
    ui->progressBar->setIncrement( inc, inc2 );
    valoreInc = inc;

    updateLabel();
}

void dialogSetVar::incrementa()
{
    if( tipoValore == T_FLOAT )
    {

    }
    else
    {
        int val = ui->progressBar->value();
        val = val + valoreInc;
        if( val <=  ui->progressBar->maximum() )
        {
            ui->progressBar->setValue( val );
            this->updateLabel();
        }
        else
        {
            val = ui->progressBar->maximum();
            ui->progressBar->setValue( val );
            this->updateLabel();
        }
    }    
}

void dialogSetVar::decrementa()
{
    if( tipoValore == T_FLOAT )
    {

    }
    else
    {
        int val = ui->progressBar->value();
        val = val -valoreInc;
        if( val >= ui->progressBar->minimum() )
        {
            ui->progressBar->setValue( val );
            this->updateLabel();
        }
    }

}

void dialogSetVar::setEnableProgressbar( bool en)
{
    ui->progressBar->setEnabled(en);

    if( en == false ) realtime = true;
}

void dialogSetVar::paintEvent(QPaintEvent *e)
{
    //QPainterPath path;
    QPainter painter(this);
    QRect area = this->rect();

    area.setX(2);
    area.setY(2);
    area.setWidth( area.width()-2 );
    area.setHeight( area.height()-2 );


    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(4);
    pen.setBrush(Qt::blue);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);


    painter.setPen(pen);

    //path.addRoundedRect( area, 15, 15);
    //painter.drawPath( path );
    //painter.fillPath( path , Qt::blue );
    painter.drawRoundRect( area ,4,4 );
}

//void dialogSetVar::on_buttonUP_pressed()
//{
//    ui->buttonUP->setStyleSheet(QStringLiteral("border-image: url(:/img/buttonPiuDown.png);"));
//    this->incrementa();
//}

//void dialogSetVar::on_buttonUP_released()
//{
//    ui->buttonUP->setStyleSheet(QStringLiteral("border-image: url(:/img/buttonPiuUp.png);"));
//}

//void dialogSetVar::on_buttonDown_pressed()
//{
//    ui->buttonDown->setStyleSheet(QStringLiteral("border-image: url(:/img/buttonMenoDown.png);"));
//    this->decrementa();
//}

//void dialogSetVar::on_buttonDown_released()
//{
//    ui->buttonDown->setStyleSheet(QStringLiteral("border-image: url(:/img/buttonMenoUp.png);"));
//}

void dialogSetVar::salvaEsci()
{
    disconnect(rotella,SIGNAL(valueChange(bool)), ui->progressBar, SLOT(rotellaMove(bool)));
    connect( rotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    //rotella->setStep(10);
    //valore = ui->progressBar->value();
    emit saveValueInt(ui->progressBar->value());
    emit chiuso();
    this->close();
}

void dialogSetVar::setTipoValore(eTipoValore tipo)
{
    tipoValore = tipo;
}

void dialogSetVar::rotellaMove(bool up)
{
    return ;

    if( up ) this->focusNextChild();
    else     this->focusPreviousChild();

}

void dialogSetVar::rotellaClick()
{
    this->salvaEsci();

//    if(     this->focusWidget() == ui->buttonPiu   ) this->incrementa();
//    else if(this->focusWidget() == ui->buttonMeno  ) this->decrementa();
//    else if(this->focusWidget() == ui->buttonSalva ) this->on_buttonSalva_clicked();
//    else if(this->focusWidget() == ui->progressBar )
//    {
//        if( ui->progressBar->selected == false )
//        {
//            disconnect( rotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
//            connect(rotella,SIGNAL(valueChange(bool)), ui->progressBar, SLOT(rotellaMove(bool)));
//            rotella->setStep(2);
//            ui->progressBar->selected = true;
//            ui->progressBar->update();
//        }
//        else
//        {
//            disconnect(rotella,SIGNAL(valueChange(bool)), ui->progressBar, SLOT(rotellaMove(bool)));
//            connect( rotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
//            rotella->setStep(10);
//            ui->progressBar->selected = false;
//            ui->progressBar->update();
//        }
//    }
}

void dialogSetVar::setRotella( Rotella* _rotella)
{
    rotella = _rotella;
    disconnect( rotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    connect(    rotella,SIGNAL(valueChange(bool)), ui->progressBar, SLOT(rotellaMove(bool)));
    rotella->setEnableBeep( false );
    rotella->setStep(1);
    ui->progressBar->rotella = rotella;
    ui->progressBar->selected = true;
    ui->progressBar->update();

}


