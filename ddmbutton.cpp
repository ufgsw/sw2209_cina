#include "ddmbutton.h"
#include "ui_ddmbutton.h"

#include "QPainter"

ddmButton::ddmButton(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ddmButton)
{
    ui->setupUi(this);

    pushColor        = QColor( 0 ,0 ,255 );
    borderColor      = QColor( 0 ,0 , 0 );
    backgroundColor1 = QColor( 255,50,0 );
    backgroundColor2 = QColor( 255,120,0);
    focusColor       = QColor( 255,0,0);

    premuto     = false;
    focus       = false;
    checkButton = false;
    abilitato   = true;
}

ddmButton::~ddmButton()
{
    delete ui;
}

void ddmButton::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    QLinearGradient myGradient;
    QPen penPremuto( pushColor,   2);
    QPen penBordo(   borderColor, 2);
    QPen penFocus(   focusColor,  2);
    QPen penDisable( QColor( 200,200,200), 2);

    QRect brect = this->rect();
    brect.setWidth( this->rect().width() - 4 );
    brect.setHeight( this->rect().height() - 4);
    brect.setLeft(4);
    brect.setTop(4);

    myGradient = QLinearGradient( QPointF(0,0),QPointF(1,0) );

    QPainterPath path;
    path.addRoundedRect( brect, 8 , 8 );

    if( abilitato == false )
    {
        //path.addEllipse( brect.center(), 25 , 25 );
        p.setPen(penDisable);
        //p.fillPath(path, myGradient );
        p.drawPath(path);
    }
    else if( premuto == true)
    {
        //path.addEllipse( brect.center(), 25 , 25 );
        p.setPen(penPremuto);
        //p.fillPath(path, myGradient );
        p.drawPath(path);
    }
    else if( focus == true )
    {
        //path.addEllipse( brect.center(), 25 , 25 );
        p.setPen(penFocus);
        //p.fillPath(path, myGradient );
        p.drawPath(path);
    }
    else
    {
        //path.addEllipse( brect.center(), 25 , 25 );
        p.setPen(penBordo);
        //p.fillPath(path, myGradient );
        p.drawPath(path);
    }
}

void ddmButton::mousePressEvent(QMouseEvent* )
{

    if( this->checkButton == false )
    {
        premuto = true;
    }
    else
    {
        if( premuto == true ) premuto = false;
        else                  premuto = true;
    }
    this->update();
}

void ddmButton::mouseReleaseEvent(QMouseEvent* )
{
    if( this->checkButton == false )
    {
        premuto = false;
        this->update();
    }
    emit buttonClicked( this );
}

void ddmButton::focusInEvent(QFocusEvent* )
{

}

void ddmButton::focusOutEvent(QFocusEvent* )
{

}

void ddmButton::setChecked(bool check )
{

}

void ddmButton::setTasto( QString nome, tipoTasto _tipo)
{
    ui->label->setText(nome);
    tipo = _tipo;
}

void ddmButton::setPixmap( QPixmap png)
{

}

void ddmButton::setColors(QColor bg1, QColor bg2, QColor border, QColor focus)
{

}

QString ddmButton::getText()
{
    return ui->label->text();
}

tipoTasto ddmButton::getTipo()
{
    return tipo;
}

bool ddmButton::isChecked()
{
    return premuto;
}

void ddmButton::setEnabled(bool en)
{
    abilitato = en;
}
