#include "ufgtestoedit.h"
#include <QThread>

ufgTestoEdit::ufgTestoEdit(QWidget *parent) : QLineEdit(parent)
{
    miaTastiera   = NULL;
    //miaTastieraFs = NULL;
}

void ufgTestoEdit::mousePressEvent(QMouseEvent *)
{

//    if( miaTastiera == NULL ) {
//        runTastiera();
//    }
ufgTastieraFs* miaTastieraFs;

    miaTastieraFs = new ufgTastieraFs(this);
    miaTastieraFs->setGeometry(0,0,800,480);
    miaTastieraFs->defaultText( this->text() );

    connect( miaTastieraFs, SIGNAL(endEdit(QString)), this, SLOT(tastieraFsChiusa(QString)));
    miaTastieraFs->exec();

    qDebug("Fine edit tastiera");
    delete miaTastieraFs;
}

void ufgTestoEdit::mouseReleaseEvent(QMouseEvent *)
{
    this->setCursorPosition( this->text().length() );
}

void ufgTestoEdit::focusOutEvent(QFocusEvent *)
{
    qDebug("focusout");
}

void ufgTestoEdit::runTastiera()
{
    miaTastiera = new ufgTastiera(this);
    miaTastiera->setGeometry(0,0,800,220);
    miaTastiera->setLineEdit(this);
    connect( miaTastiera, SIGNAL(close()), this, SLOT(tastieraChiusa()));
    connect( miaTastiera, SIGNAL(started()), this, SLOT(tastieraAperta()));

    QRect st = this->geometry();

    anim1 = new QPropertyAnimation( this , "geometry");
    anim1->setDuration(100);
    anim1->setStartValue( st );
    anim1->setEndValue(QRect(0,195, 800 , 40));

    connect(anim1,SIGNAL(finished()), this, SLOT(fineEdit()));

    anim1->start();
}

void ufgTestoEdit::tastieraChiusa()
{
    miaTastiera->setLineEdit(NULL);

    anim1 = new QPropertyAnimation( this , "geometry");
    anim1->setDuration(100);
    anim1->setStartValue( this->geometry() );
    anim1->setEndValue( miaTastiera->getStartRectEdit() );

    anim1->start();

    miaTastiera->hide();
    delete miaTastiera;
    miaTastiera = NULL;
}

void ufgTestoEdit::tastieraFsChiusa(QString testo)
{
    this->setText(testo);
    //miaTastieraFs->hide();
    //miaTastieraFs = NULL;
}

void ufgTestoEdit::tastieraAperta()
{
    this->setCursor(Qt::IBeamCursor);
    this->setCursorPosition( this->text().length() );
    this->setFocus();



    //this->setFocusProxy(miaTastiera);
    //miaTastiera->setFocusProxy(this);
    //qDebug("apwerta");

}

void ufgTestoEdit::fineEdit()
{
    //this->setFocus(Qt::TabFocusReason);
    //miaTastiera->show();
    miaTastiera->showanimation(QRect(0,480,800,240),QRect(0,235,800,240));
}

