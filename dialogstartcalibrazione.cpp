#include "dialogstartcalibrazione.h"
#include "ui_dialogstartcalibrazione.h"

#include <QProcess>

dialogStartCalibrazione::dialogStartCalibrazione(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogStartCalibrazione)
{
    ui->setupUi(this);

    config     = NULL;
    miaRotella = NULL;

    ui->buttonExit->setImage(       ":/img/ico/return.png", ":/img/ico/return.png", ":/img/ico/return.png" , 100 );
    ui->buttonCalibra->setImage(    ":/img/ico/ok.png",":/img/ico/ok.png",":/img/ico/ok.png" , 100 );

    //connect( ui->buttonPassword, SIGNAL(buttonClicked()), this, SLOT(setPassword()));
    connect( ui->buttonExit, SIGNAL(buttonClicked()), this, SLOT(close()));
    connect( ui->buttonCalibra, SIGNAL(buttonClicked()), this, SLOT(startCalibra()));
}

dialogStartCalibrazione::~dialogStartCalibrazione()
{
    delete ui;

}

void dialogStartCalibrazione::showEvent(QShowEvent*)
{
    QString titolo      = tr("PROCEDURA DI CALIBRAZIONE");
    QString descrizione = tr("Info calibrazione");

    ui->labelMessaggio->setText( descrizione );
    ui->labelTitolo->setText( titolo );

//    if( config != NULL )
//    {
//        if( config->passwordCalibra != "0000" )
//            ui->buttonPassword->setImage( ":/img/ico/lock.png" , ":/img/ico/lock.png" , ":/img/ico/lock.png" , 100 );
//        else
//            ui->buttonPassword->setImage( ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , 100 );
//    }
}

void dialogStartCalibrazione::closeEvent(QCloseEvent*)
{
    emit onClose();
}

void dialogStartCalibrazione::setParametri(parametri *par)
{
    config = par;
}

void dialogStartCalibrazione::setRotella(Rotella *_rotella)
{
    miaRotella = _rotella;
    disconnect( miaRotella, SIGNAL(valueChange(bool)), 0, 0 );
    disconnect( miaRotella, SIGNAL(click()),           0, 0 );
    connect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    connect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));

    miaRotella->setEnableBeep( true );
    miaRotella->setStep(1);
    miaRotella->delegate = this;
}

void dialogStartCalibrazione::setPassword()
{
    QString psw1;
    QString psw2;
    tastierinoNumerico* tast;

    if( config->passwordCalibra != "0000" )
    {
        tast = new tastierinoNumerico(this);
        tast->setGeometry( 200, 40, 400,400 );
        tast->setTitle( tr("Vecchia password"));
        tast->exec();
        if( tast->getText() == config->passwordCalibra || tast->getText() == "1247" )
        {
            config->passwordCalibra = "0000";
            //ui->buttonPassword->setImage( ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , 100 );
            config->save();
        }
    }
    else
    {
        tast = new tastierinoNumerico(this);
        tast->setGeometry( 200, 40, 400,400 );
        tast->setTitle( tr("Nuova password"));
        tast->exec();
        psw1 = tast->getText();
        tast->setTitle( tr("Riscrivi password"));
        tast->exec();
        psw2 = tast->getText();

        if( psw1 == psw2 ) {
            config->passwordCalibra = psw1;
            //ui->buttonPassword->setImage( ":/img/ico/lock.png" , ":/img/ico/lock.png" , ":/img/ico/lock.png" , 100 );
            config->save();
        }
    }
    if( tast != NULL ) delete tast;
}

void dialogStartCalibrazione::startCalibra()
{
    QProcess *mount = new QProcess(this);
    mount->start("sync");
    mount->waitForFinished(-1);

    emit onStartCalibra();
    close();
}

void dialogStartCalibrazione::rotellaClick()
{

}

void dialogStartCalibrazione::rotellaMove(bool up)
{

}
