#ifndef DIALOGWARNING_H
#define DIALOGWARNING_H

#include <QDialog>

namespace Ui {
class dialogWarning;
}

class dialogWarning : public QDialog
{
    Q_OBJECT

public:
    explicit dialogWarning(QWidget *parent = 0);
    ~dialogWarning();

    void setText(QString testo);

private:
    Ui::dialogWarning *ui;

    void paintEvent(QPaintEvent *e);
};

#endif // DIALOGWARNING_H
