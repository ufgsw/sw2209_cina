#ifndef DIALOGSETVAR_H
#define DIALOGSETVAR_H

#include <QDialog>
#include "rotella.h"

enum eTipoValore {
    T_INT,
    T_FLOAT,
    T_TIME,
    T_DATE,
    T_PERCENT
};


namespace Ui {
class dialogSetVar;
}

class dialogSetVar : public QDialog
{
    Q_OBJECT

public:
    explicit dialogSetVar(QWidget *parent = 0);
    ~dialogSetVar();

    void setRotella( Rotella* _rotella);


private:
    Ui::dialogSetVar *ui;
    eTipoValore tipoValore;

    Rotella* rotella;
    bool     realtime;

    void paintEvent(QPaintEvent* e);
    void updateLabel();

public:
    bool  salva;
    //int   valore;
    int   valoreInc;
    float valoref;
    float valorefInc;

    void setValoriInt( int min, int max, int inc, int inc2, int val );
    void setValoriF(   float min, float max, float inc, float val );
    void setTitle( QString title);
    void setTipoValore( eTipoValore tipo);
    void setEnableProgressbar( bool en);

public slots:
    void incrementa();
    void decrementa();
    void valueChanged();

    void rotellaMove(bool up);
    void rotellaClick();

private slots:
    void salvaEsci();

signals:
    void saveValueInt( int value );
    void beep( int tempo );
    void chiuso();
};

#endif // DIALOGSETVAR_H
