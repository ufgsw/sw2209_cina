#ifndef DIALOGELENCOPROTOCOLLI_H
#define DIALOGELENCOPROTOCOLLI_H

#include <QDialog>
#include "rotella.h"
#include "diatermia.h"
#include "dialogselezionaprogramma.h"

typedef enum
{
    GENERICI             = 1,
    ORTOPEDICI           = 2,
    ORTOPEDICI_GINOCCHIO = 3,
    NEUROLOGICI          = 4,
    CINA                 = 5
}eProtocolli;

namespace Ui {
class dialogElencoProtocolli;
}

class dialogElencoProtocolli : public QDialog
{
    Q_OBJECT

public:
    explicit dialogElencoProtocolli(QWidget *parent = 0);
    ~dialogElencoProtocolli();

    void setProtocllo( eProtocolli _protocollo );
    void setRotella( Rotella* _rotella );
    void setLicenzaStart(bool lic);

private:
    Ui::dialogElencoProtocolli *ui;
    dialogSelezionaProgramma* formSelezionaProgramma;

    eProtocolli tipoProtocollo;
    Rotella* miaRotella;

    sProgramma programma1;
    sProgramma programma2;
    sProgramma programma3;
    sProgramma programma4;

    bool mLicenza;

public slots:
    void riprendoRotella();
    void rotellaMove(bool up);
    void rotellaClick();

    void showNext();

    void showProgrammi1();
    void showProgrammi2();
    void showProgrammi3();
    void showProgrammi4();
    void showProgrammi5();
    void showProgrammi6();
    void showProgrammi7();
    void showProgrammi8();
    void showProgrammi9();
    void showProgrammi10();
    void showProgrammi11();
    void showProgrammi12();
    void showProgrammi13();
    void showProgrammi14();

    void selezionatoProgramma(int prog);

    void esci();

signals:
    void onclose();
    void caricaProgramma( sProgramma programma);

};

#endif // DIALOGELENCOPROTOCOLLI_H
