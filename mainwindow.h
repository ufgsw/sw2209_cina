#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>

#include "ufgbutton2label.h"
#include "dialodselezionaab.h"
#include "dialogselezionaabc.h"
#include "dialogselezionatrattamento.h"
#include "dialogsetvar.h"
#include "dialoginfo.h"
#include "dialogsetup.h"
#include "dialogmenuguida.h"
#include "dialogstartcalibrazione.h"
#include "dialogwarning.h"
#include "dieloguser.h"
#include "dialogelencoprotocolli.h"
#include "rotella.h"
#include "ufgseriale.h"

//#include "ufgbutton.h"
//#include "ufgtestoedit.h"
//#include "ufgtastiera.h"
#include "diatermia.h"

#include <QTranslator>
#include <QFile>
#include <QDir>
#include <QDateTime>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#define CT200 1
#define CT250 2
#define CT300 3

struct sAlarm
{
    int al_corto            :1;
    int al_conduzione_diodi :1;
    int al_corrente         :1;
    int al_vref             :1;
    int al_tatt             :1;
    int al_vpower           :1;
};

enum eAlarm
{
    // allarmi generati durante il 'ciclo'
    AL_CORTO                = 0x0001,
    AL_CONDUZIONE_DIODI     = 0x0002,
    AL_ALCURR               = 0x0004,
    AL_VREF                 = 0x0008,
    AL_TATT                 = 0x0010,
    AL_VPOWER               = 0x0020,
    AL_TEMP                 = 0x0040,
    AL_TRANS                = 0x0080,
    AL_VPAZ                 = 0x0100,
    AL_IPAZ                 = 0x0200,
    AL_IVUOTO               = 0x0400,
    AL_VCO3                 = 0x0800,
    AL_APROVAMASSA          = 0x1000,
    AL_SONDA_NC             = 0x2000,
    AL_VMIN                 = 0x4000,
    AL_DEADMAN              = 0x8000,
};

enum eWarning
{
    // warning generati prima dello start del 'ciclo'
    W_COMUNICAZIONE             = 0x0001,
    W_ELETTRODOMASSA_SCOLLEGATO = 0x1000,
    W_ELETTRODO_SCOLLEGATO      = 0x2000,
    W_POMPETTA                  = 0x8000
};


enum eStato
{
    IN_STOP = 1,
    IN_PAUSA,
    IN_CALIBRAZIONE,
    IN_RUN
};

union uAlarm
{
    int    val;
    eAlarm tipo;
};

union uWarning
{
    int      val;
    eWarning tipo;
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QThread *threadSerial;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setParametri(sProgramma* pa);
    void refreshParametri();

public slots:

    void newConfig();

    void setFrequenza();
    void setFrequenzaPemf();
    void setTrattamento();
    void setPotenza(int val);
    void setTempo(int secondi);

    void getFrequenzaPemf();
    void getFrequenza();
    void getTrattamento();
    void getPotenza();
    void getElettrodi();
    void getTempo();

    void incrementaPotenza();
    void decrementaPotenza();

    void showMenuGuida();
    void showInfo();
    void showAllarme();
    void showWarning();
    void showSetup();
    void showCalibra();

    void updateTabFocus();
    void setCicala(int tempo);

    void rotellaMove(bool up);
    void rotellaClick();

    void procSerialRx(char* line,int len);
    void proc485Rx(   char* line,int len);
    void comTask();
    void refreshTask();

    void startStop(void);
    void startEsecuzione(void);
    void stopEsecuzione(void);
    void pausaEsecuzione(void);

    void caricaProgramma( sProgramma programma );
    void setLingua( QString lingua, bool reload );

private slots:

    void exeCalibrazione();
    void riprendoRotella();
    void resetAllarmi();
    void richiediPassword();

signals:
    void allarmeInCorso();
    void warningInCorso();

private:
    Ui::MainWindow *ui;

    ufgSeriale*  seriale;
    ufgSeriale*  rs485;
    QFont font;

    parametri*  config;
    QTranslator traduttore;

    dialogSelezionaabcd*     formSelezionaABCD;
    dialogSelezionaabc*      formSelezionaABC;
    dialodSelezionaAB*       formSelezionaAB;
    dialogInfo*              formInfo;
    dialogSetVar*            formSetVar;
    dialogMenuGuida*         formGuida;
    dialogSetup*             formSetup;
    dialogStartCalibrazione* formCalibra;
    dialogWarning*           formWarning;
    dialogUser*              formUser;
    dialogElencoProtocolli*  formElencoProtocolli;

    tastierinoNumerico* tastierino;

    Rotella* miaRotella;

    eStato  stato;

    uWarning    mioWarning;
    uWarning    warning;
    uWarning    warningOld;
    uAlarm      allarmi;
    uAlarm      allarmiOld;
    QString     releaseHardware;
    int         statoMacchina;
    int         ingressi;
    uTipoSonda  sondaCollegata;
    uTipoSonda  sondaCollegataOld;

    int     allarmeComunicazione;
    int     cicala;
    int     tempoRun;       // in secondi
    int     counterPC;
    int     timeoutWarning;
    int     comando;        // 1 = calibra
    int     vco3;

    //bool trattamentoCapacitivo;

    // variabili usate per i checkBox
    bool frequenza450khz;
    bool frequenza680khz;
    bool elettrodiStatici;
    bool elettrodiDinamici;
    bool pemf100Hz;
    bool pemf200Hz;
    bool pemf300Hz;

    //bool trattamentoCapacitivoDinamico;
    //bool trattamentoCapacitivoStatico;
    //bool trattamentoResistivoDinamico;
    //bool trattamentoResistivoStatico;

    QTime timeStart;
    QTime* ptime;
    QRect  rectInterno;
    bool fermoPerWarning;

    //ufgTastiera* miatastiera;
    //QSqlTableModel *model;
    //QSqlQueryModel *modelquery;
    //QSqlDatabase db;

    bool listFileAndDirectory( QDir dir);
    unsigned short CRC16( char *puchMsg, unsigned int dataLength);
    void setScritte();

protected:
    void showEvent(QShowEvent*);
    void changeEvent(QEvent* event);

};

#endif // MAINWINDOW_H
