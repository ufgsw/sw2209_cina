#include "dialogsetup.h"
#include "ui_dialogsetup.h"

#include <QProcess>

//explicit dialogSetup(QWidget *parent = 0, parametri* conf = NULL);

dialogSetup::dialogSetup(QWidget *parent, parametri* conf ) :
    QDialog(parent),
    ui(new Ui::dialogSetup)
{
    config = conf;

    ui->setupUi(this);

    ui->buttonPassword->setBordoDimensione(2);
    ui->buttonExit->setBordoDimensione(2);
    //ui->buttonCalibra->setBordoDimensione(2);

    ui->buttonExit->setImage(       ":/img/ico/return.png", ":/img/ico/return.png", ":/img/ico/return.png" , 100 );
    //ui->buttonCalibra->setImage(    ":/img/ico/mixer.png" , ":/img/ico/mixer.png" , ":/img/ico/mixer.png" , 100 );

    connect( ui->buttonPassword, SIGNAL(buttonClicked()), this, SLOT(selezionatoPassword()));
    connect( ui->buttonExit,    SIGNAL(buttonClicked()), this, SLOT(exit()));
    //connect( ui->buttonCalibra, SIGNAL(buttonClicked()), this, SLOT(exeCalibrazione()));

    connect( ui->checkMacchina1, SIGNAL(buttonClicked()), this, SLOT(selezionatoMacchina1()));
    connect( ui->checkMacchina2, SIGNAL(buttonClicked()), this, SLOT(selezionatoMacchina2()));
    connect( ui->checkMacchina3, SIGNAL(buttonClicked()), this, SLOT(selezionatoMacchina3()));

    ui->checkMacchina1->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->checkMacchina2->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );
    ui->checkMacchina3->setColor( QColor(0,50,255),QColor(0,180,255), QColor(255,255,255), QColor(255,0,0) );

    if(      config->tipoMacchina == 1 ) selezionatoMacchina1();
    else if( config->tipoMacchina == 2 ) selezionatoMacchina2();
    else                                 selezionatoMacchina3();
}

dialogSetup::~dialogSetup()
{
    delete ui;
}

void dialogSetup::showEvent(QShowEvent *)
{
    ui->checkMacchina1->setTitle( tr("Tipo 1"));
    ui->checkMacchina2->setTitle( tr("Tipo 2"));
    ui->checkMacchina3->setTitle( tr("Tipo 3"));

    ui->boxTipoMacchina->setTitle( tr("Tipo di macchina"));
    //ui->boxLingua->setTitle( tr("Lingua"));

    if( config->passwordSetup != "0000" )
        ui->buttonPassword->setImage( ":/img/ico/lock.png" , ":/img/ico/lock.png" , ":/img/ico/lock.png" , 100 );
    else
        ui->buttonPassword->setImage( ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , 100 );

}

void dialogSetup::selezionatoMacchina1()
{
    ui->checkMacchina1->setChecked(true);
    ui->checkMacchina2->setChecked(false);
    ui->checkMacchina3->setChecked(false);

    config->tipoMacchina = 1;
}

void dialogSetup::selezionatoMacchina2()
{
    ui->checkMacchina1->setChecked(false);
    ui->checkMacchina2->setChecked(true);
    ui->checkMacchina3->setChecked(false);

    config->tipoMacchina = 2;
}

void dialogSetup::selezionatoMacchina3()
{
    ui->checkMacchina1->setChecked(false);
    ui->checkMacchina2->setChecked(false);
    ui->checkMacchina3->setChecked(true);

    config->tipoMacchina = 3;
}

void dialogSetup::selezionatoPassword()
{
    QString psw1;
    QString psw2;
    tastierinoNumerico* tast;

    if( config->passwordSetup != "0000" )
    {
        tast = new tastierinoNumerico(this);
        tast->setGeometry( 200, 40, 400,400 );
        tast->setTitle( tr("Vecchia password"));
        tast->exec();
        if( tast->getText() == config->passwordSetup || tast->getText() == "1247" )
        {
            config->passwordSetup = "0000";
            ui->buttonPassword->setImage( ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , ":/img/ico/unlock.png" , 100 );
        }
    }
    else
    {
        tast = new tastierinoNumerico(this);
        tast->setGeometry( 200, 40, 400,400 );
        tast->setTitle( tr("Nuova password"));
        tast->exec();
        psw1 = tast->getText();
        tast->setTitle( tr("Riscrivi password"));
        tast->exec();
        psw2 = tast->getText();

        if( psw1 == psw2 ) {
            config->passwordSetup = psw1;
            ui->buttonPassword->setImage( ":/img/ico/lock.png" , ":/img/ico/lock.png" , ":/img/ico/lock.png" , 100 );
        }
    }
    if( tast != NULL ) delete tast;
}

//void dialogSetup::exeCalibrazione()
//{
//    emit onCalibra();
//    close();
//}

void dialogSetup::exit()
{
    config->save();

    QProcess *mount = new QProcess(this);
    mount->start("sync");
    mount->waitForFinished(-1);

    emit onClose();
    close();
}
