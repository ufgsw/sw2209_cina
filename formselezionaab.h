#ifndef FORMSELEZIONAAB_H
#define FORMSELEZIONAAB_H

#include <QWidget>

namespace Ui {
class formSelezionaAB;
}

class formSelezionaAB : public QWidget
{
    Q_OBJECT

public:
    explicit formSelezionaAB(QWidget *parent = 0);
    ~formSelezionaAB();

    void setTitle( QString title );
    void setVar1( QString nome, bool* var);
    void setVar2( QString nome, bool* var);

private:
    Ui::formSelezionaAB *ui;

    bool* variabileA;
    bool* variabileB;

    void salva();
    void annulla();

protected:
    void paintEvent(QPaintEvent* e);

};

#endif // FORMSELEZIONAAB_H
