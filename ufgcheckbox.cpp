#include "ufgcheckbox.h"
#include "ui_ufgcheckbox.h"
#include "QPainter"

ufgCheckBox::ufgCheckBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ufgCheckBox)
{
    ui->setupUi(this);

    backgroundColor1  = QColor( 255 ,255 ,255 );
    backgroundColor2 = Qt::gray;
    borderColor = QColor( 0,0,200);
    focusColor  = Qt::black;

    focus   = false;
    checked = false;
}

ufgCheckBox::~ufgCheckBox()
{
    delete ui;
}

void ufgCheckBox::setChecked(bool ck)
{
    checked = ck;
    aggiornaImmagine();
}

void ufgCheckBox::setTitle(QString title)
{
    ui->labelTesto->setText(title);
}

void ufgCheckBox::setFontSize(int size)
{
    QFont mioFont = ui->labelTesto->font();
    mioFont.setPointSize( size );
    ui->labelTesto->setFont( mioFont );
}

void ufgCheckBox::setColor(QColor col1, QColor col2, QColor cborder, QColor cfocus)
{
    backgroundColor1 = col1;
    backgroundColor2 = col2;
    borderColor      = borderColor;
    focusColor       = Qt::black;
    this->update();
}

void ufgCheckBox::aggiornaImmagine()
{
    if( checked == true ) ui->labelImmagine->setPixmap(QPixmap(QString::fromUtf8(":/img/checkEN.png")));
    else                  ui->labelImmagine->setPixmap(QPixmap(QString::fromUtf8(":/img/checkDS.png")));

}

void ufgCheckBox::paintEvent(QPaintEvent* e)
{
    QPainter p(this);
    QLinearGradient myGradient;
    QPen pen(     QColor( 255 ,150 ,150 ), 2);
    QPen penBordo( borderColor , 3);
    QPen penFocus( focusColor  , 3);

    QRect brect = this->rect();
    brect.setWidth(  this->rect().width()  - 3 );
    brect.setHeight( this->rect().height() - 3);
    brect.setLeft(3);
    brect.setTop(3);

    myGradient = QLinearGradient( QPointF(0,0),QPointF(1,0) );

    p.setRenderHint(QPainter::Antialiasing);
    QPainterPath path;
    path.addRoundedRect( brect , 15, 15);
    myGradient.setStart(  brect.size().width() / 2, 0 );
    myGradient.setFinalStop( brect.size().width() / 2, brect.size().height() );
    myGradient.setColorAt(1, backgroundColor1 );
    myGradient.setColorAt(0, backgroundColor2 );

    if( focus == true )
    {
        p.setPen(pen);
        p.fillPath(path,myGradient);
        p.drawPath(path);
        p.setPen(penFocus);
        p.drawRoundedRect( brect , 15, 15);
    }
    else
    {
        p.setPen(pen);
        p.fillPath(path,myGradient);
        p.drawPath(path);
        p.setPen(penBordo);
        p.drawRoundedRect( brect , 15, 15);
    }
}

void ufgCheckBox::mousePressEvent(QMouseEvent* e)
{
//    if( checked == true ) checked = false;
//    else                  checked = true;
//    aggiornaImmagine();
}

void ufgCheckBox::mouseReleaseEvent(QMouseEvent* e)
{
    emit buttonClicked();

}

void ufgCheckBox::focusInEvent(QFocusEvent *e)
{
    focus = true;
    this->update();
}

void ufgCheckBox::focusOutEvent(QFocusEvent *e)
{
    focus = false;
    this->update();
}
