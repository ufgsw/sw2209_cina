#include "dialogmenuguida.h"
#include "ui_dialogmenuguida.h"

dialogMenuGuida::dialogMenuGuida(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogMenuGuida)
{
    ui->setupUi(this);

    ui->buttonExit->setImage( ":/img/ico/return.png", ":/img/ico/return.png", ":/img/ico/return.png" , 100 );
    //ui->buttonExit->setBordoDimensione(2);

    connect( this->ui->buttonExit, SIGNAL(buttonClicked()), this, SLOT(esci()) );
    ui->buttonExit->setFocus();

    connect( this->ui->buttonGenerici, SIGNAL(buttonClicked()),    this, SLOT(showGenerici()));
    connect( this->ui->buttonNeurologici, SIGNAL(buttonClicked()), this, SLOT(showNeurologici()));
    connect( this->ui->buttonOrtopedici, SIGNAL(buttonClicked()),  this, SLOT(showOrtopedici()) );

    formElencoProtocolli = new dialogElencoProtocolli(this);
    formElencoProtocolli->setGeometry( 0,0,800,480 );

    connect( formElencoProtocolli, SIGNAL(caricaProgramma(sProgramma)), this, SLOT(selezionatoProgramma(sProgramma)));
    connect( formElencoProtocolli, SIGNAL(onclose()), this, SLOT(riprendoRotella()));
}



dialogMenuGuida::~dialogMenuGuida()
{
    delete formElencoProtocolli;
    delete ui;
}

void dialogMenuGuida::showEvent(QShowEvent*)
{
    ui->buttonGenerici->setTitle( tr("PROTOCOLLI"));
    ui->buttonGenerici->setText(  tr("GENERICI"));
    ui->buttonNeurologici->setTitle( tr( "PROTOCOLLI PATOLOGIE"));
    ui->buttonNeurologici->setText( tr("NEUROLOGICI"));
    ui->buttonOrtopedici->setTitle( tr("PROTOCOLLI PATOLOGIE"));
    ui->buttonOrtopedici->setText( tr("ORTOPEDICI"));

    ui->labelGenerici->setText( tr("Descrizione protocolli generici") );
    ui->labelNeurologici->setText( tr( "Descrizione protocolli neurologici"));
    ui->labelOrtopedici->setText( tr("Descrizione protocolli ortopedici"));
}

void dialogMenuGuida::rotellaMove(bool up)
{
//    if( ui->selezioneA->focus ) ui->selezioneB->setFocus();
//    else ui->selezioneA->setFocus();

//    if(     this->focusWidget() == ui->selezioneA  ) selezionatoA();
//    else if(this->focusWidget() == ui->selezioneB  ) selezionatoB();

    if( up ) {
        this->focusNextChild();
    }
    else
    {
        this->focusPreviousChild();
    }
}

void dialogMenuGuida::rotellaClick()
{
//    esci();

    if(      this->focusWidget() == ui->buttonExit        ) esci();
    else if( this->focusWidget() == ui->buttonGenerici    )
    {
        showGenerici();
    }
    else if( this->focusWidget() == ui->buttonNeurologici )
    {
        showNeurologici();
    }
    else if( this->focusWidget() == ui->buttonOrtopedici  )
    {
        showOrtopedici();
    }
}

void dialogMenuGuida::setRotella(Rotella *_rotella)
{
    miaRotella = _rotella;
    this->riprendoRotella();
}

void dialogMenuGuida::riprendoRotella()
{
    disconnect( miaRotella, SIGNAL(valueChange(bool)), 0, 0 );
    disconnect( miaRotella, SIGNAL(click()),           0, 0 );
    connect( miaRotella, SIGNAL(valueChange(bool)), this, SLOT(rotellaMove(bool)));
    connect( miaRotella, SIGNAL(click()),           this, SLOT(rotellaClick()));

    miaRotella->setEnableBeep( true );
    miaRotella->setStep(1);
    miaRotella->delegate = this;
}

void dialogMenuGuida::esci()
{
    emit onClose();
    close();
}

void dialogMenuGuida::showGenerici()
{
    formElencoProtocolli->setLicenzaStart( mLicenza   );
    formElencoProtocolli->setProtocllo(    GENERICI   );
    formElencoProtocolli->setRotella(      miaRotella );
    formElencoProtocolli->showNormal();
}

void dialogMenuGuida::showOrtopedici()
{
    formElencoProtocolli->setLicenzaStart( mLicenza   );
    formElencoProtocolli->setProtocllo(    ORTOPEDICI );
    formElencoProtocolli->setRotella(      miaRotella );
    formElencoProtocolli->showNormal();
}

void dialogMenuGuida::showNeurologici()
{
    formElencoProtocolli->setLicenzaStart( mLicenza );
    formElencoProtocolli->setProtocllo( NEUROLOGICI );
    formElencoProtocolli->setRotella( miaRotella );
    formElencoProtocolli->showNormal();
}

void dialogMenuGuida::selezionatoProgramma(sProgramma programma)
{
    emit( caricaProgramma(programma) );
    this->close();
}
