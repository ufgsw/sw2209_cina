#ifndef UFGTASTIERA_H
#define UFGTASTIERA_H

#include <QDialog>
#include <QLineEdit>
#include <QPropertyAnimation>
#include <QTimer>

#include "ufgbuttontastiera.h"

enum tipoTastiera
{
    T_DEFAULT,
    T_MINUSCOLE,
    T_MAIUSCOLE,
    T_ALT1,
    T_ALT2
};

namespace Ui {
class ufgTastiera;
}

class ufgTastiera : public QDialog
{
    Q_OBJECT

public:
    explicit ufgTastiera(QWidget *parent = 0);
    ~ufgTastiera();

    void setLineEdit(QLineEdit* _edit);
    void showanimation( QRect start, QRect end);

    QRect getStartRectEdit();

private:
    Ui::ufgTastiera *ui;

    QTimer* timer;
    QPropertyAnimation* anim1;
    QLineEdit* edit;

    tipoTastiera tipo;
    tipoTastiera oldTipo;

    QRect rectStart;
    QRect rectEnd;
    QRect rectEdit;

    bool visibile;
    bool shift;
    bool caps;

    void setFN(tipoTastiera _fn);

private slots:
    void tastoPremuto(ufgButtonTastiera* tasto);
    void animazione();
protected:
    void paintEvent(QPaintEvent *);
    void showEvent(QShowEvent* );

signals:
    void close();
    void started();
};

#endif // UFGTASTIERA_H
