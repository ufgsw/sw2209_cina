#ifndef DIELOGUSER_H
#define DIELOGUSER_H

#include <QDialog>
#include "diatermia.h"
#include "tastierinonumerico.h"
#include "rotella.h"
#include "dialogstartcalibrazione.h"

#include "dialogorologio.h"

namespace Ui {
class dielogUser;
}

class dialogUser : public QDialog
{
    Q_OBJECT

public:
    explicit dialogUser(QWidget *parent = 0);
    ~dialogUser();
    void setParametri( parametri* conf);
    void setRotella( Rotella* _rotella);
private:
    Ui::dielogUser *ui;
    parametri* config;
    Rotella*   mRotella;

    DialogOrologio* formOrologio;
    dialogStartCalibrazione* formCalibra;

private slots:
    void buttonItalia_clicked();
    void buttonInglese_clicked();
    void buttonTedesco_clicked();
    void buttonCinese_clicked();

    void selezionatoPassword();
    void showCalibra();
    void exeCalibra();
    void showOrologio();
    void exit();
signals:
    void onCalibra();
    void onClose();
};

#endif // DIELOGUSER_H
