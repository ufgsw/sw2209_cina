#ifndef DIALOGSELEZIONAPROGRAMMA_H
#define DIALOGSELEZIONAPROGRAMMA_H

#include <QDialog>
#include "diatermia.h"
#include "rotella.h"

namespace Ui {
class dialogSelezionaProgramma;
}

class dialogSelezionaProgramma : public QDialog
{
    Q_OBJECT

public:
    explicit dialogSelezionaProgramma(QWidget *parent = 0);
    ~dialogSelezionaProgramma();

    void setProgrammi( QString protocollo, QString programma, sProgramma* prog1, sProgramma* prog2, sProgramma* prog3, sProgramma* prog4 );
    void setRotella(Rotella *_rotella);
    void setLicenzaStart(bool lic);

private:
    Ui::dialogSelezionaProgramma *ui;

    int tipoTrattamento;
    Rotella* miaRotella;

    bool mLicenza;

    void showEvent(QShowEvent *);


public slots:
    void rotellaMove(bool up);
    void rotellaClick();

    void selezionato1();
    void selezionato2();
    void selezionato3();
    void selezionato4();

    void esci();

signals:
    void programmaSelezionato( int numero );
    void onclose();
};

#endif // DIALOGSELEZIONAPROGRAMMA_H
