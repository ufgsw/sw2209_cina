#include "ufgbutton.h"
#include "QPainter"

ufgButton::ufgButton(QWidget *parent) : QWidget(parent)
{
//    QRect area = this->rect();
//    area.setWidth( area.width() - 4);
//    area.setHeight( area.height() -4 );

    colorNormal = QColor(142,180,227);
    colorPress  = QColor(0,0,255);
    colorFocus  = QColor( 255,150,0);

    dimensioneBordo = 4;

    bordo   = true;
    premuto = false;
    focus   = false;
}

void ufgButton::setImage(QString normal, QString press, QString focus, int scale )
{
    imageNormal = QPixmap(QString::fromUtf8(normal.toUtf8()));
    imagePress  = QPixmap(QString::fromUtf8(press.toUtf8()));
    imageFocus  = QPixmap(QString::fromUtf8(focus.toUtf8()));

    if( scale != 100 )
    {
        QSize dim;
        dim.setHeight(  (imageNormal.height() * scale) / 100  );
        dim.setWidth(   (imageNormal.width() * scale) / 100   );
        imageNormal = imageNormal.scaled( dim );
        imageFocus  = imageFocus.scaled( dim  );
        imagePress  = imagePress.scaled( dim  );
    }

    this->update();
}

void ufgButton::setBordoEnable(bool en)
{
    bordo = en;
    this->update();
}

void ufgButton::paintEvent(QPaintEvent *e)
{
    //QRect        area = this->rect();
    QPainter     p(this);
    QPen         pen(  QColor( 142 ,180 ,227 ), dimensioneBordo );

    QRect brect = this->rect();
    brect.setWidth( this->rect().width() - dimensioneBordo );
    brect.setHeight( this->rect().height() - dimensioneBordo);
    brect.setLeft(dimensioneBordo);
    brect.setTop(dimensioneBordo);


    if( premuto )
    {
        pen.setColor(colorPress);
        p.setPen(pen);
        if( bordo ) p.drawRoundedRect( brect , 15, 15);

        p.drawPixmap( (this->width()  - imagePress.width())  / 2,
                      (this->height() - imagePress.height()) / 2,
                      imagePress.width(),imagePress.height(),imagePress);
    }
    else if(focus )
    {        
        pen.setColor(colorFocus);
        p.setPen(pen);
        if( bordo ) p.drawRoundedRect( brect , 15, 15);

        p.drawPixmap( (this->width()  - imageFocus.width())  / 2,
                      (this->height() - imageFocus.height()) / 2,
                      imageFocus.width(),imageFocus.height(),imageFocus);
    }
    else
    {
        pen.setColor(colorNormal);
        p.setPen(pen);
        if( bordo ) p.drawRoundedRect( brect , 15, 15);

        p.drawPixmap( (this->width()  - imageNormal.width())  / 2,
                      (this->height() - imageNormal.height()) / 2,
                      imageNormal.width(),imageNormal.height(),imageNormal);
    }
}

void ufgButton::mousePressEvent(QMouseEvent* e)
{
    if( this->isEnabled() )
    {
        premuto = true;
        this->update();
    }
}

void ufgButton::mouseReleaseEvent(QMouseEvent* e)
{
    if( this->isEnabled() )
    {
        premuto = false;
        this->update();
        emit buttonClicked();
    }
}

void ufgButton::focusInEvent(QFocusEvent* e)
{
    focus = true;
    this->update();
}

void ufgButton::focusOutEvent(QFocusEvent* e)
{
    focus = false;
    this->update();
}


