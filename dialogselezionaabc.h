#ifndef DIALOGSELEZIONAABC_H
#define DIALOGSELEZIONAABC_H

#include <QDialog>

namespace Ui {
class dialogSelezionaabc;
}

class dialogSelezionaabc : public QDialog
{
    Q_OBJECT

public:
    explicit dialogSelezionaabc(QWidget *parent = 0);
    ~dialogSelezionaabc();

    void setTitle(QString title);
    void setVariabileA( bool* var, QString name);
    void setVariabileB( bool* var, QString name);
    void setVariabileC( bool* var, QString name);


private:
    Ui::dialogSelezionaabc *ui;

    void paintEvent(QPaintEvent *e);
    void showEvent(QShowEvent* e);

    bool* variabileA;
    bool* variabileB;
    bool* variabileC;

public slots:
    void selezionatoA();
    void selezionatoB();
    void selezionatoC();
    void esci();

    void rotellaMove(bool up);
    void rotellaClick();

signals:
    void onClose();

};

#endif // DIALOGSELEZIONAABC_H
