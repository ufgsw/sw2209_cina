#ifndef DIALOGINFO_H
#define DIALOGINFO_H

#include <QDialog>

namespace Ui {
class dialogInfo;
}

class dialogInfo : public QDialog
{
    Q_OBJECT

public:
    explicit dialogInfo(QWidget *parent = 0);
    ~dialogInfo();

    void setText(QString text);
    void setTitle( QString title);
    void setImageTitle( QString file);

    void setRotella( int rotella, int tasto );

private:
    Ui::dialogInfo *ui;

    void paintEvent(QPaintEvent* e);
signals:
    void onClose();
    void beep(int tempo);

public slots:
    void rotellaMove(bool up);
    void rotellaClick();

private slots:
    void esci();

};

#endif // DIALOGINFO_H
