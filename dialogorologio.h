#ifndef DIALOGOROLOGIO_H
#define DIALOGOROLOGIO_H

#include <QDialog>
#include "tastierinonumerico.h"
#include <QTimer>

namespace Ui {
class DialogOrologio;
}

class DialogOrologio : public QDialog
{
    Q_OBJECT

public:
    explicit DialogOrologio(QWidget *parent = 0);
    ~DialogOrologio();

private:
    Ui::DialogOrologio *ui;
    QTimer* timer;
    tastierinoNumerico* tastierino;

    int mOre;
    int mMinuti;
    int mSecondi;


    void showEvent(QShowEvent* e);

private slots:

    void esci();

    void aggiorna();

    void setOre();
    void setMinuti();
    void setSecondi();


};

#endif // DIALOGOROLOGIO_H
