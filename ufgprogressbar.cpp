#include "ufgprogressbar.h"
#include "ui_ufgprogressbar.h"

#include <QPainter>
#include <QMouseEvent>
#include <QTimer>

ufgProgressBar::ufgProgressBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ufgProgressBar)
{
    ui->setupUi(this);

    myMinimum          = 0;
    myMaximum          = 100;
    myValue            = 50;
    myIncrement        = 1;
    myFastIncrement    = 1;
    myIncrementSet     = 1;
    myFastIncrementSet = 1;

    focus     = false;
    selected  = false;
    enabled   = true;

    borderColor      = QColor(255,255,255);
    backgroundColor1 = QColor(0,50,255);
    backgroundColor2 = QColor(0,180,255);
    focusColor       = QColor(255,0,0);
    selectedColor    = QColor(50,255,50);

    timer1 = new QTimer(this);
    connect(timer1, SIGNAL(timeout()), this, SLOT(controlloIncremento()));

    rotella = NULL;


}

ufgProgressBar::~ufgProgressBar()
{
    delete ui;
}

void ufgProgressBar::paintEvent(QPaintEvent *e)
{
    QRect rect1 = this->rect();
    rect1.setWidth(  this->rect().width()  - 3 );
    rect1.setHeight( this->rect().height() - 3 );
    rect1.setLeft(3);
    rect1.setTop(3);

    QRect rect2 = rect1;

    rect2.setWidth( ((rect1.size().width() - 3 ) * myValue) / myMaximum );
    rect2.setHeight( rect1.size().height() - 3 );
    rect2.setY( rect1.y()  + 4 );
    rect2.setLeft(5);
    //rect2.setTop(5);

    QPainter p(this);
    QPen pen(         QColor( 255 ,150 ,150 ), 2);
    QPen penFocus(    focusColor,    3);
    QPen penBordo(    borderColor ,  3);
    QPen penSelected( selectedColor, 3);

    QLinearGradient myGradient1 = QLinearGradient( QPointF(0,0),QPointF(1,0) );
    QLinearGradient myGradient2 = QLinearGradient( QPointF(0,0),QPointF(1,0) );

    p.setRenderHint(QPainter::Antialiasing);
    QPainterPath path1;
    path1.addRoundedRect( rect1 , 10, 10);
    myGradient1.setStart(  rect1.width() / 2, 0 );
    myGradient1.setFinalStop( rect1.width() / 2, rect1.height() );
    myGradient1.setColorAt( 1, QColor(150,150,150) );
    myGradient1.setColorAt( 0, QColor(210,210,210) );

    QPainterPath path2;
    path2.addRoundedRect( rect2, 10, 10);
    myGradient2.setStart(  rect2.width() / 2, 0 );
    myGradient2.setFinalStop( rect2.size().width() / 2, this->rect().size().height() );
    myGradient2.setColorAt(1, backgroundColor1 );
    myGradient2.setColorAt(0, backgroundColor2 );

    if( selected == true )
    {
        p.setPen(pen);
        p.fillPath(path1,myGradient1);
        p.drawPath(path1);
        p.fillPath(path2,myGradient2);
        p.drawPath(path2);
        p.setPen(penSelected);
        p.drawRoundedRect( rect1 , 10, 10);
    }
    else if( focus == true)
    {
        p.setPen(pen);
        p.fillPath(path1,myGradient1);
        p.drawPath(path1);
        p.fillPath(path2,myGradient2);
        p.drawPath(path2);
        p.setPen( penFocus);
        p.drawRoundedRect( rect1 , 10, 10);
    }
    else
    {
        p.setPen(pen);
        p.fillPath(path1,myGradient1);
        p.drawPath(path1);
        p.fillPath(path2,myGradient2);
        p.drawPath(path2);
        p.setPen( penBordo);
        p.drawRoundedRect( rect1, 10, 10);
    }
}

void ufgProgressBar::mousePressEvent(QMouseEvent* e)
{

}

void ufgProgressBar::mouseReleaseEvent(QMouseEvent* e)
{

}

void ufgProgressBar::mouseMoveEvent(QMouseEvent *event)
{
    if( enabled == true )
    {
        int xx = event->localPos().x();

        //QString st = QString::number( xx ,10);

        // pos in pixel
        int pos = xx;

        // pos in  percentuale
        if( pos < 0 ) pos = 0;
        if( pos > this->rect().width() ) pos = this->rect().width();
        xx = (pos * 100) / this->rect().width();
        myValue = (xx * myMaximum) / 100;

//        myValue = (pos * myMaximum) / this->rect().width() ;

//        QString st = QString("value : %1 %2").arg( myValue).arg(xx);
//        qDebug( st.toLatin1() );

        this->update();
        emit valueChange(myValue);
    }
}

void ufgProgressBar::focusInEvent(QFocusEvent *e)
{
    focus = true;
    this->update();
}

void ufgProgressBar::focusOutEvent(QFocusEvent *e)
{
    focus = false;
    this->update();
}

void ufgProgressBar::setColor(QColor col1, QColor col2, QColor cborder, QColor cfocus, QColor cselected)
{
    backgroundColor1 = col1;
    backgroundColor2 = col2;
    borderColor      = cborder;
    focusColor       = cfocus;
    selectedColor    = cselected;
}

void ufgProgressBar::rotellaMove(bool up)
{
    if( timer1->isActive() == false && rotella != NULL ) timer1->start(100);
    if( up )
    {
        myValue = myValue + myIncrement;
        if( myValue > myMaximum ) myValue = myMaximum;
    }
    else
    {
        myValue = myValue - myIncrement;
        if( myValue < myMinimum ) myValue = myMinimum;
    }
    this->update();
    emit valueChange(myValue);
}

void ufgProgressBar::controlloIncremento()
{
    int dif = rotella->getDiff();

//    QString db = QString("dif %1").arg( dif );
//    qDebug( db.toLatin1() );

    if( dif != 0 )
    {
        // qualcuno ha mosso la rotella
        if(      dif > 30 ) myIncrement = (myFastIncrement * 3);
        else if( dif > 20 ) myIncrement = (myFastIncrement * 2);
        else if( dif > 10 ) myIncrement = myFastIncrement;
        else                myIncrement = myIncrementSet;
    }
    else
    {
        timer1->stop();
        myIncrement = myIncrementSet;
    }
}

void ufgProgressBar::rotellaClick()
{

}




