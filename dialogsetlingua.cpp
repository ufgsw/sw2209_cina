#include "dialogsetlingua.h"
#include "ui_dialogsetlingua.h"

dialogSetLingua::dialogSetLingua(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogSetLingua)
{
    ui->setupUi(this);

    ui->buttonItalia->setBordoEnable(false);
    ui->buttonItalia->setImage( ":/img/flag/italia-128.png",":/img/flag/italia-128.png",":/img/flag/italia-128.png", 100);

    ui->buttonCina->setBordoEnable(false);
    ui->buttonCina->setImage( ":/img/flag/cina-128.png",":/img/flag/cina-128.png",":/img/flag/cina-128.png", 100);

    ui->buttonInglese->setBordoEnable(false);
    ui->buttonInglese->setImage( ":/img/flag/UK-128.png",":/img/flag/UK-128.png",":/img/flag/UK-128.png", 100);




}

dialogSetLingua::~dialogSetLingua()
{
    delete ui;
}
